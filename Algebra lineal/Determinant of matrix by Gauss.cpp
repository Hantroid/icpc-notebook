#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-7;

// ------------- Determinant of matrix by Gauss ------------------
// A es una matriz cuadrada de NxN
Double Determinant(vector< vector<Double> > A){
	Long n = A.size();
	Double det = 1.0;
	for(Long i = 0; i < n; i++){
		Long k = i;
		for(Long j = i+1; j < n; j++){
			if(fabs(A[j][i]) > fabs(A[k][i])) k = j;
		}
		if(fabs(A[k][i]) < EPS) return 0.0;
		swap(A[i],A[k]);
		if(i != k) det = -det;
		det *= A[i][i];
		for(Long j = i+1; j < n; j++) A[i][j] /= A[i][i];
		for(Long j = 0; j < n; j++){
			if(j != i && fabs(A[j][i]) > EPS){
				for(Long k = i+1; k < n; k++){
					A[j][k] -= A[i][k] * A[j][i];
				}
			}
		}
	}
	return det;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(6);
	cout << fixed;
	
	vector< vector<Double> > A;
	A.push_back({8.0,0.0,0.0,-1.0,-1.0,-1.0,-1.0});
	A.push_back({0.0,9.0,-1.0,-1.0,-1.0,-1.0,-1.0});
	A.push_back({0.0,-1.0,6.0,-1.0,-1.0,-1.0,-1.0});
	A.push_back({-1.0,-1.0,-1.0,9.0,0.0,0.0,-1.0});
	A.push_back({-1.0,-1.0,-1.0,0.0,8.0,0.0,0.0});
	A.push_back({-1.0,-1.0,-1.0,0.0,0.0,9.0,-1.0});
	A.push_back({-1.0,-1.0,-1.0,-1.0,0.0,-1.0,6.0});
	cout << Determinant(A) << endl;
	return 0;
}


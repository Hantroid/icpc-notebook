#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long M = 100;
const Long INF = 1e10;

// ------------- Gauss Elimination MOD 2 SLAE -----------------------
// compeljidad O((min(n,m)*n*m)/32)
// n = numero de ecuaciones
// m = numero de variables, M > m
// retorna la cantidad de soluciones y los posbiles valores de X
// 0 si no existe solucion, 1 si tiene 1 solucion e INF si tiene infinitas soluciones
// si tiene al menos 1 (1 o INF) salucion el vector "X" retorna una de ellas, en caso contrario "X" no importa
pair< Long,bitset<M> > Gauss_MOD2_SLAE(vector< bitset<M> > A, Long n, Long m){ // A*X = B donde B es la ultima columna de A
	vector<Long> where(m,-1);
	for(Long col = 0, row = 0; col < m && row < n; col++){
		for(Long i = row; i < n; i++){
			if(A[i][col]){
				swap(A[i],A[row]);
				break;
			}
		}
		if(!A[row][col]) continue;
		where[col] = row;
		for(Long i = 0; i < n; i++){
			if(i != row && A[i][col]) A[i] ^= A[row];
		}
		row++;
	}
	
	bitset<M> X;
	for(Long j = 0; j < m; j++){
		if(where[j] != -1) X[j] = A[where[j]][m];
	}
	// valida q tenga al mneos una solucion
	// si no hay solucion -> la ecuacion i queda de la formma (0 0 0 0 0 0 0 0) = var & var != 0 o (0 0 0 0 0 0 Xj 0 Xk) = var
	for(Long i = 0; i < n; i++){
		bool sum = false;
		for(Long j = 0; j < m; j++) sum ^= (X[j] & A[i][j]);
		if(sum != A[i][m]) return make_pair(0,X);
	}
	// si econtro alguna solucion valida que todas las X tenga alguna fila q lo represente y hay al menos una q no tiene una fila
	// entonces tiene infinitas soluciones
	for(Long i = 0; i < m; i++){
		if(where[i] == -1) return make_pair(INF,X);
	}
	return make_pair(1,X);
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	vector< bitset<M> > A;
	bitset<M> aux;
	aux.set(0,1); // coeficiente de la variable 0
	aux.set(1,0); // coeficiente de la variable 1
	aux.set(2,1); // respuesta de la ecuacion
	A.push_back(aux); // 1ra ecuacion
	A.push_back(aux); // 2da ecuacion
	
	pair< Long,bitset<M> > rp = Gauss_MOD2_SLAE(A,2,2);
	
	cout << rp.first << "\n";
	cout << rp.second[0] << " " << rp.second[1] << "\n";
	return 0;
}


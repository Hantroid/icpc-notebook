#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long INF = 1e10;

Long fp(Long a, Long b, Long c){
	if(b == 0) return 1;
	if(b == 1) return a%c;
	Long t = fp(a,b/2,c);
	t = (t*t)%c;
	if(b%2 == 1) return (a*t)%c;
	return t;
}

// ---------------------- Gauss Elimination - MOD PRIME SLAE ----------------------
// complejidad O((min(n,m)*n*m*log(primo) + n*m*log(primo))/2)
// retorna la cantidad de soluciones y los posbiles valores de X
// 0 si no existe solucion, 1 si tiene 1 solucion e INF si tiene infinitas soluciones
// si tiene al menos 1 (1 o INF) salucion el vector "X" retorna una de ellas, en caso contrario "X" no importa
// A*X = B donde B es la ultima columna de A
pair< Long,vector<Long> > Gauss_MOD_SLAE(vector< vector<Long> > A, Long mod){
	Long n = A.size(); // n = numero de ecuaciones
	Long m = A[0].size() - 1; // m = numero de variables
	for(Long i = 0; i < n; i++){
		for(Long j = 0; j <= m; j++){
			A[i][j] %= mod;
			if(A[i][j] < 0) A[i][j] += mod;
		}
	}
	//determina en que fila se encuentra la solucion de la variable X[j]  (0 ... 0 Xij Xij+1 ... Xim) = bi -> where[j] = i
	vector<Long> where(m,-1);
	for(Long col = 0, row = 0; col < m && row < n; col++){
		Long sel = row;
		for(Long i = row; i < n; i++){
			if(A[i][col] != 0){
				sel = i;
				break;
			}
		}
		if(A[sel][col] == 0) continue;
		for(Long i = col; i <= m; i++) swap(A[sel][i], A[row][i]);
		where[col] = row;
		for(Long i = row+1; i < n; i++){
			Long c = (A[i][col] * fp(A[row][col],mod-2,mod)) % mod;
			for(Long j = col; j <= m; j++){
				A[i][j] -= (A[row][j] * c) % mod;
				A[i][j] %= mod;
				if(A[i][j] < 0) A[i][j] += mod;;
			}
		}
		row++;
	}
	
	vector<Long> X(m,0);
	for(Long j = m-1; j >= 0; j--){
		if(where[j] != -1){
			Long b = A[where[j]][m];
			for(Long k = m-1; k > j; k--){
				b -= (X[k] * A[where[j]][k]) % mod;
				b %= mod;
				if(b < 0) b += mod;
			}
			X[j] = (b * fp(A[where[j]][j],mod-2,mod)) % mod;
		}
	}
	// valida q tenga al menos una solucion
	for(Long i = 0; i < n; i++){
		Long sum = 0;
		for(Long j = 0; j < m; j++){
			sum += (X[j] * A[i][j]) % mod;
			sum %= mod;
		}
		if(A[i][m] != sum) return make_pair(0,X);
	}
	// si econtro alguna solucion valida que todas las X tenga alguna fila q lo represente y hay al menos una q no tiene una fila
	// entonces tiene infinitas soluciones
	for(Long i = 0; i < m; i++){
		if(where[i] == -1) return make_pair(INF,X);
	}
	return make_pair(1,X);
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	vector< vector<Long> > A;
	A.push_back({2,1,4});
	A.push_back({1,2,5});
	
	pair< Long,vector<Long> > rpta = Gauss_MOD_SLAE(A,1000000007);
	
	cout << rpta.first << "\n";
	for(Long x : rpta.second){
		cout << x << " ";
	}
	return 0;
}


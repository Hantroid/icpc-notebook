#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-7;
const Double INF = 1e10;

// complejidad O(min(n,m)*n*m)
//se puede optimizar formando una matriz triangular y luego hallar las variables del ultimo al primero O((min(n,m)*n*m)/2 + n*m)

// retorna la cantidad de soluciones y los posbiles valores de X
// 0 si no existe solucion, 1 si tiene 1 solucion e INF si tiene infinitas soluciones
// si tiene al menos 1 (1 o INF) salucion el vector "X" retorna una de ellas, en caso contrario "X" no importa
pair< Long,vector<Double> > Gauss_solve_SLAE(vector< vector<Double> > A){ // A*X = B donde B es la ultima columna de A
	Long n = A.size(); // n = numero de ecuaciones
	Long m = A[0].size() - 1; // m = numero de variables
	
	//determina en que fila se encuentra la solucion de la variable X[j]  (0 ... 0 Xij 0 ... 0 0) = bi -> where[j] = i
	vector<Long> where(m,-1);
	for(Long col = 0, row = 0; col < m && row < n; col++){
		Long sel = row;
		for(Long i = row; i < n; i++){
			if(fabs(A[i][col]) > fabs(A[sel][col])){
				sel = i;
			}
		}
		if(fabs(A[sel][col]) < EPS) continue;
		for(Long i = col; i <= m; i++){
			swap(A[sel][i], A[row][i]);
		}
		where[col] = row;
		for(Long i = 0; i < n; i++){
			if(i != row){
				Double c = A[i][col] / A[row][col];
				for(Long j = col; j <= m; j++){
					A[i][j] -= A[row][j] * c;
				}
				//en caso de q A[row][col] sea muy grande
				/*for(Long j = col+1; j <= m; j++){
					A[i][j] -= (A[row][j] * A[i][col]) / A[row][col];
				}
				A[i][col] = 0.0;*/
			}
		}
		row++;
	}
	
	vector<Double> X(m,0.0);
	for(Long j = 0; j < m; j++){
		if(where[j] != -1){
			X[j] = A[where[j]][m] / A[where[j]][j];
		}
	}
	// valida q tenga al mneos una solucion
	// si no hay solucion -> la ecuacion i queda de la formma (0 0 0 0 0 0 0 0) = var & var != 0 o (0 0 0 0 0 0 Xj 0 Xk) = var
	for(Long i = 0; i < n; i++){
		Double sum = 0.0;
		for(Long j = 0; j < m; j++){
			sum += X[j] * A[i][j];
		}
		if(fabs(sum - A[i][m]) > EPS){
			return make_pair(0,X);
		}
	}
	// si econtro alguna solucion valida que todas las X tenga alguna fila q lo represente y hay al menos una q no tiene una fila
	// entonces tiene infinitas soluciones
	for(Long i = 0; i < m; i++){
		if(where[i] == -1){
			return make_pair(INF,X);
		}
	}
	return make_pair(1,X);
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	vector< vector<Double> > A;
	/*A.push_back({1.0,1.0,3.0});
	A.push_back({2.0,2.0,6.0});
	A.push_back({1.0,-1.0,8.0});*/
	A.push_back({1.0,0.0,0.0,0.0,1.0,1.0,0.0,12.0});
	A.push_back({0.0,1.0,1.0,1.0,0.0,0.0,1.0,16.0});
	A.push_back({1.0,1.0,0.0,0.0,0.0,1.0,1.0,20.0});
	A.push_back({0.0,0.0,1.0,1.0,1.0,0.0,0.0,8.0});
	A.push_back({1.0,1.0,1.0,0.0,0.0,0.0,0.0,12.0});
	A.push_back({0.0,0.0,0.0,1.0,1.0,1.0,1.0,16.0});
	A.push_back({1.0,1.0,1.0,1.0,1.0,1.0,1.0,28.0});
	
	pair< Long,vector<Double> > rpta = Gauss_solve_SLAE(A);
	
	cout << rpta.first << "\n";
	for(Double x : rpta.second){
		cout << x << " ";
	}
	return 0;
}


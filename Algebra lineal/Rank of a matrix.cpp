#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-7;

Long rank_matrix(vector< vector<Double> > A){
	Long n = A.size();
	Long m = A[0].size();
	Long rank = max(n,m);
	vector<bool> row_selected(n,false);
	for(Long i = 0; i < m; i++){
		Long j;
		for(j = 0; j < n; j++){
			if(!row_selected[j] && fabs(A[i][j]) > EPS){
				break;
			}
		}
		if(j == n){
			rank--;
		}else{
			row_selected[j] = true;
			for(Long p = i + 1; p < m; p++){
				A[j][p] /= A[j][i];
			}
			for(Long k = 0; k < n; k++){
				if(k != j && fabs(A[k][i]) > EPS){
					for(Long p = i + 1; p < m; p++){
						A[k][p] -= A[j][p] * A[k][i];
					}
				}
			}
		}
	}
	return rank;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	vector< vector<Double> > A;
	A.push_back({3.0,2.0,1.0});
	A.push_back({4.0,5.0,6.0});
	
	cout << rank_matrix(A) << endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

//-------------------------------------------------------------------------------------------------
// Independent Vectors: A set of vectors v1→,v2→,…,vn→ is called independent,
// if none of them can be written as the sum of a linear combination of the rest.

// Basis of a Vector Space: A set of vectors is called a basis of a vector
// space, if is the smallest sized set such that all other vectors in the
// vector space are representable by a linear combination of just the element
// vectors of that set.

// Find the basis of a vector space of n vectors, where each vector have d dimensions
// Complexity: O(n * d)

// Each vector is represented by a bitmask, if the dimension is more than
// 63 use a bitset

vector<Long> getBasis(vector<Long> &vectors){
	Long n = vectors.size();
	Long d = 60; // supose the max dimension is 60
	vector<Long> representativeVectors;
	vector<Long> basis(d,0);
	
	for(Long i = 0; i < n; i++){
		Long curMask = vectors[i];
		for(Long j = 0; j < d; j++){
			if((curMask & 1ll << j) == 0) continue;
			if(!basis[j]){
				basis[j] = curMask;
				representativeVectors.push_back(vectors[i]);
				break;
			}
			curMask ^= basis[j];
		}
	}
	// sz = smallest sized set
	Long sz = representativeVectors.size()
	return representativeVectors;
}
//-------------------------------------------------------------------------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-7;

vector<Long> X, Y;
vector<vector<Double>> A;
vector<Double> b, c;
Double z;
Long n, m;

void pivot(Long x, Long y){
	swap(X[y], Y[x]);
	b[x] /= A[x][y];
	for(Long i = 0; i < m; i++) if(i != y) A[x][i] /= A[x][y];
	A[x][y] = 1.0 / A[x][y];
	for(Long i = 0; i < n; i++){
		if(i != x && fabs(A[i][y]) > EPS){
			b[i] -= A[i][y] * b[x];
			for(Long j = 0; j < m; j++) if(j != y) A[i][j] -= A[i][y] * A[x][j];
			A[i][y] = -A[i][y] * A[x][y];
		}
	}
	z += c[y] * b[x];
	for(Long i = 0; i < m; i++) if(i != y) c[i] -= c[y] * A[x][i];
	c[y] = -c[y] * A[x][y];
}

pair<Double,vector<Double>> simplex(vector<vector<Double>> _A, vector<Double> _b, vector<Double> _c){
	A = _A;
	b = _b;
	c = _c;
	n = b.size();
	m = c.size();
	z = 0;
	X = vector<Long>(m);
	Y = vector<Long>(n);
	for(Long i = 0; i < m; i++) X[i] = i;
	for(Long i = 0; i < n; i++) Y[i] = i+m;
	while(true){
		Long x = -1, y = -1;
		Double mn = -EPS;
		for(Long i = 0; i < n; i++) if(b[i] < mn) mn = b[i], x = i;
		if(x < 0) break;
		for(Long i = 0; i < m; i++) if(A[x][i] < -EPS){ y = i; break; }
		assert(y >= 0);
		pivot(x,y);
	}
	while(true){
		Double mx = EPS;
		Long x = -1, y = -1;
		for(Long i = 0; i < m; i++) if(c[i] > mx) mx = c[i], y = i;
		if(y < 0) break;
		Double mn = 1e200;
		for(Long i = 0; i < n; i++) if(A[i][y] > EPS && b[i]/A[i][y] < mn) mn = b[i]/A[i][y], x = i;
		assert(x>=0);
		pivot(x,y);
	}
	
	vector<Double> r(m);
	for(Long i = 0; i < n; i++) if(Y[i] < m) r[Y[i]] = b[i];
	return {z,r};
}


int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	return 0;
}

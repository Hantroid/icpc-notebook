#include <bits/stdc++.h>
using namespace std;

struct Nodo{
	int dato;
	Nodo *der;
	Nodo *izq;
};

void menu();
Nodo *crearNodo(int);
void insertarNodo(Nodo *&,int );

Nodo *arbol = NULL;

int main(){
	menu();
	
	//getch();
	return 0;
}

//funcion menu
void menu(){
	int dato, opcion;
	do{
		cout<<"MENU"<<endl;
		cout<<"1. Insertar un nuevo nodo"<<endl;
		cout<<"2. Salir"<<endl;
		cout<<" Opcion: ";
		cin>>opcion;
		switch(opcion){
			case 1: cout<<endl;
					cout<<"Digite un numero: "<<endl;
					cin>>dato;
					insertarNodo(arbol,dato);
					cout<<endl;
					system("pause");
					break;
		}
		system("cls");
	}while(opcion!=2);
}

//funcion para crar nodo
Nodo *crearNodo(int n){
	Nodo *nuevo_nodo=new Nodo();
	nuevo_nodo->dato = n;
	nuevo_nodo->der = NULL;
	nuevo_nodo->izq = NULL;
	return nuevo_nodo;
}

//funcion insertar elementos en el arbol
void insertarNodo(Nodo *&arbol, int n){
	if(arbol==NULL){
		Nodo *nuevo_nodo = crearNodo(n);
		arbol=nuevo_nodo;
	}else{
		int valorRaiz=arbol->dato; //obrítiene valor de la raiz
		if(n<valorRaiz){
			insertarNodo(arbol->izq, n);
		}else{
			insertarNodo(arbol->der, n);
		}
	}
}

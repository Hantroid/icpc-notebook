#include <bits/stdc++.h>
using namespace std;

vector<long long> adj[26];
bool vis[26];
long long D[26];

void bfs(long long s){
	deque<long long> Q;
	Q.push_back(s);
	vis[s]=true;
	D[s]=0;
	cout<<char(s+'A')<<" "<<D[s]<<endl;
	while(!Q.empty()){
		long long u=Q.front();//toma el valor del primer elemento
		long long sz=adj[u].size();
		for(long long i=0; i<sz; i++){
			long long v=adj[u][i];
			if(!vis[v]){
				vis[v]=true;
				D[v]=D[u]+1;
				cout<<char(v+'A')<<" "<<D[v]<<endl;
				Q.push_back(v);
			}
		}
		Q.pop_front();//saca el primer elemento
	}
}

int main(){
	long long n,m;
	char x,y;
	cin>>n>>m;
	for(long long i=0; i<m; i++){
		cin>>x>>y;
		adj[x-'A'].push_back(y-'A');
		adj[y-'A'].push_back(x-'A'); // debe ser bidirecional
	}
	for(long long i=0; i<26; i++){
		D[i]=n+1;
	}
	bfs('D'-'A');
	return 0;
}


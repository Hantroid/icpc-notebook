#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;
const ll INF=1e9;
ll d[N];
bool used[N];
vector<ll> adj[N];
vector<ll> cost[N];

void bfs01(ll s){
	for(ll i=0; i<n; i++){
		d[i]=INF;
		used[i]=0;
	}
	d[s]=0;
	deque<ll> Q;
	Q.push_back(s);
	while(!Q.empty()){
		ll u=Q.front();
		Q.pop_front();
		if(used[u]) continue;
		used[u]=1;
		for(ll i=0; i<adj[u].size(); i++){
			ll v=adj[u][i];
			ll w=cost[u][i];
			ll temp=d[u]+w;
			if(temp<d[v]){
				d[v]=temp;
				if(w==0){
					Q.push_front(v);
				}else{
					Q.push_back(v);
				}
			}
		}
	}
	return;
}

int main(){
	
	return 0;
}


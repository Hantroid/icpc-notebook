#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=100010;

Long tree1[N];
Long tree2[N];

Long query1(Long i){
	Long sum=0;
	while(i>0){
		sum+=tree1[i];
		i-=(i&-i);
	}
	return sum;
}
Long query2(Long i){
	Long sum=0;
	while(i>0){
		sum+=tree2[i];
		i-=(i&-i);
	}
	return sum;
}

void update1(Long i, Long val){
	while(i<=N){
		tree1[i]+=val;
		i+=(i&-i);
	}
	return;
}
void update2(Long i, Long val){
	while(i<=N){
		tree2[i]+=val;
		i+=(i&-i);
	}
	return;
}

Long query(Long x){
	return query1(x)*x-query2(x);
}

void update(Long i, Long j, Long v){
	update1(i,v);
	update1(j+1,-v);
	update2(i,v*(i-1));
	update2(j+1,-j*v);
	return;
}

void solve(){
	
	for(Long i=0; i<N; i++){
		tree1[i]=0;
		tree2[i]=0;
	}
	
	Long n,q,t;
	cin>>n>>q;
	
	for(Long i=0; i<q; i++){
		cin>>t;
		if(t==0){
			Long l,r,z;
			cin>>l>>r>>z;
			update(l,r,z);
		}else{
			Long l,r;
			cin>>l>>r;
			cout<<query(r)-query(l-1)<<endl;
		}
	}
	return;
}

int main(){
	Long t;
	cin>>t;
	for(Long i=0; i<t; i++){
		solve();
	}
	return 0;
}


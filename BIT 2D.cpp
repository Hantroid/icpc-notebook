#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=1100;

Long tree[N][N];
Long val[N][N];
Long n;

Long query(Long i ,Long j){
	Long sum=0;
	while(i>0){
		Long y=j;
		while(y>0){
			sum+=tree[i][y];
			y-=(y&-y);
		}
		i-=(i&-i);
	}
	return sum;
}

void update(Long i, Long j, Long v){
	while(i<=n+1){
		Long y=j;
		while(y<=n+1){
			tree[i][y]+=v;
			y+=(y&-y);
		}
		i+=(i&-i);
	}
	return;
}

void solve(){
	
	scanf("%lld",&n);
	char s[10];
	while(scanf("%s",&s)){
		if(s[0]=='E'){
			break;
		}
		if(s[1]=='E'){
			Long x,y,v;
			scanf("%lld %lld %lld",&x,&y,&v);
			x++;
			y++;
			Long dif=v-val[x][y];
			val[x][y]=val[x][y]+dif;
			update(x,y,dif);
		}else{
			Long x1,y1,x2,y2;
			scanf("%lld %lld %lld %lld",&x1,&y1,&x2,&y2);
			x1++;
			y1++;
			x2++;
			y2++;
			Long aux=query(x2,y2)-query(x1-1,y2)-query(x2,y1-1)+query(x1-1,y1-1);
			printf("%lld\n",aux);
		}
	}
	
	for(Long i=0; i<=n+3; i++){
		for(Long j=0; j<=n+3; j++){
			tree[i][j]=0;
			val[i][j]=0;
		}
	}
	return;
}

int main(){
	Long t;
	scanf("%lld",&t);
	for(Long i=0; i<t; i++){
		solve();
		printf("\n");
	}
	return 0;
}


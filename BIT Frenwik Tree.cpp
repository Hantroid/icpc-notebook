#include <bits/stdc++.h>
#define ALL(x) x.begin(),x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c))-(c).begin())
using namespace std;
typedef long long Long;

struct FenwickTree{
	Long tree[n+1];
	
	FenwickTree(){
		for(Long i=0; i<=n; i++)	tree[i]=0;
	}
	
	Long query(Long i){
		Long sum=0;
		while(i > 0){
			sum += tree[i];
			i -= ( i & -i );
		}
		return sum;
	}
	void update(Long i, Long dif){
		while(i <= n){
			tree[i] += dif;
			i += ( i & -i );
		}
	}
};

int main(){
	
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
int main(){
	long long n=4;
	for(long long mask=0; mask<(1<<n); mask++){
		cout<<mask<<" : ";
		for(long long i=0; i<n; i++){
			if((mask>>i) & 1 == 1){
				cout<<" "<<i;
			}
		}
		cout<<endl;
	}
	return 0;
}


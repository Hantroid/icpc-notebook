#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;

const ll N=100001;

ld f(ld x){
	return 3.0*x*x-5.0*x+7.0;
}

ld bt(ld l, ld r){
	while(r-l>1e-9){
		ld m1=l+(r-l)/3;
		ld m2=r-(r-l)/3;
		if(f(m1)>f(m2)){
			l=m1;
		}else{
			r=m2;
		}
	}
	return l;
}

int main(){
	cout<<bt(-1e10,1e10)<<endl;
	cout<<f(bt(-1e10,1e10))<<endl;
	return 0;
}


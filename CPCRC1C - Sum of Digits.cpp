#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;

ll dp[10][2][100];
string s;

ofstream out("pack.txt");

ll f(ll pos,ll low,ll sum){
	if(pos==s.size()){
		out<<pos<<" "<<low<<" "<<sum<<" : "<<sum<<endl;
		return sum;
	}
	if(dp[pos][low][sum]!=-1){
		return dp[pos][low][sum];
	}
	dp[pos][low][sum]=0;
	if(low==0){
		for(ll i=0; i<=(s[pos]-'0'); i++){
			if(i==(s[pos]-'0')){
				dp[pos][low][sum]+=f(pos+1,0,sum+i);
			}else{
				dp[pos][low][sum]+=f(pos+1,1,sum+i);
			}
		}
	}else{
		for(ll i=0; i<=9; i++){
			dp[pos][low][sum]+=f(pos+1,1,sum+i);
		}
	}
	out<<pos<<" "<<low<<" "<<sum<<" : "<<dp[pos][low][sum]<<endl;
	return dp[pos][low][sum];
}

int main(){
	/*s="11";
	for(ll i=0; i<10; i++){
		for(ll j=0; j<2; j++){
			for(ll k=0; k<100; k++){
				dp[i][j][k]=-1;
			}
		}
	}
	out<<f(0,0,0)<<endl;*/
	char a[10];
	char b[10];
	while(true){
		cin>>a>>b;
		if(a[0]==b[0] && a[0]=='-' && a[1]=='1'){
			break;
		}
		ll rep=0;
		s=a;
		for(ll i=0; i<s.size(); i++){
			rep+=(s[i]-'0');
		}
		for(ll i=0; i<10; i++){
			for(ll j=0; j<2; j++){
				for(ll k=0; k<100; k++){
					dp[i][j][k]=-1;
				}
			}
		}
		ll aux1=f(0,0,0);
		out<<endl;
		s=b;
		for(ll i=0; i<10; i++){
			for(ll j=0; j<2; j++){
				for(ll k=0; k<100; k++){
					dp[i][j][k]=-1;
				}
			}
		}
		ll aux2=f(0,0,0);
		out<<endl;
		out<<rep+aux2-aux1<<endl;
	}
	return 0;
}


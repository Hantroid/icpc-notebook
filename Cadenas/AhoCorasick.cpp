#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

//--------------------------- Aho Corasick ---------------------------
// test -> https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&problem=1620
const Long LEN = 1e6 + 10;
const Long ALPHA = 52;

map<string,Long> id_word; // usar map en caso donde no importa repeticiones de las palabras y poder asignarle un id
Long end_word[LEN]; // q palabra finaliza en ese nodo (si importa la repeticion usar vector)
Long trie[LEN][ALPHA]; // trie comun
Long cnt_word[LEN]; // usar cuando importa las repeticiones (uso aun no claro)
Long fail_out[LEN]; // sirve de puente entre las palabras finales (las palabras finales posee esa rama en un nodo)
Long fail[LEN]; // sirve de puente para los links de los prefijos al mayor sufijo de las otras ramas
Long nodes = 1; // cantidad de nodos
// fail indica la mayor rama que sea un sufijo de otra rama actual, la cual es menor a la longitud de la rama actual
// Ej: en la rama q forma "aaabbab" hasta el momento, otra rama que forma el prefijo "bab", entonces el fail(link) de "aaabbab"
//     ser� la palabra "bab", debido a que es el mayor sufijo en el trie de la rama "aaabbab".

// fail_out indica la mayor palabra que sea un sufijo de la rama, la cual es menor a la longitud de la rama (palabra)
// Ej: en la rama q forma "aaabbab", otra rama que forma la palabra "bab", entonces el fail_out(link_palabras) de "aaabbab"
//     ser� la palabra "bab", debido a que es el mayor sufijo en el trie de la palabra "aaabbab".

//   ** Tener encuenta q cada palabra es prefijo de si misma, pero esto no se representa tanto para el fail y fail_out,
//      para ello en el caso de la palabra se determina el final de la palabra con el end_word

Long conv(char c){
	if('a' <= c && c <= 'z') return c - 'a';
	return c - 'A' + 26;
}

void ini_ahoCorasick(){
	id_word.clear();
	for(Long i = 0; i <= nodes; i++){
		end_word[i] = cnt_word[i] = fail[i] = fail_out[i] = 0;
		for(Long j = 0; j < ALPHA; j++){
			trie[i][j] = 0;
		}
	}
	nodes = 1;
	return;
}

void addString(string &s, Long id){
	Long cur = 0; // aplicacion de trie
	for(char c : s){
		Long x = conv(c);
		if(!trie[cur][x]){
			nodes++;
			trie[cur][x] = nodes;
		}
		cur = trie[cur][x];
	}
	cnt_word[cur]++;
	end_word[cur] = id;
}

// crea los links de las palabras con el mayor sufijo de las otras palabras
void build_ahoCorasick(){
	queue<Long> q; // creacion de la cola para crear los links (fail)
	q.push(0);
	while(!q.empty()){
		Long u = q.front();
		q.pop();
		for(Long i = 0; i < ALPHA; i++){
			Long v = trie[u][i];
			if(v == 0) continue;
			q.push(v);
			if(u == 0) continue;
			fail[v] = fail[u];
			while(fail[v] != 0 && trie[fail[v]][i] == 0){ // es equivalente al KMP pero en trie
				fail[v] = fail[fail[v]];
			}
			fail[v] = trie[fail[v]][i]; // crea los links en los prefijos
			fail_out[v] = (end_word[fail[v]] != 0 ? fail[v] : fail_out[fail[v]]); // crea los links entre las palabras finales
			cnt_word[v] += cnt_word[fail[v]];
		}
	}
	return;
}

Long go(Long cur, char c){
	while(cur != 0 && trie[cur][conv(c)] == 0){
		cur = fail[cur];
	}
	cur = trie[cur][conv(c)];
	// agregar una funcion si se requiere
	return cur;
}
// Aplicaciones
//  - contar la cantidad de palabras que aparece en el texto (v = fail_out[v]) hasta q v sea 0 y agregar 1 para cada end_word (q es
//    determiando por el fial_out) de dicha palabra en el transcurso que las letras del texto avanza, se puede obtener informacion de
//    la posicion donde inicia y termina la subcadena que contiene a la palabra en el texto creando un arreglo conde me indique la 
//    posicion y q palabra es
//  Ej: Texto = abab, palabras = {bab, ab};
//      Texto = a              -> "a" no existe como palabra
//      Texto = ab             -> "ab" existe como palabra entonces la cadena "abab" posee 1 substring "ab" hasta el momento
//      Texto = aba            -> "aba" no existe como palabra pero el mayor sufijo es "ba", entonces "aba" -> "ba"
//      Texto = abab           -> "bab" existe como palabra entonces la cadena "abab", pero ademas la cadena posee la palabra "ab"
//                                como substring en su sufijo, por eso ahora la cadena "abab" posee:
//                                2 substring "ab" y 1 substring "bab" como substring
//                  si la cadena "bab" tuviese mas sufijos q contengan palabras el fail_out recorrera todas hasta q llegue a la raiz

// En caso de buscar muchas palabras en muchos textos, crear una especie de trie con los textos (similar a un como un dfs) donde
// cada rama representa un texto agregar la informacion encontrada (ej, cantidad de match con las palabras) cada vez q avanza, y luego
// quitar cuando ya no usa ese nodo (cuando el dfs retrocede, para ir por otra rama (texto)) y guardar la respuestas en un arreglo
//--------------------------- Aho Corasick ---------------------------

bool vis[LEN];

void solve(){
	ini_ahoCorasick();
	vector<string> queries;
	string texto, palabra;
	cin >> texto;
	Long cnt_palabras;
	cin >> cnt_palabras;
	Long ids = 1;
	for(Long i = 0; i < cnt_palabras; i++){
		cin >> palabra;
		if(id_word.find(palabra) == id_word.end()){
			id_word[palabra] = ids;
			addString(palabra,ids);
			ids++;
		}
		queries.push_back(palabra);
		vis[i+1] = false;
	}
	build_ahoCorasick();
	
	Long cur = 0;
	for(Long i = 0; i < texto.size(); i++){
		cur = go(cur,texto[i]);
		vis[end_word[cur]] = true;
	}
	
	for(auto x : queries){
		if(vis[id_word[x]]){
			cout << "y\n";
		}else{
			cout << "n\n";
		}
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Long t;
	cin >> t;
	while(t--){
		solve();
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

string leastRotation(string &s){
	Long tam = s.size();
	s += s;
	Long n = s.size();
	vector<Long> f(n,-1);
	Long k = 0;
	for(Long j = 1; j < n; j++){
		Long i = f[j - k -1];
		while(i != -1 && s[j] != s[k + i + 1]){
			if(s[j] < s[k + i + 1]) k = j - i - 1;
			i = f[i];
		}
		if(s[j] != s[k + i + 1]){
			if(s[j] < s[k]) k = j;
			f[j - k] = -1;
		}else{
			f[j - k] = i + 1;
		}
	}
	s = s.substr(k,tam);
	return s;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	string s = "jhasjaavsda";
	string rp = leastRotation(s);
	cout << rp << "\n";
	Long n = s.size();
	string ans = rp;
	s = rp + rp;
	for(Long i = 0; i < n; i++) ans = min(ans,s.substr(i,n));
	cout << ans << "\n";
	return 0;
}

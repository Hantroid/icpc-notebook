#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

//const Long N=;

const Long N = 1e5;
const Long mod = 1e9+7;

string s;
Long pot[N];
Long hpref[N];

Long hSub(Long l, Long r){
	if(l == 0) return hpref[r];
	return ((hpref[r] - hpref[l-1] * pot[r - l + 1]) % mod + mod) % mod;
}

void hashing(){
	Long n = s.size();
	Long base;
	base = 29;
	pot[0] = 1;
	for(Long i = 1; i < n; i++){
		pot[i] = (pot[i-1] * base) % mod;
	}
	hpref[0] = s[0] - 'a' + 1;
	for(Long i = 1; i < n; i++){
		hpref[i] = (hpref[i-1] * base + (s[i] - 'a' + 1)) % mod;
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	hashing();
	return 0;
}


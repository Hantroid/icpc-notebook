#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

const Long N = 1e5;
const Long mod = 1e9+7;

string s;
Long pot[2][N];
Long hpref[2][N];

pair<Long,Long> hSub(Long l, Long r){
	if(l == 0) return {hpref[0][r],hpref[1][r]};
	Long aux1 = ((hpref[0][r] - hpref[0][l-1] * pot[0][r - l + 1]) % mod + mod) % mod;
	Long aux2 = ((hpref[1][r] - hpref[1][l-1] * pot[1][r - l + 1]) % mod + mod) % mod;
	return {aux1,aux2};
}

void hashing(){
	Long n = s.size();
	Long base[2];
	base[0] = 29;
	base[1] = 37;
	pot[0][0] = 1;
	pot[1][0] = 1;
	for(Long i = 1; i < n; i++){
		pot[0][i] = (pot[0][i-1] * base[0]) % mod;
		pot[1][i] = (pot[1][i-1] * base[1]) % mod;
	}
	hpref[0][0] = s[0] - 'a' + 1;
	hpref[1][0] = s[0] - 'a' + 1;
	for(Long i = 1; i < n; i++){
		hpref[0][i] = (hpref[0][i-1] * base[0] + (s[i] - 'a' + 1)) % mod;
		hpref[1][i] = (hpref[1][i-1] * base[1] + (s[i] - 'a' + 1)) % mod;
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    hashing();
    
	return 0;
}


#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

const Long N = 1e5+10;
const Long mod = 1e9+7;

string s;
Long pot[N];
Long hpref[N];
Long hsuff[N];

Long Pref(Long l, Long r, Long n){
	if(l > r) swap(l,r);
	if(l == 0) return hpref[r];
	return ((hpref[r] - hpref[l-1] * pot[r - l + 1]) % mod + mod) % mod;
}
Long Suff(Long l, Long r, Long n){
	if(l > r) swap(l,r);
	if(r == n-1) return hsuff[l];
	return ((hsuff[l] - hsuff[r+1] * pot[r - l + 1]) % mod + mod) % mod;
}

void hashing(){
	Long n = s.size();
	Long base;
	base = 29;
	pot[0] = 1;
	for(Long i = 1; i < n; i++){
		pot[i] = (pot[i-1] * base) % mod;
	}
	hpref[0] = s[0] - 'A' + 1;
	for(Long i = 1; i < n; i++){
		hpref[i] = (hpref[i-1] * base + (s[i] - 'A' + 1)) % mod;
	}
	hsuff[n-1] = s[n-1] - 'A' + 1;
	for(Long i = n-2; i >= 0; i--){
		hsuff[i] = (hsuff[i+1] * base + (s[i] - 'A' + 1)) % mod;
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    s = "abaz";
    
    hashing();
    for(Long i = 0; i < s.size(); i++){
    	for(Long j = i; j < s.size(); j++){
    		if(Pref(i,j,s.size()) == Suff(i,j,s.size())){
    			cout << i << " " << j << " es palindormo" << endl;
			}
		}
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

vector<Long> d_pal[2];

// 0 = : palindromo par
// 1 = : palindromo impar
void Manachers(vector<Long> &A){ 
	Long n = A.size();
	d_pal[0].resize(n,0);
	d_pal[1].resize(n,0);
	for(Long i = 0, l = 0, r = -1; i < n; i++){
		Long k = (i > r) ? 1 : min(d_pal[1][l + r - i], r - i + 1);
		while(0 <= i - k && i + k < n && A[i - k] == A[i + k]) k++;
		d_pal[1][i] = k--;
		if(i + k > r){
			l = i - k;
			r = i + k;
		}
	}
	
	for(Long i = 0, l = 0, r = -1; i < n; i++){
		Long k = (i > r) ? 0 : min(d_pal[0][l + r - i + 1], r - i + 1);
		while(0 <= i - k - 1 && i + k < n && A[i - k - 1] == A[i + k]) k++;
		d_pal[0][i] = k--;
		if(i + k > r){
			l = i - k - 1;
			r = i + k;
		}
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	// aabbbcccc
	// 1 1 1 2 1 1 2 2 1
	// 0 1 0 1 1 0 1 2 1
	
	string s;
	cin >> s;
	vector<Long> v;
	for(Long i = 0; i < s.size(); i++) v.push_back(s[i] - 'a');
	Manachers(v);
	
	for(Long i = 0; i < s.size(); i++){
		cout << d_pal[1][i] << " ";
	}
	cout << endl;
	
	for(Long i = 0; i < s.size(); i++){
		cout << d_pal[0][i] << " ";
	}
	cout << endl;
	return 0;
}


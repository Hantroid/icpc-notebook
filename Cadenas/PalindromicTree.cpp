#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

struct palindromic_tree{
    static const Long SIGMA=26;
    struct Node{
        Long len, link, to[SIGMA];
        Long cnt;
        Node(Long len, Long link=0, Long cnt=1):len(len),link(link),cnt(cnt){
            memset(to,0,sizeof(to));
        }
    };
    vector<Node> ns;
    Long last;
    palindromic_tree():last(0){ns.push_back(Node(-1)); ns.push_back(Node(0));}
    void add(Long i, string &s){
        Long p=last, c=s[i]-'a';
        while(s[i-ns[p].len-1]!=s[i])p=ns[p].link;
        if(ns[p].to[c]){
            last=ns[p].to[c];
            ns[last].cnt++;
        }else{
            Long q=ns[p].link;
            while(s[i-ns[q].len-1]!=s[i])q=ns[q].link;
            q=max(1ll,ns[q].to[c]);
            last = ns[p].to[c] = ns.size();
            ns.push_back(Node(ns[p].len+2,q,1));
        }
    }
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Long n;
	cin >> n;
	string s; cin>>s;
	palindromic_tree p;
	for(Long i = 0; i < s.size(); i++) p.add(i,s);
	
	Long ans = 0;
	for(Long i = 0; i < p.ns.size(); i++){
		if(p.ns[i].len >= 3 && p.ns[i].len % 2 == 1) ans++;
	}
	
	cout << ans << "\n";
	
	return 0;
}

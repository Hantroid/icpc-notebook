#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

//const Long MX=;

struct suffixArray{
	string s;
	Long n, alphabet;
	vector<Long> p, c, cnt, rank, lcp; // rank is the order when suffix start in ith position on the string
	vector< vector<Long> > st;
	
	suffixArray(){}
	suffixArray(string s) : s(s){
		s += '$'; // add adicional chacracter thas it is lower than other characters
		n = s.size();
		alphabet = 256;
		p.resize(n,0); // p is a permutation of suffix array order
		c.resize(n,0); // c assing a class to every index
		cnt.resize(max(alphabet,n),0); // its a count to use radix sort
		rank.resize(n-1,0); // position of suffix in sorted list of suffixes
		lcp.resize(n-2,0); // lcp between 2 continuous suffix in sorted list of suffixes
	}
	
	void build(){ //O(nlogn)
		for(Long i = 0; i < n; i++) cnt[s[i]]++;
		for(Long i = 1; i < alphabet; i++) cnt[i] += cnt[i-1];
		for(Long i = 0; i < n; i++) p[--cnt[s[i]]] = i; // get a permutation
		
		// assing the classes = 0 to the fist in lexicographical order, same substring have same class
		c[p[0]] = 0;
		Long classes = 1;
		for(Long i = 1; i < n; i++){
			if(s[p[i]] != s[p[i-1]]) classes++;
			c[p[i]] = classes - 1;
		}
		
		vector<Long> pn(n), cn(n);
		for(Long h = 0; (1ll<<h) < n; h++){
			for(Long i = 0; i < n; i++){
				pn[i] = p[i] - (1ll<<h);
				if(pn[i] < 0) pn[i] += n;
			}
			
			// reorder the permutation by the previous substring
			fill(cnt.begin(), cnt.begin() + classes, 0);
			for(Long i = 0; i < n; i++) cnt[c[pn[i]]]++;
			for(Long i = 1; i < classes; i++) cnt[i] += cnt[i-1];
			for(Long i = n-1; i >= 0; i--) p[--cnt[c[pn[i]]]] = pn[i];
			
			// assing the new classes
			cn[p[0]] = 0;
			classes = 1;
			for(Long i = 1; i < n; i++){
				if(c[p[i]] != c[p[i-1]] || c[p[i]+(1<<h)-(p[i]+(1<<h)<n?0:n)] != c[p[i-1]+(1<<h)-(p[i-1]+(1<<h)<n?0:n)]) classes++;
				cn[p[i]] = classes - 1;
			}
			swap(c,cn);
		}
		n--;
		p.erase(p.begin());
		return;
	}
	
	// i and j are positions on the string [0,n>
	Long lcp2(Long i, Long j){ // LCP of 2 substring of s O(1)
		Long l = rank[i];
		Long r = rank[j];
		if(l > r) swap(l,r);
		Long T = r-l+1;
		Long lg = 63-(__builtin_clzll(T));
		return min(st[lg][l],st[lg][r-(1<<lg)+1]);
	}
	
	void buildST(){ //O(nlogn)
		st.push_back(vector<Long> (n,-1));
		for(Long i = 0; i < n; i++) st[0][i] = n - p[i];
		for(Long h = 1; (1ll<<h) <= n; h++){
			st.push_back(vector<Long> (n,-1));
			for(Long i = 0; i + (1ll<<h) - 1 < n; i++){
				st[h][i] = min({st[h-1][i],st[h-1][i+(1ll<<(h-1))],lcp[i-1+(1ll<<(h-1))]});
			}
		}
		return;
	}
	
	void lcp_construction(){ // lcp construction O(n)
		for(Long i = 0; i < n; i++) rank[p[i]] = i;
		Long k = 0;
		for(Long i = 0; i < n; i++){
			if(rank[i] == n - 1){
				k = 0;
				continue;
			}
			Long j = p[rank[i] + 1];
			while(i + k < n && j + k < n && s[i + k] == s[j + k]) k++;
			lcp[rank[i]] = k;
			if(k) k--;
		}
		buildST(); // use if need sparse table in lcp
		return;
	}
	
	Long num_different_substrings(){
		Long ans = (n * n + n) / 2;
		for(Long i = 0; i < n - 1; i++) ans -= lcp[i];
		return ans;
	}
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    string s = "aaba";
    suffixArray sa = suffixArray(s);
    sa.build();
    
    /*for(Long x : sa.p){
    	cout << x << " ";
	}
	cout << endl;
	
	for(Long i = 0; i < s.size(); i++){
		for(Long j = 0; j < s.size(); j++){
			cout << i << " " << j << " = " << sa.compare(i,j,4) << endl;
		}
	}*/
	
	/*for(Long i = 0; i < s.size(); i++){
		for(Long j = 0; j < s.size(); j++){
			cout << i << " " << j << " = " << sa.lcp2(i,j) << endl;
		}
	}*/
	
	sa.lcp_construction();
	for(Long xx : sa.lcp) cout << xx << " ";
	cout << endl;
	
	cout << "---> " << sa.num_different_substrings() << endl;
	return 0;
}


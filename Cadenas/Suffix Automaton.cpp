#include <bits/stdc++.h>
using namespace std;
typedef long long Long;


//----------------------------------- Suffix Automaton ---------------------------
// Complexity O(nlog(Alpha))   --->   O(n)
// Memory O(n)             --->   O(nAlpha)
// const Long MX = ; // MX should be almost |S|*2
// OBS: can construct automaton only of 1 strings if need more strings
// you can add any simbols between words ***
struct state{
	Long len, link;
	map<char,Long> next; // change to vector could be optimize in time
	state(){}
	state(Long len, Long link) : len(len), link(link){}
};

struct suffixAutomaton{
	vector<bool> isClone; 
	vector<state> st;
	Long sz, last;
	
	suffixAutomaton(){}
	suffixAutomaton(Long n){ // clear map if need reuse!!
		st.resize(n << 1ll); // set st.next[i].resize(26,-1) if use vector
		isClone.resize(n << 1ll,true);
		last = 0;
		st[0].len = 0;
		st[0].link = -1;
		sz = 1;
	}
	
	void addChar(char c){ // c is Long if use vector
		Long cur = sz++;
		isClone[cur] = false;
		st[cur].len = st[last].len + 1;
		Long p = last;
		while(p != -1 && !st[p].next.count(c)){ // to vector use st[p].next[c] == -1
			st[p].next[c] = cur;
			p = st[p].link;
		}
		if(p == -1){
			st[cur].link = 0;
		}else{
			Long q = st[p].next[c];
			if(st[p].len + 1 == st[q].len){
				st[cur].link = q;
			}else{
				Long clone = sz++;
				st[clone].len = st[p].len + 1;
            	st[clone].next = st[q].next;
            	st[clone].link = st[q].link;
				while(p != -1 && st[p].next[c] == q){
					st[p].next[c] = clone;
					p = st[p].link;
				}
				st[q].link = st[cur].link = clone;
			}
		}
		last = cur;
	}
	
	void build(string &s){
		for(char c : s) addChar(c);
		return;
	}
	
	void print(){
		cout << "Nxt Array" << endl;
		for(Long i = 0; i < sz; i++){
			for(auto par : st[i].next){
				cout << i << " -> " << par.second << " : " << par.first << endl;
			}
		}
		cout << "Suffix Links" << endl;
		for(Long i = 0; i < sz; i++){
			cout << i << " -> " << st[i].link << endl;
		}
		return;
	}
};
// para pusher los nodos en direccion a los links, crear un vector de adj, donde las tranciciones tienen direccion opuesta
// luego aplicar topological sort O(n)
// cuando se cuenta la longitud de la palabra recorrida en el automata, para cada transicion si existe la sgte palabra la longitud aumenta 
// en 1 sino la trancision vuelve a travez del suffix link donde la lingitud es igual a al ".len" del estado donde se encuentra

// Nota: - Cada nodo posee un suffix link, pero cada nodo puede ser suffix link de 0 o mas nodos
// Aplicaciones
// - Validar si un conjunto de patrones es contenido por un texto (construir el automata del texto, y para cada patron validar 
//   que pueda transicionar durante todo el patron, sino puede transicionar, etonces el patron no es contenido por el texto)
// - Hallar la cantidad de diferentes substring (dp[padre] = 1 + suamtoria (dp[hijos]), las aristas son "st[i].next[char] = (char, node)" )
//   No olvidar la memorizacion, sino dar� TLE, rpta = dp[0] - 1
// - Suma de longitudes de todas las subcadenas distintas (usando el dp anterior, ans[v] = sumatoria(dp[hijos] + ans[hijo]), rpta = ans[0])
// - Hallar el k-th lexicograficamente menor de todas las substring diferentes O(|S| * APLPHA) (reutilizando el dp de cantidad diferentes de
//   substring, reconstruir el dp en base a la cantidad de soluciones q pose cada estado, recorrer los estados si el L <= k && k <= R, sino
//   cumple buscar en el siguiente estado)
// - Hallar la cantidad de subtrings distintas con longitud L (para cada nodo agreagar 1 en el rango de [len(st[u].link) + 1, len[u]])
//   Ej: cuando se tiene el automata, cuando se agrega 1 a la longitud de los nodos en un camino determinado 0 -> v, se debe agregar 1
//       en el rango de [1, len[v]], pero como el ultimo nodo posee un suffix link, estar�a repitiendo las longitudes del mayor sufijo
//       es por ello que solo se toma el rango de [len(st[u].link) + 1, len[u]]
//       si el camino forma "acbac", contaria las longitudes de "acbac", "cbac", "bac", "ac", "c", pero como el mayor sufijo de "acbac" es
//       "ac" estar�a repitiendose "ac" y "c", que fue contado en algun momento
// - Hallar el menor cyclic shift (Desde el nodo 0 ir siempre al menor caracter, hasta que no se pueda, luego completar con las cadenas
//   restantes que forman parte del inicio de la cadena)
// - Hallar la cantidad de ocurrencias de un conjunto de patrones P en un texto T O(P) (crear el automata de T, crear un arreglo extra que 
//   posee la cantidad de ocurrencias si termina en cierto estado cnt[u], a cada nodo que no es un clon asignar cnt[u] = 1 y a los demas 0, 
//   incluyendo cnt[nodo = 0] = 0, a cada nodo asignar cnt[st[u].link] += cnt[u] (ordenado de forma decreciente por el st[u].len),
//   luego para cada patron realizar las tranciciones, si trancisiona
//   completamente la respuesta sera cnt[estado final], sino puede transicionar ser� 0, complejidad O(P))
// - Hallar la k-th subcadena lexicobraficamente con repeticion de una cadena S (2 formas)
//   Forma1: a cada nodo que no es un clon asignar memo[u] = n - pos (pos indexado desde 0, de "abc", cuando agrega "a" -> pos = 0, cuando 
//   agrega "c" -> pos = 2) y a los demas 0, incluyendo el memo[nodo = 0] = 0, luego a cada nodo asignar memo[st[u].link] += memo[u]
//   Forma2: a cada nodo que no es un clon asignar cnt[u] = 1 y a los demas 0, incluyendo cnt[nodo = 0] = 0, despues a cada nodo asignar
//   cnt[st[u].link] += cnt[u], luego realizar un dp de la forma (dp[padre] = cnt[padre] + suamtoria (dp[hijos]), se generara un dp identico
//   a la forma anterior, solo en el caso del que padre sea el nodo padre = 0, realizar dp[padre] = suamtoria (dp[hijos])
//   Forma 1 y 2: de esta forma se sabe a que nodo ir en base a las transiciones (para hallar la subcadena se debe transicionar por los
//	 estados similar a la reconstruccion del anterior dp, preferible simular en papel)
// - Hallar la primera ocurrencia de un conjunto de patrones P en un texto T O(P) (crear el automata de T, esta vez agregando un arreglo
//   llamado firstpos(cur) = len(cur) y para clon firstpos(clone) = firstpos(q), las posiciones estan indexadas desde 1, luego procesar las
//   las trancisiones del patron, si llega a su ultima transicion, la respuesta es = firstpos(State) - len(P) + 1, ya que el estado final 
//   dar� como respuesta la posicion del �ltimo car�cter del patron en su primera ocurrencia asi que para volver al primer caracter se aplica
//   la funcion, de caso contrario no existe la primera ocurrencia, complejidad O(P))
// - Hallar la menor LONGITUD de la cadena que no aparece en la string S (Se puede realizar un dp partiendo desde el estado inicial,
//   desde este estado podremos llamar recursivamente cual la respuesta para el subproblema en sus hijos, por ejemplo: si en el estado
//   inicial no se puede transicionar al estado que agrega la letra "c", pero si se puede transicionar a "a", y "b", entonces quiere decir
//   q "c" es la cadena de menor longitud que no existe en la string S, la palabra "d" o "z" tambien pueden ser respuesta si es que no 
//   aparecen, en caso que a partir del estado inicial se pueda transicionar a travez de todas los caracteres, entonces la respuesta ser�
//   1 + dp[hijos], cplejidad O(nAlph))
// - Hallar la subcadena comun mas larga de 2 cadenas O(S + T) (Crear el automata para S, luego trancisionar a travez del automata los
//   caracteres de la cadena T, partiendo desde el estado 0, si puede transicionar, la subcadena aumenta en 1, en caso contrario se 
//   transicionar� al suffix link que a la par reduce la longitud al tama�o de st[link].len, pero seguira maximizando la respuesta, ya que 
//   es el mayor sufijo del estado donde se econtraba, la respuesta es la maxima longitud del todo el proceso)
// - Hallar la subcadena comun mas larga de multiples cadenas O(sum(|S|)) (Crear 1 solo automata para todas las palabras. Cada vez que se
//   agrega una nueva palabra, setear el last = 0 para simular el proceso inicial del automata y agregar cada caracter de esta de esta 
//   nueva palabra con addChar. A�adir 2 variables al estado, ult = -1 el ultimo id de la pabalra agregada y cnt = 0 cantidad de cadenas que
//   contienen ese estado. Para calcular la respuesta, luego de crear el automata, recorrer las transiciones de cada palabra asignando
//   last = 0 al iniciar una nueva palabra, para cada estado recorrer sus links y actualizar el ultimo id y en caso de q el id se igual al
//   ultimo id que paso por ahi, dejar de expandir sino aumenta su cnt en 1, la respuesta seria el mayor ".len" del estado q tenga el ".cnt"
//   igual a la cantidad de palabras) // el automata solo aplicable para este caso, tambien se puede realizar agregando caracteres entre
//   cada palabra o usar Aho Corasick
/*	Codigo // importante primero crear el automata, luego recorrer (***)
	Long ans = 0;
	for(Long i = 0; i < cad.size(); i++){
		Long cur = 0;
		for(char c : cad[i]){
			cur = sa.st[cur].next[c];
			Long auxi = cur;
			while(auxi != 0 && sa.st[auxi].ult != i){
				sa.st[auxi].ult = i;
				sa.st[auxi].tot++;
				if(sa.st[auxi].tot == cad.size()) ans = max(ans,sa.st[auxi].len);
				auxi = sa.st[auxi].link;
			}
		}
	}
*/
//----------------------------------- Suffix Automaton ---------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;


	return 0;
}


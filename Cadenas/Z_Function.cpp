#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;


//---------------------------------- Z Function ------------------------------
// The Z-function for this string is an array of length n where the i-th element 
// is equal to the greatest number of characters starting from the position i 
// that coincide with the first characters of s.

vector<Long> z_function(string& s){
	Long l = 0, r = 0, n = s.size();
	vector<Long> z(n,0);
	for(Long i = 1; i < n; i++){
		if(i <= r) z[i] = min(r - i + 1, z[i - l]);
		while(i + z[i] < n && s[z[i]] == s[i + z[i]]) z[i]++;
		if(i + z[i] - 1 > r) l = i, r = i + z[i] - 1;
	}
	return z;
}

// Aplicaciones
// - Buscar si un patron se repite en un texto y que posiciones
// - Encontrar cuantas subcadenas son distintas y cuales son O(n^2)
//   Ej: Aplicar Z function de [0 a n - 1], luego setear Z[0] = tama�o de la cadena, para cada Z Function de cierta posicion "p" se sabe 
//       cual es el prefijo mas largo a partir de la pos "p", con un arreglo booleano hacer true para cada intervalo (i -> i + Z[p][i] - 1,
//        i -> i + Z[p][i] - 2, ..., i -> i) como booleano, apenas se encuentra un rango verdadero se rompe el bucle
//       ABCNABC hallando la funcion Z para la pos 0 seria, Z[0] = [0,0,0,0,3,0,0], seteando Z[p][0] -> Z[0] = [7,0,0,0,3,0,0]
//       como la subcadena ABC se repite empezando desde 0 y 4, los rangos de [0,2] y [4,6] seran verdaderos, para cuando se aplique
//       la Z Function desde la pos 4, ya no se volvera a contar el rango [4,6], ya que se cont� en la pos 0
//---------------------------------- Z Function ------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


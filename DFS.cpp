#include <bits/stdc++.h>
#define N 100000
using namespace std;
vector<long long> adj[N];
bool vis[N],ciclo;
long long comp[N], T[N];

void dfs(long long u,long long curComp){
	vis[u]=1;
	comp[u]=curComp;
	long long sz=adj[u].size();
	for(long long i=0; i<sz; i++){
		long long v=adj[u][i];
		if(!vis[v]){
			dfs(v,curComp);
			T[v]=u;
		}else{
			if(v!=T[u]){
				ciclo =1; // ancestro ya visitado (back edge)
			}
		}
	}
}

int main(){
	long long n,m,x,y;
	cin>>n>>m;
	for(long long i=0; i<m; i++){
		cin>>x>>y;
		x--;
		y--;
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	long long cantComp=0;
	for(long long i=0; i<n; i++){
		if(!vis[i]){
			T[i]=-1;
			cantComp++;
			dfs(i,cantComp);
		}
	}
	cout<<"nro comp: "<<cantComp<<endl;
	for(long long i=0; i<n; i++){
		cout<<i+1<<" "<<" -> "<<comp[i]<<" "<<T[i]+1<<endl;
	}
	cout<<"ciclo: "<<ciclo<<endl;
	return 0;
}


#include <bits/stdc++.h>
#define INF 10000000000
using namespace std;

vector<long long> adj[1001];
vector<long long> pes[1001];
bool vis[1001];
long long dis[1001];
long long previo[1001];
long long n,m;

struct nodo{
	long long fin;
	long long peso;
	nodo(long long a, long long b){
		fin=a;
		peso=b;
	}
	nodo(){}
	bool operator < (nodo X) const{
		return X.peso<peso;
	}
};

void dijkstra(long long ini){
	for(long long i=0; i<n; i++){
		dis[i]=INF;
		previo[i]=-1;
	}
	priority_queue<nodo> Q;
	Q.push(nodo(ini,0));
	dis[ini]=0;
	while(!Q.empty()){
		/*priority_queue<nodo> aux=Q;
		while(!aux.empty()){
			nodo aa=aux.top();
			cout<<aa.peso<<" - "<<aa.fin<<" | ";
			aux.pop();
		}
		cout<<endl;*/
		nodo ac=Q.top();
		Q.pop();
		if(!vis[ac.fin]){
			vis[ac.fin]=true;
			long long sz=adj[ac.fin].size();
			for(long long i=0; i<sz; i++){
				long long ady=adj[ac.fin][i];
				long long peso=pes[ac.fin][i];
				if(dis[ac.fin]+peso<dis[ady]){
					dis[ady]=dis[ac.fin]+peso;
					previo[ady]=ac.fin;
					Q.push(nodo(ady,dis[ady]));
				}
			}
		}
	}
	return;
}

int main(){
	long long x,y,peso;
	cin>>n>>m;
	for(long long i=0; i<m; i++){
		cin>>x>>y>>peso;
		x--;
		y--;
		adj[x].push_back(y);
		pes[x].push_back(peso);
		//si es bidireccional se agrega los otros 2
		adj[y].push_back(x);
		pes[y].push_back(peso);
	}
	long long ini, fin;
	cin>>ini>>fin;
	ini--;
	fin--;
	dijkstra(ini);
	cout<<dis[fin]<<endl;
	//con el arreglo previo se puede saber la ruta mas corta de nodo ini al cualquier nodo
	return 0;
}


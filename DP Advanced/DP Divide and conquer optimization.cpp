#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;
const Long INF = 1e16;
Long memo[15][100011];

//---------------------------- DP Divide and conquer optimization ---------------------------
// it could be work with min or max, for simplicity we will use min
// Original Recurrence: dp[lvl][i] = min{dp[lvl-1][k] + C(k,i)}, where k < i and C(k,i): cost function
// Condition:
// if a < b and ka <= kb for dp[lvl][a] and dp[lvl][b] and ka if the leftmost optim point, where
// dp[lvl][a] = dp[lvl-1][ka] + C(ka,a) and dp[lvl][b] = dp[lvl-1][kb] + C(kb,b)
//
// Proof this condition by contradiction:
// if (ka is optim when i = a and kb is optim when i = b) and (kb < ka and a < b)
// we proof that: dp[lvl-1][kb] + C(kb,b) < dp[lvl-1][ka] + C(ka,b), so:
//
// dp[lvl][a] < dp[lvl - 1][kb] + C(kb,a)
// dp[lvl-1][ka] + C(ka,a) < dp[lvl-1][kb] + C(kb,a)
// dp[lvl-1][ka] + C(ka,a) # C(a,b) < dp[lvl-1][kb] + C(kb,a) # C(a,b), where C(kx,a) # C(a,b) = C(kx,b)
// in this step you need proof that C(ka,b) - C(ka,a) <= C(kb,b) - C(kb,a) , if not you can't use DP Divide and conquer optimization
// dp[lvl-1][ka] + C(ka,b) < dp[lvl-1][kb] + C(kb,b)  --> this is contradiction, so we can use DP optimization

// Note: in arrays dp could be dp[lvl][i] = min{dp[lvl-1][k-1] + C(k,i)}, because kopt is the point between indes k-1 and k 
// Complexity O(K*N*log(N)*C), where C: costFunction
Long costFunction(Long l, Long r){
	// return cost function
	return 0;
}

void setDP(Long lvl, Long l, Long r, Long optL, Long optR){
	if(l > r) return;
	Long med = (r + l) / 2;
	Long ans = INF; // -INF if need max, because negative ans should be optim
	Long optK = optL;
	for(Long i = optL; i <= min(optR, med); i++){
		Long val = memo[lvl - 1][i - 1] + costFunction(i,med);
		if(ans > val){
			ans = val;
			optK = i;
		}
	}
	memo[lvl][med] = ans;
	setDP(lvl,l,med - 1,optL,optK);
	setDP(lvl,med + 1,r,optK,optR);
	return;
}

// the position will be index by 1 for simplicity
void DPDivideConquer(Long lvl, Long n){
	for(Long i = 0; i <= lvl; i++){
		for(Long j = 0; j <= n; j++){
			memo[i][j] = INF; // or -INF if meed max
		}
	}
	// Warning lvl = min(lvl,n), if lvl > n no answer
	memo[0][0] = 0; // change base value if necesary 
	for(Long i = 1; i <= lvl; i++) setDP(i,1,n,1,n);
	// memo[lvl][n] menas that use exactly lvl, if you can use lvl or less
	// you need get ans betwwen 1 and lvl
	return;
}
//---------------------------- DP Divide and conquer optimization ---------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;


	return 0;
}


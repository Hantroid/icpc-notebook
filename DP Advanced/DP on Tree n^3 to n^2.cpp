// Given a tree T of N nodes and an integer K, find number of different sub trees of size less than or equal to K.
void solve(int v)
{
    Sub[v] = 1;
    dp[v][0] = 1;
    dp[v][1] = 1;

    for(auto u : childrens[v])
    {
       solve(u);

       fill(tmp , tmp + k + 1 , 0);
       for(int i = 1; i <= min(Sub[v] , k); i++)
         for(int j = 0; j <= Sub[u] && i + j <= K; j++)
          tmp[i + j] += dp[v][i] * dp[u][j];

                    Sub[v] += Sub[u];

       for(int i = 0; i <= min(K , Sub[v]); i++)
         dp[v][i] = tmp[i];
    }
}

#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	 
	// Definition
	// add bit by bit in the mask, beside looking for all subset for mask
	// S(mask,i) = S(mask, i-1)                           If bit i is off
	// S(mask,i) = S(mask, i-1) U S(mask ^ (1<<i), i-1)   If bit i is on
	
	//iterative version
	for(int mask = 0; mask < (1<<N); ++mask){
		dp[mask][-1] = A[mask];	//handle base case separately (leaf states)
		for(int i = 0; i < N; ++i){
			if(mask & (1<<i))
				dp[mask][i] = dp[mask][i-1] + dp[mask^(1<<i)][i-1];
			else
				dp[mask][i] = dp[mask][i-1];
		}
		F[mask] = dp[mask][N-1];
	}
	
	//memory optimized, super easy to code.
	for(int i = 0; i<(1<<N); ++i) F[i] = A[i];
		
	for(int i = 0;i < N; ++i){
		for(int mask = 0; mask < (1<<N); ++mask){
			if(mask & (1<<i))
				F[mask] += F[mask^(1<<i)];
		}
	}
	
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 2002;
const Long INF = 1e18;

///------------------------------------- Knuth Optimization ----------------------------------

/// O(n^3) function to Optimize to O(n^2)
/// dp[i][j] =  min(i < k < j){dp[i][k] + dp[k][j]} + C[i][j]

/// Knuth's Conditions:
/// kOpt[i,j-1] <= kOpt[i,j] <= kOpt[i+1,j]
/// Here, kOpt[i][j] = k represents the index which gives the optimal answer if we make a partition at k.
/// cost[i][j] represents the cost for the interval i to j.
/// For any a,b,c,d where a <= b <= c <= d :
/// cost[a][c] + cost[b][d] <= cost[a][d] + cost[b][c] (quadrangle inequality).
/// cost[b][c] <= cost[a][d] (monotonicity).

Long memo[MX][MX];
Long kOpt[MX][MX];

Long costFunction(Long l, Long r){
	// return costFunction
	// check (l <= r)
	return 0;
}

void KnuthOptimization(Long l, Long r){
	if(kOpt[l][r] != -1) return; // memorization
	if(l == r){
		kOpt[l][r] = l;
		// memo[l][r] = 0 // usually
		memo[l][r] = costFunction(l,r); // change base value if necessary
		return;
	}
	KnuthOptimization(l,r-1);
	KnuthOptimization(l+1,r);
	// code is when split [l,r] to [l,opt] and [opt+1,r], 
	// WARNING: if need split to [l,opt] [opt,r] or [l,opt-1] [opt+1,r] change condtions in "for" and "if"
	for(Long m = kOpt[l][r-1]; m <= kOpt[l+1][r] && m < r; m++){
		if(memo[l][r] > memo[l][m] + memo[m+1][r] + costFunction(l,r)){
			memo[l][r] = memo[l][m] + memo[m+1][r] + costFunction(l,r);
			kOpt[l][r] = m;
		}
	}
}

void initialize(Long n){
	for(Long i = 0; i < n; i++){
		for(Long j = 0; j < n; j++){
			memo[i][j] = INF;
			kOpt[i][j] = -1;
		}
	}
}

///------------------------------------- Knuth Optimization ----------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	return 0;
}

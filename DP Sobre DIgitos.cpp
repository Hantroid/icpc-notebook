#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;

ll dp[10][2];
string s;

ll f(ll pos,ll low){
	if(pos==s.size()){
		cout<<pos<<" "<<low<<" : "<<1<<endl;
		return 1;
	}
	if(dp[pos][low]!=-1){
		return dp[pos][low];
	}
	dp[pos][low]=0;
	if(low==0){
		for(ll i=0; i<=ll(s[pos]-'0'); i++){
			if(i==ll(s[pos]-'0')){
				dp[pos][low]+=f(pos+1,0);
			}else{
				dp[pos][low]+=f(pos+1,1);
			}
		}
	}else{
		for(ll i=0; i<=9; i++){
			dp[pos][low]+=f(pos+1,1);
		}
	}
	cout<<pos<<" "<<low<<" : "<<dp[pos][low]<<endl;
	return dp[pos][low];
}

int main(){
	s="11";
	for(ll i=0; i<10; i++){
		for(ll j=0; j<2; j++){
			dp[i][j]=-1;
		}
	}
	cout<<f(0,0)<<endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

struct Line{
	Long m, h;
	Line(Long m, Long h) : m(m), h(h) {}
};

struct CHT{
	vector<Line> c;
	Long posX = 0; // only for dp optimization
	Long intersect(Line a, Line b){
		Long dH = b.h - a.h;
		Long dSlope = a.m - b.m;
		return dH / dSlope + ((dH ^ dSlope) < 0 && dH % dSlope); //floored division
	}
	void add(Long m, Long h){
		Line l = Line(m,h);
		if(c.size() && m == c.back().m){
			l.h = max(h, c.back().h);
			c.pop_back();
			if(posX) posX--;
		}
	}
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	return 0;
}

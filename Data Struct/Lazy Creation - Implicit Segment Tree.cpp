#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

struct Node{
	Long val, lazy;
	Node *left;
	Node *right;
	
	Node(){
		val = lazy = 0;
		left = right = nullptr;
	}
};

Long combine(Long l, Long r){ return l + r; }

struct SegmentTree{
	Node *root;
	Long n;
	
	SegmentTree(){}
	SegmentTree(Long n) : n(n){
		root = new Node();
	}
	
	void push(Node *node, Long tl, Long tr){
		if(node->left == nullptr){
			node->left = new Node();
			node->right = new Node();
		}
		if(node->lazy != 0){
			node->val += (tr - tl + 1) * node->lazy;
			node->left->lazy += node->lazy;
			node->right->lazy += node->lazy;
			node->lazy = 0;
		}
	}
	
	Long query(Long l, Long r, Node *node, Long tl, Long tr){
		push(node,tl,tr);
		if(l <= tl && tr <= r) return node->val;
		Long tm = (tl + tr) / 2;
		if(r < tm + 1){
			return query(l,r,node->left,tl,tm);
		}else if(tm < l){
			return query(l,r,node->right,tm+1,tr);
		}
		return combine(query(l,r,node->left,tl,tm),query(l,r,node->right,tm+1,tr));
	}
	Long query(Long l, Long r){ return query(l,r,root,0,n-1); }
	
	void update(Long l, Long r, Long value, Node *node, Long tl, Long tr){
		push(node,tl,tr);
		if(tr < l || tl > r) return;
		if(l <= tl && tr <= r){
			node->val += (tr - tl + 1) * value;
			if(tl != tr){
				node->left->lazy += value;
				node->right->lazy += value;
			}
			return;
		}
		Long tm = (tl + tr) / 2;
		update(l,r,value,node->left,tl,tm);
		update(l,r,value,node->right,tm+1,tr);
		node->val = combine(node->left->val,node->right->val);
	}
	void update(Long l, Long r, Long value){ update(l,r,value,root,0,n-1); }
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;


	return 0;
}


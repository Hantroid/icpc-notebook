#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

struct mnxStack{
	stack< pair<Long,Long> > s1, s2;
	Long f(Long a, Long b){
		return min(a,b);
	}
	Long get(){
		if(s1.empty() || s2.empty()){
			return (s1.empty() ? s2.top().second : s1.top().second);
		}
		return f(s1.top().second,s2.top().second);
	}
	void push(Long val){
		Long element = (s1.empty() ? val : f(val,s1.top().second));
		s1.push({val,element});
	}
	void pop(){
		if(s2.empty()){
			while(!s1.empty()){
				Long element = s1.top().first;
				s1.pop();
				Long val = s2.empty() ? element : f(element,s2.top().second);
				s2.push({element,val});
			}
		}
		Long remove_element = s2.top().first;
		s2.pop();
	}
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


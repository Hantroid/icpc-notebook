#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
using namespace std;
using namespace __gnu_pbds;

typedef long long Long;

typedef tree<Long, null_type, less<Long> , rb_tree_tag, tree_order_statistics_node_update> ordered_set;

ordered_set X;
//find_by_order(k) -> return iterator to the k-th element (0-indexed) - O(log n)
//order_of_key(num) -> # of items strictly smaller than num - O(log n)
int main() {
	
	X.insert(1);
    X.insert(2);
    X.insert(4);
    X.insert(8);
    X.insert(16);

    cout<<*X.find_by_order(1)<<endl; // 2
    cout<<*X.find_by_order(2)<<endl; // 4
    cout<<*X.find_by_order(4)<<endl; // 16
    cout<<(X.end() == X.find_by_order(6))<<endl; // true = 1

    cout<<X.order_of_key(-5)<<endl;  // 0
    cout<<X.order_of_key(1)<<endl;   // 0
    cout<<X.order_of_key(3)<<endl;   // 2
    cout<<X.order_of_key(4)<<endl;   // 2
    cout<<X.order_of_key(400)<<endl; // 5
	return 0;
}




#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 1e5 + 10;
const Long MX2 = 21 * MX; //O(n + Q log n)

struct Node{
	Long ans, pref, suff, tot;
	Node(){}
	Node(Long ans) : ans(ans){
		pref = suff = ans;
		tot = 1;
	}
};

Node combine(const Node &A, const Node &B){
	Node C;
	C.pref = A.pref;
	if(A.tot == A.pref) C.pref = A.pref + B.pref;
	C.suff = B.suff;
	if(B.tot == B.suff) C.suff = A.suff + B.suff;
	C.ans = max({A.ans, B.ans, A.suff + B.pref});
	C.tot = A.tot + B.tot;
	return C;
}

struct SegmentTree {
	Long n, nodes, lastVersion;
	Node st[MX2];
	Long roots[MX];
	Long left[MX2];
	Long right[MX2];
	
	Long build(vector<Long> &v, Long tl, Long tr) { //O(n)
		Long node = nodes++;
		if(tl == tr){
			st[node] = Node(v[tl]);
			return node;
		}
		Long tm = (tl + tr) / 2;
		left[node] = build(v, tl, tm);
		right[node] = build(v, tm + 1, tr);
		st[node] = combine(st[left[node]], st[right[node]]);
		return node;
	}
	
	void build(vector<Long> &v){
		nodes = 1;
		n = v.size();
		roots[0] = build(v, 0, n - 1);
		lastVersion = 0;
	}

	Node query(Long l, Long r, Long node, Long tl, Long tr) { //O(log n)
		if(l <= tl && tr <= r) return st[node];
		Long tm = (tl + tr) / 2;
		if(r < tm + 1) return query(l, r, left[node], tl, tm);
		else if(tm < l) return query(l, r, right[node], tm + 1, tr);
		return combine(query(l, r, left[node], tl, tm), query(l, r, right[node], tm + 1, tr));
	}
	
	Node query(Long l , Long r, Long version = -1) { 
		//query in the version (or the last version if it's -1)
		assert(n > 0);
		if(version == -1) version = lastVersion;
		return query(l, r, roots[version], 0, n - 1);
	}

	Long update(Long pos, Long val, Long prevNode, Long tl, Long tr) { //O(log n)
		Long node = nodes++;
		if(tl == tr){
			st[node] = Node(val);
			return node;
		}
		Long tm = (tl + tr) / 2;
		left[node] = left[prevNode];
		right[node] = right[prevNode];
		if(pos <= tm) left[node] = update(pos, val, left[prevNode], tl, tm);
		else right[node] = update(pos, val, right[prevNode], tm + 1, tr);
		st[node] = combine(st[left[node]], st[right[node]]);
		return node;
	}
	
	void update(Long pos, Long val, Long version = -1) {
		//update a past version and append the new version to the history
		//(or update the last version if it's -1)
		assert(n > 0);
		if(version == -1) version = lastVersion;
		lastVersion++;
		roots[lastVersion] = update(pos, val, roots[version], 0, n - 1);
	}	
}st;

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

// build O(n)
// update O(log(n)) in range
// get O(log(n)) in range

const ll N=100005;
ll st[N*4];
ll A[N];

void build(ll l, ll r, ll nodo){
	if(l==r){
		st[nodo]=A[r];
		return;
	}
	ll med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo]=0;
	return;
}

void update(ll ini, ll fin, ll val, ll l, ll r, ll nodo){
	if(ini>fin) return;
	if(ini==l && r==fin){
		st[nodo]+=val;
	}else{
		ll med=(l+r)/2;
		update(ini,min(r,med),val,l,med,2*nodo);
		update(max(l,med+1),r,val,med+1,r,2*nodo+1);
	}
	return;
}

ll get(ll pos, ll l, ll r, ll nodo){
	if(l==r) return st[nodo];
	ll med=(l+r)/2;
	if(pos<=med){
		return st[nodo]+get(pos,l,med,nodo*2);
	}
	return st[nodo]+get(pos,med+1,r,nodo*2+1);
}

int main(){
	ll n,m;
	cin>>n;
	for(ll i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	cin>>m;
	for(ll i=0; i<m; i++){
		ll x,y,z;
		cin>>x>>y;
		if(x==1){
			ll val;
			cin>>z>>val;
			update(y-1,z-1,val,0,n-1,1);
		}else{
			cout<<get(y-1,0,n-1,1)<<endl;
		}
	}
	return 0;
}


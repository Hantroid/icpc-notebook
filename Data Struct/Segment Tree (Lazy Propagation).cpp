#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

// build O(n)
// update O(log(n))
// query O(log(n))

const Long N=100005;
const Long INF=1e18;
Long st[N<<2];
Long lazy[N<<2];
Long A[N];

void push(Long l, Long r, Long nodo){
	if(lazy[nodo]!=0){
		st[nodo]+=(r-l+1)*lazy[nodo];
		if(l!=r){
			lazy[nodo*2]+=lazy[nodo];
			lazy[nodo*2+1]+=lazy[nodo];
		}
		lazy[nodo]=0;
	}
	return;
}

void build(Long l, Long r, Long nodo){
	if(l==r){
		st[nodo]=A[r];
		return;
	}
	Long med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo]=st[nodo*2]+st[nodo*2+1];
	return;
}

void update(Long ini, Long fin, Long val, Long l, Long r, Long nodo){
	push(l,r,nodo);
	if(fin<l || r<ini) return;
	if(ini<=l && r<=fin){
		st[nodo]+=(r-l+1)*val;
		if(l!=r){
			lazy[nodo*2]+=val;
			lazy[nodo*2+1]+=val;
		}
		return;
	}
	Long med=(l+r)/2;
	update(ini,fin,val,l,med,2*nodo);
	update(ini,fin,val,med+1,r,2*nodo+1);
	st[nodo]=st[nodo*2]+st[nodo*2+1];
	return;
}

Long query(Long ini, Long fin, Long l, Long r, Long nodo){
	push(l,r,nodo);
	if(fin<l || r<ini) return 0;
	if(ini<=l && r<=fin) return st[nodo];
	Long med=(l+r)/2;
	Long L=query(ini,fin,l,med,nodo*2);
	Long R=query(ini,fin,med+1,r,nodo*2+1);
	return L+R;
}

int main(){
	Long n,m;
	cin>>n;
	for(Long i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	Long x,y,z;
	cin>>m;
	for(Long i=0; i<m; i++){
		/*cout<<"-----------------------"<<endl;
		for(Long j=0; j<n; j++){
			cout<<query(j,j,0,n-1,1)<<" ";
		}
		cout<<endl;
		cout<<"-----------------------"<<endl;*/
		cin>>x>>y>>z;
		if(x==1){
			Long val;
			cin>>val;
			update(y-1,z-1,val,0,n-1,1);
		}else{
			cout<<query(y-1,z-1,0,n-1,1)<<endl;
		}
	}
	return 0;
}


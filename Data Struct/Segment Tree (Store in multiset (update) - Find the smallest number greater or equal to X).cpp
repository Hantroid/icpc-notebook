#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

// build n*log(n)^2
// query log(n)^2
// update log(n)^2

const ll N=100005;
const ll INF=1e18;

ll A[N];
multiset<ll> st[N<<2];

void build(ll l, ll r, ll nodo){
	if(l==r){
		st[nodo].insert(A[l]);
		return;
	}
	ll med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo].insert(st[nodo*2].begin(),st[nodo*2].end());
	st[nodo].insert(st[nodo*2+1].begin(),st[nodo*2+1].end());
	return;
}

ll query(ll ini, ll fin, ll val, ll l , ll r, ll nodo){
	if(fin<l || r<ini) return INF;
	if(ini<=l && r<=fin){
		multiset<ll> :: iterator pos=st[nodo].lower_bound(val);
		if(pos==st[nodo].end()){
			return INF;
		}
		return *pos;
	}
	ll med=(l+r)/2;
	ll L=query(ini,fin,val,l,med,nodo*2);
	ll R=query(ini,fin,val,med+1,r,nodo*2+1);
	return min(L,R);
}

void update(ll pos, ll val, ll l, ll r, ll nodo){
	st[nodo].erase(st[nodo].find(A[pos]));
	st[nodo].insert(val);
	if(l==r){
		A[pos]=val;
	}else{
		ll med=(l+r)/2;
		if(pos<=med){
			update(pos,val,l,med,nodo*2);
		}else{
			update(pos,val,med+1,r,nodo*2+1);
		}
	}
	return;
}

int main(){
	ll n,m;
	cin>>n;
	for(ll i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	cin>>m;
	ll x,y,z;
	for(ll i=0; i<m; i++){
		cin>>x;
		if(x==1){
			cin>>y>>z;
			y--;
			update(y,z,0,n-1,1);
		}else{
			ll val;
			cin>>y>>z>>val;
			y--;
			z--;
			ll ans=query(y,z,val,0,n-1,1);
			if(ans==INF){
				cout<<"No existe"<<endl;
			}else{
				cout<<ans<<endl;
			}
		}
	}
	return 0;
}


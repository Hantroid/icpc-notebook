#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

// build n*log(n)
// query log(n)^2

const Long N=100005;
const Long INF=1e18;

Long A[N];
vector<Long> st[N<<2];

void build(Long l, Long r, Long nodo){
	if(l==r){
		st[nodo]=vector<Long> (1,A[l]);
		return;
	}
	Long med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	merge(st[nodo*2].begin(),st[nodo*2].end(),st[nodo*2+1].begin(),st[nodo*2+1].end(),back_inserter(st[nodo]));
	return;
}

Long query(Long ini, Long fin, Long val, Long l , Long r, Long nodo){
	if(fin<l || r<ini) return INF;
	if(ini<=l && r<=fin){
		Long pos=lower_bound(st[nodo].begin(),st[nodo].end(),val)-st[nodo].begin();
		if(pos==st[nodo].size()){
			return INF;
		}
		return st[nodo][pos];
	}
	Long med=(l+r)/2;
	Long L=query(ini,fin,val,l,med,nodo*2);
	Long R=query(ini,fin,val,med+1,r,nodo*2+1);
	return min(L,R);
}

int main(){
	Long n,m;
	cin>>n;
	for(Long i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	cin>>m;
	Long x,y,z;
	for(Long i=0; i<m; i++){
		cin>>x;
		if(x==1){
			cin>>y>>z;
			y--;
			//update(y,z,0,n-1,1);
		}else{
			Long val;
			cin>>y>>z>>val;
			y--;
			z--;
			Long ans=query(y,z,val,0,n-1,1);
			if(ans==INF){
				cout<<"No existe"<<endl;
			}else{
				cout<<ans<<endl;
			}
		}
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100005;
ll st[N*4];
ll A[N];

void build(ll l, ll r, ll nodo){
	if(l==r){
		if(A[r]==0){
			st[nodo]=1;
		}else{
			st[nodo]=0;
		}
		return;
	}
	ll med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo]=st[nodo*2]+st[2*nodo+1];
	return;
}

void update(ll a, ll v, ll l, ll r, ll nodo){
	if(a<l || r<a) return;
	if(l==r){
		if(v==0){
			st[nodo]=1;
		}else{
			st[nodo]=0;
		}
		return;
	}
	ll med=(l+r)/2;
	update(a,v,l,med,2*nodo);
	update(a,v,med+1,r,2*nodo+1);
	st[nodo]=st[2*nodo]+st[2*nodo+1];
	return;
}

ll find_kth(ll k, ll l, ll r, ll nodo){
	if(st[nodo]<k) return -1;
	if(l==r) return l;
	ll med=(l+r)/2;
	if(st[nodo*2]>=k){
		return find_kth(k,l,med,nodo*2);
	}
	return find_kth(k-st[nodo*2],med+1,r,nodo*2+1);
}

int main(){
	ll n,m;
	cin>>n;
	for(ll i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	cin>>m;
	ll x,y,z;
	for(ll i=0; i<m; i++){
		cin>>x;
		if(x==1){
			cin>>y>>z;
			update(y-1,z,0,n-1,1);
		}else{
			cin>>y;
			cout<<find_kth(y,0,n-1,1)+1<<endl;
		}
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100005;

struct data{
	ll sum,pref,suff,ans;
};

ll A[N];
data st[N<<2];

data combine(data l, data r){
	data res;
    res.sum = l.sum + r.sum;
    res.pref = max(l.pref, l.sum + r.pref);
    res.suff = max(r.suff, r.sum + l.suff);
    res.ans = max(max(l.ans, r.ans), l.suff + r.pref);
	return res;
}

data make_data(ll val){
	data res;
    res.sum = val;
    res.pref = res.suff = res.ans = max(0ll, val);
	return res;
}

void build(ll l, ll r, ll nodo){
	if(l==r){
		st[nodo]=make_data(A[l]);
		return;
	}
	ll med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo]=combine(st[nodo*2],st[2*nodo+1]);
	return;
}

void update(ll pos, ll val, ll l, ll r, ll nodo){
	if(pos<l || r<pos) return;
	if(l==r){
		st[nodo]=make_data(val);
		return;
	}
	ll med=(l+r)/2;
	update(pos,val,l,med,2*nodo);
	update(pos,val,med+1,r,2*nodo+1);
	st[nodo]=combine(st[2*nodo],st[2*nodo+1]);
	return;
}

data query(ll ini, ll fin, ll l , ll r, ll nodo){
	if(fin<l || r<ini) return make_data(0);
	if(ini<=l && r<=fin) return st[nodo];
	ll med=(l+r)/2;
	data L=query(ini,fin,l,med,nodo*2);
	data R=query(ini,fin,med+1,r,nodo*2+1);
	return combine(L,R);
}

int main(){
	ll n,m;
	cin>>n;
	for(ll i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	cin>>m;
	ll x,y,z;
	for(ll i=0; i<m; i++){
		cin>>x;
		if(x==1){
			cin>>y>>z;
			y--;
			update(y,z,0,n-1,1);
		}else{
			cin>>y>>z;
			y--;
			z--;
			data ans=query(y,z,0,n-1,1);
			cout<<"Suma = "<<ans.sum<<endl;
			cout<<"maxPref = "<<ans.pref<<endl;
			cout<<"maxSuff = "<<ans.suff<<endl;
			cout<<"maxSubSeg = "<<ans.ans<<endl;
		}
	}
	return 0;
}


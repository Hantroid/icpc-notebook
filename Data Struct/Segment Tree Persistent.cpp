#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 2e5 + 10;
const Long MX2 = 19 * MX;

struct SegmentTree{
	int roots[MX];
	Long st[MX2];
	int left[MX2];
	int right[MX2];
	int maxN, lastVersion, nodes;
	
	Long combine(Long x, Long y){ return x + y; }
	
	int build(vector<Long> &a, int tl, int tr){
		int node = nodes++;
		if(tl == tr){
			st[node] = a[tl];
			return node;
		}else{
			Long tm = (tl + tr) / 2;
			left[node] = build(a, tl, tm);
			right[node] = build(a, tm + 1, tr);
			st[node] = combine(st[left[node]], st[right[node]]);
			return node;
		}
	}
	
	void build(vector<Long> &a){
		nodes = 1;
		maxN = a.size();
		roots[0] = build(a,0,maxN-1);
		lastVersion = 0;
	}
	
	Long query(int l, int r, int node, int tl, int tr){
		if(l <= tl && tr <= r) return st[node];
		int tm = (tl + tr) / 2;
		if(r < tm + 1){
			return query(l, r, left[node], tl, tm);
		}else if(tm < l){
			return query(l, r, right[node], tm + 1, tr);
		}else{
			return combine(query(l, r, left[node], tl, tm), query(l, r, right[node], tm + 1, tr));
		}
	}
	
	Long query(int l, int r, int version = -1){
		assert(maxN > 0);
		if(version == -1) version = lastVersion;
		return query(l, r, roots[version], 0, maxN - 1);
	}
	
	int update(int pos, Long val, int prevNode, int tl, int tr){
		int node = nodes++;
		if(tl == tr){
			st[node] = val;
			return node;
		}else{
			int tm = (tl + tr) / 2;
			if(pos <= tm){
				left[node] = update(pos, val, left[prevNode], tl, tm);
				right[node] = right[prevNode];
			}else{
				left[node] = left[prevNode];
				right[node] = update(pos, val, right[prevNode], tm + 1, tr);
			}
			st[node] = combine(st[left[node]], st[right[node]]);
			return node;
		}
	}
	
	void update(int pos, int val, int version = -1){
		assert(maxN>0);
		if(version == -1) version = lastVersion;
		lastVersion++;
		roots[lastVersion] = update(pos, val, roots[version], 0, maxN - 1);
	}
}ST;

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	return 0;
}

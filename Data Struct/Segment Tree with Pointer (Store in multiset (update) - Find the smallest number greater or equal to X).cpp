#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 2e5 + 10;

Long A[MX];

struct Node{
	vector<Long> val;
	Node *left;
	Node *right;
	
	Node(){
		left = right = nullptr;
	}
};

struct SegmentTree{
	Node *root;
	Long n;
	
	SegmentTree(){}
	SegmentTree(Long n) : n(n){
		root = new Node();
	}
	
	void build(Node *node, Long tl, Long tr){
		if(node->left == nullptr){
			node->left = new Node();
			node->right = new Node();
		}
		if(tl == tr){
			node->val = vector<Long> (1,A[tl]);// add a global array
			return;
		}
		Long tm = (tr + tl) / 2;
		build(node->left,tl,tm);
		build(node->right,tm+1,tr);
		merge(node->left->val.begin(),node->left->val.end(),node->right->val.begin(),node->right->val.end(),back_inserter(node->val));
	}
	void build(){ build(root,0,n-1); }
	
	Long query(Long l, Long r, Long value, Node *node, Long tl, Long tr){
		if(tr < l || tl > r) return 0;
		if(l <= tl && tr <= r){
			return tr - tl + 1 - (upper_bound(node->val.begin(),node->val.end(),value) - node->val.begin());
		}
		Long tm = (tl + tr) / 2;
		Long L = query(l,r,value,node->left,tl,tm);
		Long R = query(l,r,value,node->right,tm+1,tr);
		return L + R;
	}
	Long query(Long l, Long r, Long value){ return query(l,r,value,root,0,n-1); }
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	
	return 0;
}


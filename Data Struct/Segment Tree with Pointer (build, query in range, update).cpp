#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX =

Long A[MX];

struct Node{
	Long val;
	Node *left;
	Node *right;
	
	Node(Long val) : val(val){
		left = right = nullptr;
	}
};

Long combine(Long x, Long y){ return x + y; }

struct SegmentTree{
	Long identity;
	Node *root;
	Long n;
	
	SegmentTree(){}
	SegmentTree(Long n, Long identity) : n(n), identity(identity){
		root = new Node(identity);
	}
	
	void push(Node *node){
		if(node->left == nullptr){
			node->left = new Node(identity);
			node->right = new Node(identity);
		}
	}
	
	void build(Node *node, Long tl, Long tr){
		if(tl == tr){
			node->val = A[tl]; // add a global array
			return;
		}
		push(node);
		Long tm = (tr + tl) / 2;
		build(node->left,tl,tm);
		build(node->right,tm+1,tr);
		node->val = combine(node->left->val,node->right->val);
	}
	void build(){ build(root,0,n-1); }
	
	Long query(Long l, Long r, Node *node, Long tl, Long tr){
		if(l <= tl && tr <= r) return node->val;
		Long tm = (tl + tr) / 2;
		if(r < tm + 1){
			return query(l,r,node->left,tl,tm);
		}else if(tm < l){
			return query(l,r,node->right,tm+1,tr);
		}
		return combine(query(l,r,node->left,tl,tm),query(l,r,node->right,tm+1,tr));
	}
	Long query(Long l, Long r){ return query(l,r,root,0,n-1); }
	
	void update(Long l, Long r, Long value, Node *node, Long tl, Long tr){
		if(tr < l || tl > r) return;
		if(l <= tl && tr <= r){
			node->val = value;
			return;
		}
		Long tm = (tl + tr) / 2;
		update(l,r,value,node->left,tl,tm);
		update(l,r,value,node->right,tm+1,tr);
		node->val = combine(node->left->val,node->right->val);
	}
	void update(Long l, Long r, Long value){ update(l,r,value,root,0,n-1); }
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;


	return 0;
}


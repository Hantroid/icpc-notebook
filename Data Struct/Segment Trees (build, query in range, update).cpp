#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

// build O(n)
// query O(log(n)) in range
// update O(log(n)) only 1 vlaue

const Long N=100005;
Long st[N*4];
Long A[N];

void build(Long l, Long r, Long nodo){
	if(l==r){
		st[nodo]=A[r];
		return;
	}
	Long med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo]=st[nodo*2]+st[2*nodo+1];
	return;
}

void update(Long a, Long v, Long l, Long r, Long nodo){
	if(a<l || r<a) return;
	if(l==r){
		st[nodo]=v;
		return;
	}
	Long med=(l+r)/2;
	update(a,v,l,med,2*nodo);
	update(a,v,med+1,r,2*nodo+1);
	st[nodo]=st[2*nodo]+st[2*nodo+1];
	return;
}

/*si solo se quiere tomar el valor izquiero o dereho sin agregar un neutro
se debe analizar el dezplazamiento del nodo padre a los nodos de los hijos

esto puede ocurrir cuando no se encuentra un valor neutro

HINT: (modificar)
	Long L=query(ini,min(fin,med),l,med,nodo*2);
	Long R=query(max(ini,med+1),fin,med+1,r,nodo*2+1);
	if(l<=ini && ini<=med && med+1<=fin && fin<=r){
		return combine(L,R);
	}else if(l<=ini && ini<=med){
		return L;
	}
	return R;
*/

Long query(Long ini, Long fin, Long l, Long r, Long nodo){
	if(fin<l || r<ini) return 0;
	if(ini<=l && r<=fin) return st[nodo];
	Long med=(l+r)/2;
	Long L=query(ini,fin,l,med,nodo*2);
	Long R=query(ini,fin,med+1,r,nodo*2+1);
	return R+L;
}

// Note:
// find MEX in range - Offline
// update in order: first value x where last[x] < l, last[x] = is the
// the last position where the number x was

int main(){
	Long n,m;
	cin>>n;
	for(Long i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	cin>>m;
	for(Long i=0; i<m; i++){
		Long x,y,z;
		cin>>x>>y>>z;
		if(x==1){
			update(y-1,z,0,n-1,1);
		}else{
			cout<<query(y-1,z-1,0,n-1,1)<<endl;
		}
	}
	return 0;
}


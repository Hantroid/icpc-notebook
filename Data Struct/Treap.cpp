#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MAXINT = 1e9;

mt19937_64  rng(chrono::steady_clock::now().time_since_epoch().count());
Long random(Long a, Long b){ return uniform_int_distribution<Long>(a , b)(rng); }

// all keys should be diferent, if the keys could be repeat so the key should be a pair(key, frequency)
// the key never change

struct item{
    Long key, prior;
    item *l, *r;
    item(){}
    item(Long key, Long prior) : key(key), prior(prior), l(nullptr), r(nullptr) {}
};
typedef item* pitem;

void split(pitem t, Long key, pitem &l, pitem &r){ // O(log(N))
    if(t == nullptr){
    	l = r = nullptr;
	}else if(key < t->key){  // with "<" split [..., k3, key] [k4, ...], but wiht "<=" split [..., k3] [key, k4, ...]
		split(t->l, key, l, t->l), r = t;
	}else{
		split(t->r, key, t->r, r), l = t;
	}
}

void merge(pitem &t, pitem l, pitem r){  // O(log(N))
	if(l == nullptr || r == nullptr){
		t = l ? l : r;
	}else if(l->prior > r->prior){
		merge(l->r, l->r, r), t = l;
	}else{
		merge(r->l, l, r->l), t = r;
	}
}

void insert(pitem &t, pitem it){  // O(log(N))
	if(t == nullptr){
		t = it;
	}else if(it->prior > t->prior){
		split(t, it->key, it->l, it->r), t = it;
	}else{
		insert(it->key < t->key ? t->l : t->r, it);
	}
}

void erase(pitem &t, Long key){  // O(log(N))
	if(t == nullptr) return;
	if(t->key == key){
		merge(t, t->l, t->r);
	}else{
		erase(key < t->key ? t->l : t->r, key);
	}
}

pitem unite(pitem l, pitem r){  // O(Mlog(N/M))
    if(!l || !r) return l ? l : r;
    if(l->prior < r->prior) swap(l, r);
    pitem lt, rt;
    split(r, l->key, lt, rt);
    l->l = unite(l->l, lt);
    l->r = unite(l->r, rt);
    return l;
}

void heapify(pitem t){
	if(t == nullptr) return;
	pitem max = t;
	if(t->l != nullptr && t->l->prior > max->prior) max = t->l;
	if(t->r != nullptr && t->r->prior > max->prior) max = t->r;
	if(max != t){
		swap(t->prior, max->prior);
		heapify(max);
	}
}

pitem build(vector<Long> &a, Long l, Long r){ // O(N)
	if(l > r) return nullptr;
	Long med = (r + l) / 2;
	pitem t = new item(a[med],random(0,MAXINT));
	t->l = build(a, l, med - 1);
	t->r = build(a, med + 1, r);
	heapify(t);
	//updNode(t); if necesary
	return t;
}

void printNode(pitem node){
	if(node == nullptr){
		cout << "( - , - )";
	}else{
		cout << "(" << node->key << ", " << node->prior << ")";
	}
}

void printTreap(pitem root){
	if(root == nullptr) return;
	if(root->l != nullptr){
		printNode(root);
		cout << " -> ";
		printNode(root->l);
		cout << " L " << endl;
	}
	printTreap(root->l);
	if(root->r != nullptr){
		printNode(root);
		cout << " -> ";
		printNode(root->r);
		cout << " R " << endl;
	}
	printTreap(root->r);
}
//const Long MX = ;

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	vector<Long> a = {3,5,7,9,13,14,17,19,21,26,28};
	
	pitem root = build(a,0,a.size()-1);
	
	//printTreap(root);

	/*pitem root = new item(20,60 - 5);
	root = unite(root,new item(10,60 - 10));
	root = unite(root,new item(7,60 - 60));
	root = unite(root,new item(12,60 - 40));
	root = unite(root,new item(23,60 - 32));
	root = unite(root,new item(35,60 - 30));
	root = unite(root,new item(55,60 - 42));
	root = unite(root,new item(1,-2));
	root = unite(root,new item(8,-3));
	root = unite(root,new item(11,15));
	root = unite(root,new item(14,12));
	root = unite(root,new item(21,26));
	root = unite(root,new item(25,3));
	root = unite(root,new item(40,9));
	root = unite(root,new item(60,2));*/
	
	return 0;
}


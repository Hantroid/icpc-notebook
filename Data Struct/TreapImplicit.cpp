#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MAXINT = 1e9;
const Long INF = 1e18;
mt19937_64  rng(chrono::steady_clock::now().time_since_epoch().count());
Long random(Long a, Long b){ return uniform_int_distribution<Long>(a , b)(rng); }

typedef struct item* pitem;
struct item{
	Long prior, value, cnt;
	bool rev;
	pitem l, r;
	item(){}
	item(Long value, Long prior) : value(value) , prior(prior), l (nullptr), r(nullptr) , cnt(0), rev(false){}
};

// getCnt help to find the position in the Treap
// Also could create getSum to get the sum in subtree, 
// and should write some fuction in updNode and push to update the Sum and Value store in that node
Long getCnt(pitem it){ return it ? it->cnt : 0; }

void push(pitem it){
	if(it != nullptr && it->rev){
		it->rev = false;
		swap(it->l,it->r);
		if(it->l) it->l->rev ^= true;
		if(it->r) it->r->rev ^= true;
	}
}

void updNode(pitem it){
	if(it == nullptr) return;
	push(it->l); // update the left child to ensure that was update and with lazy == 0, for example when update sum in range
	push(it->r); // update the right child to ensure that was update and with lazy == 0
	//it->sum = getSum(it->l) + getSum(it->r) + it->value; // it->value = vaule in this position
	it->cnt = getCnt(it->l) + getCnt(it->r) + 1;
}

void merge(pitem & t, pitem l, pitem r){
	push(l);
	push(r);
	if(!l || !r){
		t = l ? l : r;
	}else if(l->prior > r->prior){
		merge(l->r, l->r, r), t = l;
	}else{
		merge(r->l, l, r->l), t = r;
	}
    updNode(t);
}

void split(pitem t, pitem &l, pitem &r, Long key, Long add = 0){
	if (t == nullptr){
		l = r = nullptr;
		return;
	}
	push(t);
	Long curKey = add + getCnt(t->l);
	if(key <= curKey){   // with "<" split [..., k3, key] [k4, ...], but wiht "<=" split [..., k3] [key, k4, ...]
		split(t->l, l, t->l, key, add), r = t;
	}else{
		split(t->r, t->r, r, key, curKey + 1), l = t;
	}
	updNode(t);
}

struct Treap{
	pitem tree;
	
	Treap(){ tree = nullptr; }
	
	void insert(Long key, Long value){ // key = position where insert
		Long prior = random(0, MAXINT);
		pitem t = new item(value, prior);
		pitem t1, t2;
		split(tree, t1, t2, key);
		merge(t1, t1, t);
		merge(tree, t1, t2);
	}
	
	void erase(pitem &t, Long key, Long add = 0){ // key = position will be erase
		if(t == nullptr) return;
		push(t);
		Long curKey = add + getCnt(t->l);
		if(curKey == key) merge(t, t->l , t->r);
		if(key < curKey) erase(t->l, key, add);
		if(key > curKey) erase(t->r, key , curKey + 1);
		updNode(t);
	}
	void erase(Long key){ erase(tree, key); }
	
	Long search(pitem t, Long key, Long add = 0){ // get the value in postion key
		if(t == nullptr) return -INF;
		push(t);
		Long curKey = add + getCnt(t->l);
		if(curKey == key) return t->value;
		if(key < curKey) return search(t->l, key , add);
		return search(t->r ,key, curKey + 1);
	}
	Long search(Long key){ return search(tree, key); }
	
	void replace(pitem &t, Long key, Long val , Long add = 0){ // change the value in position key
		if(t == nullptr) return;
		push(t);
		Long curKey = add + getCnt(t -> l);
		if(curKey == key){
			t->value = val;
			updNode(t); // for update the sum in subtree
			return;
		}
		if(key < curKey) replace(t->l, key, val, add);
		if(key > curKey) replace(t->r, key, val, curKey + 1);
		updNode(t); // for update the sum in subtree
	}
	void replace(Long key, Long val){ return replace(tree, key, val); }

	void reverse(Long l, Long r){
		if(l > r) return;
	    pitem t1, t2, t3;
	    split(tree, t1, t2, l); // split tree in t1 and t2, t1 = [0,1,...,l-1] and t2 = [l,l+1,...,n-1]
	    // r - l + 1 represent where split and not r, because before were splited 
	    // aditionally go to the next position to get t2 = [l,l+1,...,r-1,r]
	    split(t2, t2, t3, r - l + 1); 
	    t2->rev ^= true;
	    merge(tree, t1, t2);
	    merge(tree, tree, t3);
	}
	
	void output(pitem t){
	    if(t == nullptr)  return;
	    push(t);
	    output(t->l);
	    printf("%d ", t->value);
	    output(t->r);
	}
	void output(){ output(tree); }
};


int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;


	return 0;
}

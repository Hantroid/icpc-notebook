#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MAXINT = 1e9;

mt19937_64  rng(chrono::steady_clock::now().time_since_epoch().count());
Long random(Long a, Long b){ return uniform_int_distribution<Long>(a , b)(rng); }

// all keys should be diferent, if the keys could be repeat so the key should be a pair(key, frequency)
// the key never change
// could be get position with 0-index, an the list is sort ascendingly

struct item{
    Long key, prior, cnt;
    item *l, *r;
    item(){}
    item(Long key, Long prior) : key(key), prior(prior), cnt(0), l(nullptr), r(nullptr) {}
};
typedef item* pitem;

Long getCnt(pitem it){ return it ? it->cnt : 0; }

void updCnt(pitem it){
    if(it == nullptr) return;
    it->cnt = getCnt(it->l) + getCnt(it->r) + 1;
}

void split(pitem t, Long key, pitem &l, pitem &r){ // O(log(N))
    if(t == nullptr){
    	l = r = nullptr;
	}else if(key <= t->key){
		split(t->l, key, l, t->l), r = t;
	}else{
		split(t->r, key, t->r, r), l = t;
	}
	updCnt(t);
}

void merge(pitem &t, pitem l, pitem r){  // O(log(N))
	if(l == nullptr || r == nullptr){
		t = l ? l : r;
	}else if(l->prior > r->prior){
		merge(l->r, l->r, r), t = l;
	}else{
		merge(r->l, l, r->l), t = r;
	}
	updCnt(t);
}

void insert(pitem &t, pitem it){  // O(log(N))
	if(t == nullptr){
		t = it;
	}else if(it->prior > t->prior){
		split(t, it->key, it->l, it->r), t = it;
	}else{
		insert(it->key < t->key ? t->l : t->r, it);
	}
	updCnt(t);
}

void erase(pitem &t, Long key){  // O(log(N))
	if(t == nullptr) return;
	if(t->key == key){
		merge(t, t->l, t->r);
	}else{
		erase(key < t->key ? t->l : t->r, key);
	}
	updCnt(t);
}

// Search the value of key in position "pos" 0-index
Long searchValue(pitem t, Long pos, Long add = 0){ // O(log(N))
	if(t == nullptr) return -1;
	Long curPos = add + getCnt(t->l);
	if(curPos == pos) return t->key;
	if(pos < curPos) return searchValue(t->l, pos , add);
	return searchValue(t->r, pos, curPos + 1);
}

// Search the position of key with 0-index
Long searchPosition(pitem t, Long key, Long add = 0){ // O(log(N))
	if(t == nullptr) return -1;
	Long curPos = add + getCnt(t->l);
	if(key == t->key) return curPos + 1;
	if(key < t->key) return searchPosition(t->l, key, add);
	return searchPosition(t->r, key, curPos + 1);
}
//const Long MX = ;

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	pitem root = nullptr;
	
	Long t, x;
	while(cin >> t){
		if(t == -1) break;
		cin >> x;
		if(t == 1){
			pitem node = new item(x,random(0,MAXINT));
			insert(root,node);
		}else if(t == 2){
			erase(root, x);
		}else if(t == 3){
			cout << searchPosition(root,x,0) << "\n";
		}else{
			cout << searchValue(root,x-1,0) << "\n";
		}
	}
	return 0;
}

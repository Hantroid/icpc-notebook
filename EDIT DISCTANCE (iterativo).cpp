#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;

string a,b;
ll dp[2010][2010];

/*ll f(ll n, ll m){
	if(n==0){
		return m;
	}
	if(m==0){
		return n;
	}
	if(dp[n][m]!=-1){
		return dp[n][m];
	}
	if(a[n-1]==b[m-1]){
		dp[n][m]=f(n-1,m-1);
	}else{
		dp[n][m]=1+min(f(n-1,m-1),min(f(n-1,m),f(n,m-1)));
	}
	return dp[n][m];
}*/

void solve(){
	getline(cin,a);
	getline(cin,b);
	//cout<<f(a.size(),b.size())<<endl;
	for(ll i=0; i<=a.size(); i++){
		dp[i][0]=i;
	}
	for(ll i=0; i<=b.size(); i++){
		dp[0][i]=i;
	}
	for(ll i=1; i<=a.size(); i++){
		for(ll j=1; j<=b.size(); j++){
			if(a[i-1]==b[j-1]){
				dp[i][j]=dp[i-1][j-1];
			}else{
				dp[i][j]=1+min(dp[i-1][j-1],min(dp[i-1][j],dp[i][j-1]));
			}
		}
	}
	cout<<dp[a.size()][b.size()]<<endl;
	return;
}

int main(){
	ll t;
	cin>>t;
	getline(cin,a);
	for(ll i=0; i<t; i++){
		solve();
	}
	return 0;
}


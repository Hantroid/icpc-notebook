#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=100001;

Long gcd(Long a, Long b, Long &x, Long &y){
	if(a == 0){
		x = 0;
		y = 1;
		return b;
	}
	Long x1, y1;
	Long d = gcd(b%a,a,x1,y1);
	x = y1 - (b/a)*x1;
	y = x1;
	return d;
}

int main(){
	Long a, b, x, y;
	while(cin >> a >> b){
		Long d = gcd(a,b,x,y);
		cout << x << " " << y << " " << d << endl;
		cout << "Ecuacion -> " << a << " * " << x << " + " << b << " * " << y << " = " << d << endl;
	}
	return 0;
}


#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

const Long INF = 1e18;

// ------------------ agregar y quitar rangos con set ---------------------

//OBS: si se quiere dividir de la forma (l,x) + (x,r) ver recomendaciones
set< pair<Long,Long> > rangos;

void divide(Long x, Long tipo){
	auto it = rangos.upper_bound({x,INF});
	if(it != rangos.begin()){
		it--;
		Long l = (*it).first;
		Long r = (*it).second;
		if(tipo == 0){ // divide la izq
			if(l < x && x <= r){ // cambiar a "l <= x" si se quiere q dividir (l,x) + (x,r) 
				rangos.erase(it);
				rangos.insert({l,x-1}); // si se quiere q dividir (l,x) + (x,r) quitar el -1
				rangos.insert({x,r});
			}
		}else{ // divide la der
			if(l <= x && x < r){ // cambiar a "x <= r" si se quiere q dividir (l,x) + (x,r) 
				rangos.erase(it);
				rangos.insert({l,x});
				rangos.insert({x+1,r}); // si se quiere q dividir (l,x) + (x,r) quitar el +1
			}
		}
	}
	return;
}

void agrega_quita(Long l, Long r, Long tipo){
	divide(l,0);
	divide(r,1);
	auto ll = rangos.lower_bound({l,-1ll});
	auto rr = rangos.upper_bound({r,INF}); // si se quiere dividir (l,x) + (x,r) cambiar {r,INF} por {r,-1}
	//proceso previo antes de eliminacion (si es q se requiera)
	/*for(auto it = ll; it != rr; it++){
		cout << ":: " << (*it).first << " " << (*it).second << endl;
	}*/
	rangos.erase(ll,rr);
	if(tipo == 0){ // agrega
		rangos.insert({l,r});
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    Long x, y, t;
    while(true){
    	cin >> x >> y >> t;
    	agrega_quita(x,y,t);
    	cout << "------------" << endl;
    	for(auto it : rangos){
    		cout << it.first << " " << it.second << endl;
		}
		cout << "------------" << endl;
	}
	return 0;
}


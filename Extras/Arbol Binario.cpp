#include <bits/stdc++.h>
#define ALL(x) x.begin(),x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c))-(c).begin())
using namespace std;
typedef long long ll;

const ll N=2001+10;

vector<ll> A;
ll tree[N];
ll l[N];
ll r[N];
ll nodo=0;

ll crea(ll n){
	tree[nodo]=n;
	l[nodo]=-1;
	r[nodo]=-1;
	return nodo++;
}

void upd(ll raiz, ll n){
	if(n<tree[raiz]){
		if(l[raiz]==-1){
			l[raiz]=crea(n);
		}else{
			upd(l[raiz],n);
		}
	}
	if(n>tree[raiz]){
		if(r[raiz]==-1){
			r[raiz]=crea(n);
		}else{
			upd(r[raiz],n);
		}
	}
	return;
}

int main(){
	ll n,x;
	cin>>n;
	ll raiz;
	for(ll i=0; i<n; i++){
		cin>>x;
		A.push_back(x);
	}
	
	for(ll i=0; i<A.size(); i++){
		if(i==0){
			raiz=crea(A[i]);
		}else{
			upd(raiz,A[i]);
		}
	}
	
	return 0;
}


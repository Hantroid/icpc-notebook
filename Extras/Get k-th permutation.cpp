#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

Long fact[21]; // fill factorial

// k = 0 -> first permutation
vector<Long> getPermutation(Long n, Long k){
	vector<Long> ans;
	vector<Long> s;
	
	// k-th permutation doesn't exist
	if(k >= fact[n]) return ans; 
	
	for(Long i = 0; i < n; i++) s.push_back(i + 1);
	while(n--){
		Long pos = k / fact[n];
		ans.push_back(s[pos]);
		s.erase(s.begin() + pos);
		k -= pos * fact[n];
	}
	return ans;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	fact[0] = 1;
	for(Long i = 1; i < 21; i++) fact[i] = fact[i-1] * i;
	
	vector<Long> perm = getPermutation(6,2);
	for(Long xx : perm) cout << xx << " " ;
	cout << "\n";
	return 0;
}

#include <bits/stdc++.h>
using namespace std;

long long lazy[100001<<2];
long long st[100001<<2];
long long A[100001];

void build(long long l, long long r, long long nodo){
	if(l==r){
		st[nodo]=A[l];
		return;
	}
	long long med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo]=st[nodo*2]+st[nodo*2+1];
	return;
}

void update(long long ini, long long fin, long long v, long long l, long long r, long long nodo){
	if(lazy[nodo]!=0){
		st[nodo]=st[nodo]+(r-l+1)*lazy[nodo];
		if(l!=r){
			lazy[nodo*2]=lazy[nodo*2]+lazy[nodo];
			lazy[nodo*2+1]=lazy[nodo*2+1]+lazy[nodo];
		}
		lazy[nodo]=0;
	}
	if(fin<l || r<ini) return;
	if(ini<=l && r<=fin){
		st[nodo]=st[nodo]+(r-l+1)*v;
		if(l!=r){
			lazy[nodo*2]=lazy[nodo*2]+v;
			lazy[nodo*2+1]=lazy[nodo*2+1]+v;
		}
		return;
	}
	long long med=(l+r)/2;
	update(ini,fin,v,l,med,nodo*2);
	update(ini,fin,v,med+1,r,nodo*2+1);
	st[nodo]=st[nodo*2]+st[nodo*2+1];
	return;
}

long long query(long long ini, long long fin, long long l, long long r, long long nodo){
	if(lazy[nodo]!=0){
		st[nodo]=st[nodo]+(r-l+1)*lazy[nodo];
		if(l!=r){
			lazy[nodo*2]=lazy[nodo*2]+lazy[nodo];
			lazy[nodo*2+1]=lazy[nodo*2+1]+lazy[nodo];
		}
		lazy[nodo]=0;
	}
	if(fin<l || r<ini) return 0;
	if(ini<=l && r<=fin) return st[nodo];
	long long med=(l+r)/2;
	long long L=query(ini,fin,l,med,nodo*2);
	long long R=query(ini,fin,med+1,r,nodo*2+1);
	return L+R;
}

void solve(){
	long long n,m;
	cin>>n>>m;
	for(long long i=0; i<n; i++){
		A[i]=0;
	}
	for(long long i=0; i<(n<<2); i++){
		lazy[i]=0;
	}
	build(0,n-1,1);
	
	for(long long i=0; i<m; i++){
		long long f;
		cin>>f;
		if(f==0){
			long long x,y,v;
			cin>>x>>y>>v;
			x--;
			y--;
			update(min(x,y),max(x,y),v,0,n-1,1);
		}else{
			long long x,y;
			cin>>x>>y;
			x--;
			y--;
			cout<<query(min(x,y),max(x,y),0,n-1,1)<<endl;
		}
	}
	return;
}

int main(){
	long long n;
	cin>>n;
	for(long long i=0; i<n; i++){
		solve();
	}
	return 0;
}


 /*
 tets cases
 2
8 6
0 2 4 26
0 4 8 80
0 4 5 20
1 8 8
0 5 7 14
1 4 8
8 6
0 2 4 26
0 4 8 80
0 4 5 20
1 8 8
0 5 7 14
1 4 8

80
508
80
508
 */

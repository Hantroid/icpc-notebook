#include <bits/stdc++.h>
#define c 1000000007
using namespace std;

long long f[100011];
long long A[100011];
long long B[100011];
long long C[100011];
long long D[100011];
long long E[100011];

int main(){
	long long n,q;
	cin>>n>>q;
	cin>>f[1]>>f[2];
	long long a,b;
	cin>>a>>b;
	for(long long i=3; i<=100010; i++){
		f[i]=(b*f[i-1]+a*f[i-2])%c;
	}
	for(long long i=1; i<=n; i++){
		cin>>A[i];
	}
	long long l,r;
	for(long long i=0; i<q; i++){
		cin>>l>>r;
		B[l]=(B[l]+f[1])%c;
		C[l+1]=(C[l+1]+b*f[1]+c-f[2])%c;
		D[r+1]=(D[r+1]+f[r-l+2])%c;
		E[r+2]=(E[r+2]+b*f[r-l+2]+c-f[r-l+3])%c;
	}
	for(long long i=2; i<=n; i++){
		B[i]=(B[i]+c-C[i]+b*B[i-1]+a*B[i-2])%c;
		D[i]=(D[i]+c-E[i]+b*D[i-1]+a*D[i-2])%c;
	}
	for(long long i=1; i<=n; i++){
		cout<<(A[i]+B[i]+c-D[i]+c)%c<<" ";
	}
	cout<<endl;
	return 0;
}


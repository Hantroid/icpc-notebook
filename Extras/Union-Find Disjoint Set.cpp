#include <bits/stdc++.h>
#define ALL(x) x.begin(),x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c))-(c).begin())
using namespace std;
typedef long long ll;

vector<ll> adj[100];
ll parent[100];
ll rnk[100];

ll findSet(ll i){
	if(parent[i]==i){
		return parent[i];
	}
	parent[i]=findSet(parent[i]);
	return parent[i];
}

bool isSameSet(ll i, ll j){
	if(findSet(i)==findSet(j)){
		return true;
	}
	return false;
}

void unionSet(ll i, ll j){
	if(!isSameSet(i,j)){
		ll x=findSet(i);
		ll y=findSet(j);
		if(rnk[x]>rnk[y]){
			parent[y]=x;
		}else{
			parent[x]=y;
			if(rnk[x]==rnk[y]){
				rnk[y]++;
			}
		}
	}
	return;
}

int main(){
	ll n,m;
	cin>>n>>m;
	for(ll i=0; i<m; i++){
		ll x,y;
		cin>>x>>y;
		x--;
		y--;
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	for(ll i=0; i<n; i++){
		parent[i]=i;
	}
	return 0;
}


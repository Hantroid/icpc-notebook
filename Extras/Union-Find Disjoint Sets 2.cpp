#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

vector<Long> parent,rnk;

//complejidad por query O(1) por amortizacion
//tambien se puede realizar dsu en arreglos uniendo el menor arreglo al mayor arreglo
//dando una coplejidad O(nlogn)al unir todos los componentes, del caul se puede obtener informacion util
//si es guardado en un set se puede saber en que momento un par se une
//ejemplo (el tiempo minimo en q se une un par para varias queries -> usar set) prob : M ->  https://codeforces.com/gym/102021

void UnionFind(Long n){
	for(Long i=0; i<n; i++){
		rnk[i]=0;
		parent[i]=i;
	}
	return;
}

Long findSet(Long i){
	if(parent[i]==i) return i;
	parent[i]=findSet(parent[i]);
	return parent[i];
}

void unionSet(Long i, Long j){
	Long x = findSet(i);
	Long y = findSet(j);
	if(y != x){
		if(rnk[x] <= rnk[y]) swap(x,y);
		parent[y] = x;
		if(rnk[x] == rnk[y]) rnk[y]++;
	}
	return;
}

int main(){

	return 0;
}


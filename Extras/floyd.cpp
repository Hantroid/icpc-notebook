const int INF = 100;
int m[N][N], n, l;

void floyd(){
	for(int k = 0; k < n; k++){
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				m[i][j] = min(m[i][j], m[i][k] + m[k][j]);
			}
		}
	}
}

int main(){
	cin >> n >> l;
	
	for(int i = 0; i < n; i++)
		for(int j = 0; j < n; j++)
			m[i][j] = INF;
	
	for(int i = 0; i < n; i++)
		m[i][i] = 0;
	
	for(int i = 0; i < l; i++){
		int x, y, p;
		cin >> x >> y >> p;
		x--; y--;
		m[x][y] = m[y][x] = p;
	}
	
	floyd();
	
	for(int i = 0; i < n; i++){
	 	for(int j = 0; j < n; j++){
	 		cout << m[i][j] << " ";
	 	}
		cout<<endl;
	}
	
	return 0;
}

#include <bits/stdc++.h>
#define N 100000
using namespace std;

int main(){
	int n;
	cin >> n;
	for( int i = 2; i*i<=n; ++i ){
		if( n % i == 0){
			int exp = 0;
			while( n % i == 0 ){
				n = n / i;
				exp++;
			}
			cout << i << " ^ " << exp << endl;
		}
	}
	if( n != 1 ){
		cout << n << " ^ " << 1 << endl; 
	}
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

//------------------------------- Dinic with demands (lower and upper bound flow) -------------------------------
//Complexity = O(V*V*E)
//Memory = 7*E
//  En caso de que la capacidad de flujo en todas las aristas es 1, entonces
//  Complexity = O(E*sqrt(V))
struct FlowEdge{
	Long v, u;
	Long cap, flow = 0, demand;
	FlowEdge(Long v, Long u, Long cap, Long demand) : v(v), u(u), cap(cap), demand(demand) {}
};

struct DinicDemand{
	Long flow_INF = 1e18;
	vector<FlowEdge> edges;
	vector<vector<Long>> adj;
	Long n, m = 0, s, t, ss, tt, totDemand = 0;
	vector<Long> level, ptr;
	queue<Long> q;
	
	// range of nodes [0,n>
	DinicDemand(){}
	DinicDemand(Long nn, Long s, Long t) : n(nn+2), s(s), t(t) { 
		adj.resize(n);
		level.resize(n);
		ptr.resize(n);
		ss = n - 2;
		tt = n - 1;
		add_edge2(t,s,flow_INF,0);
	}
	
	// flow "v" -> "u"
	// demand = inferior limit, cap = superior limit
	void add_edge(Long v, Long u, Long cap, Long demand){
		assert(demand <= cap);
		add_edge2(v,u,cap-demand,demand);
		add_edge2(ss,u,demand,0);
		add_edge2(v,tt,demand,0);
		totDemand += demand;
	}
	void add_edge2(Long v, Long u, Long cap, Long demand){
		edges.emplace_back(v,u,cap,demand);
		edges.emplace_back(u,v,0,0);
		adj[v].push_back(m);
		adj[u].push_back(m+1);
		m += 2;
	}
	
	bool bfs(Long T){
		while(!q.empty()){
			Long v = q.front();
			q.pop();
			for(Long id : adj[v]){
				if(edges[id].cap - edges[id].flow < 1) continue;
				if(level[edges[id].u] != -1) continue;
				level[edges[id].u] = level[v] + 1;
				q.push(edges[id].u);
			}
		}
		return level[T] != -1;
	}
	
	Long dfs(Long v, Long pushed, Long T){
		if(pushed == 0) return 0;
		if(v == T) return pushed;
		for(Long& cid = ptr[v]; cid < (Long)adj[v].size(); cid++){
			Long id = adj[v][cid];
			Long u = edges[id].u;
			if(level[v]+1 != level[u] || edges[id].cap - edges[id].flow < 1) continue;
			Long tr = dfs(u,min(pushed, edges[id].cap - edges[id].flow), T);
			if(tr == 0) continue;
			edges[id].flow += tr;
			edges[id^1].flow -= tr;
			return tr;
		}
		return 0;
	}
	
	Long getFlow(Long S, Long T){
		Long f = 0;
		while(true){
			fill(level.begin(),level.end(),-1);
			level[S] = 0;
			q.push(S);
			if(!bfs(T)) break;
			fill(ptr.begin(),ptr.end(),0);
			while(Long pushed = dfs(S,flow_INF,T)){
				f += pushed;
			}
		}
		return f;
	}
	
	// flow = -1: it's impossible get flow with edges capacity (lower and upper bound)
	// flow >= 0: it's possible get flow with edges capacity (lower and upper bound) where is maximum
	Long flow(){ 
		if(getFlow(ss,tt) != totDemand) return -1;
		getFlow(s,t);
		Long f = 0;
		for(FlowEdge e : edges){
			if(e.v == s && e.u < n - 2 && e.flow + e.demand > 0){
				f += e.flow + e.demand;
			}
		}
		return f;
	}
	
	void printGraph(){ // Flow graph but edges with lower and upper bound
		for(FlowEdge e : edges){
			if(e.v < n - 2 && e.u < n - 2 && e.flow + e.demand >= 0){
				cout << e.v << " -> " << e.u << " - flow = " << e.flow + e.demand << " | lower bound = " << e.demand << " | upper_bound = " << e.cap + e.demand << endl;
			}
		}
	}
};
//------------------------------- Dinic with demands (lower and upper bound flow) -------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Long tot = 10;
	Long ss = 0;
	Long tt = 9;
	DinicDemand G = DinicDemand(tot, ss, tt);
	//G.add_edge(u,v,maximum_flow_can_pass,minimum_flow_should_pass);
	
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

//------------------------------- Dinic -------------------------------
//Complexity = O(V*V*E)
//Memory = 7*E
//  En caso de que la capacidad de flujo en todas las aristas es 1, entonces
//  Complexity = O(E*sqrt(V))
struct FlowEdge{
	Long v, u;
	Long cap, flow = 0;
	FlowEdge(Long v, Long u, Long cap) : v(v), u(u), cap(cap) {}
};

struct Dinic{
	Long flow_INF = 1e18;
	vector<bool> group;
	vector<FlowEdge> edges;
	vector< vector<Long> > adj;
	Long n, m = 0, s, t;
	vector<Long> level, ptr;
	queue<Long> q;
	// los nodos van de [0,n>
	Dinic(){}
	Dinic(Long n, Long s, Long t) : n(n), s(s), t(t) { 
		adj.resize(n);
		level.resize(n);
		ptr.resize(n);
		group.resize(n,false);
	}
	// agrega el flujo de "v" -> "u" con capcacidad "cap"
	void add_edge(Long v, Long u, Long cap){
		edges.emplace_back(v,u,cap);
		edges.emplace_back(u,v,0);
		adj[v].push_back(m);
		adj[u].push_back(m+1);
		m += 2;
	}
	
	bool bfs(){
		while(!q.empty()){
			Long v = q.front();
			q.pop();
			for(Long id : adj[v]){
				if(edges[id].cap - edges[id].flow < 1) continue;
				if(level[edges[id].u] != -1) continue;
				level[edges[id].u] = level[v] + 1;
				q.push(edges[id].u);
			}
		}
		return level[t] != -1;
	}
	
	Long dfs(Long v, Long pushed){
		if(pushed == 0) return 0;
		if(v == t) return pushed;
		for(Long& cid = ptr[v]; cid < (Long)adj[v].size(); cid++){
			Long id = adj[v][cid];
			Long u = edges[id].u;
			if(level[v]+1 != level[u] || edges[id].cap - edges[id].flow < 1) continue;
			Long tr = dfs(u,min(pushed, edges[id].cap - edges[id].flow));
			if(tr == 0) continue;
			edges[id].flow += tr;
			edges[id^1].flow -= tr;
			return tr;
		}
		return 0;
	}
	
	Long flow(){
		Long f = 0;
		while(true){
			fill(level.begin(),level.end(),-1);
			level[s] = 0;
			q.push(s);
			if(!bfs()) break;
			fill(ptr.begin(),ptr.end(),0);
			while(Long pushed = dfs(s,flow_INF)){
				f += pushed;
			}
		}
		return f;
	}
	
	void dfs2(Long u){
		group[u] = true;
		for(Long id : adj[u]){
			Long v  = edges[id].u;
			Long df = edges[id].cap - edges[id].flow;
			if(df > 0 && !group[v]){
				dfs2(v);
			}
		}
	}
	
	void getCut(){
		dfs2(s);
	}
};
//------------------------------- Dinic -------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 101;
const Long INF = 1e10;

vector<Long> adj[MX];
Long cap[MX][MX];
bool vis[MX];
bool M[MX][MX];

void limpia(Long n){
	for(Long i = 0; i <= n; i++){
		adj[i].clear();
		for(Long j = 0; j <= n; j++){
			M[i][j] = false;
			cap[i][j] = 0;
		}
	}
	return;
}

void add_edge(Long u, Long v, Long uv, Long vu){
	if(!M[u][v]) adj[u].push_back(v);
	if(!M[v][u]) adj[v].push_back(u);
	M[u][v] = M[v][u] = true;
	cap[u][v] += uv;
	cap[v][u] += vu;
	return;
}

Long dfs(Long u, Long f, Long t){
	if(u == t) return f;
	if(vis[u]) return 0;
	vis[u] = true;
	Long sz = adj[u].size();
	for(Long i = 0; i < sz; i++){
		Long v = adj[u][i];
		if(cap[u][v] == 0) continue;
		Long ret = dfs(v,min(f,cap[u][v]),t);
		if(ret > 0){
			cap[u][v] -= ret;
			cap[v][u] += ret;
			return ret;
		}
	}
	return 0;
}

Long maxFlow(Long s, Long t, Long n){
	Long flow = 0;
	while(true){
		for(Long i = 0; i <= n; i++){
			vis[i] = false;
		}
		Long inc = dfs(s,INF,t);
		if(inc == 0) break;
		flow += inc;
	}
	return flow;
}

void solve(Long n, Long T){
	limpia(n);
	Long s,t,c;
	scanf("%lld %lld %lld",&s,&t,&c);
	Long x,y,p;
	for(Long i=0; i<c; i++){
		scanf("%lld %lld %lld",&x,&y,&p);
		add_edge(x,y,p,p);
	}
	printf("Network %lld\n",T);
	printf("The bandwidth is %lld.\n",maxFlow(s,t,n));
	printf("\n");
	return;
}

int main(){
	Long n;
	Long t;
	t=1;
	while(cin>>n){
		if(n==0){
			break;
		}
		solve(n,t);
		t++;
	}
	return 0;
}



#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

struct MCMF{
	Long INF = 1e18;
	Long n;
	vector<Long> prio, curflow, prevedge, prevnode, pot;
	priority_queue< Long,vector<Long>,greater<Long> > q;
	struct edge{
		Long to, rev, f, cap, cost;
		edge(){}
		edge(Long to, Long rev, Long f, Long cap, Long cost) : to(to), rev(rev), f(f), cap(cap), cost(cost){}
	};
	vector< vector<edge> > g;
	MCMF(Long n) : n(n), prio(n), curflow(n), prevedge(n), prevnode(n), pot(n), g(n) {}
	void add_edge(Long u, Long v, Long cap, Long cost){
		g[u].push_back( edge(u, g[v].size(), 0, cap, cost) );
		g[v].push_back( edge(v, g[u].size() - 1, 0, 0, -cost) );
	}
	pair<Long,Long> get_flow(Long s, Long t){
		Long flow = 0, flow_cost = 0;
		while(true){
			q.push(s);
			fill(prio.begin(),prio.end(),INF);
			prio[s] = 0; curflow[s] = INF;
			while(!q.empty()){
				Long cur = q.top();
				Long d = (cur>>32ll), u = (cur % (1ll<<32ll));
				q.pop();
				if(d != prio[u]) continue;
				for(Long i = 0; i < g[u].size(); i++){
					edge &e = g[u][i];
					Long v = e.to;
					if(e.cap <= e.f) continue;
					Long nprio = prio[u] + e.cost + pot[u] - pot[v];
					if(prio[v] > nprio){
						prio[v] = nprio;
						q.push((nprio<<32ll) + v);
						prevnode[v] = u; prevedge[v] = i;
						curflow[v] = min(curflow[u], e.cap - e.f);
					}
				}
			}
			if(prio[t] == INF) break;
			for(Long i = 0; i < n; i++) pot[i] += prio[i];
			Long df = min(curflow[t], INF - flow);
			flow += df;
			for(Long v = t; v != s; v = prevnode[v]){
				edge &e = g[prevnode[v]][prevedge[v]];
				e.f += df;
				g[v][e.rev].f -= df;
				flow_cost += df * e.cost;
			}
		}
		return {flow,flow_cost};
	}
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


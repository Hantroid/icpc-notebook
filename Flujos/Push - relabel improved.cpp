#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

//------------------------------- Push - Relabel improved -------------------------------
//Complexity = O(V*E + V*V*sqrt(E)), worst case O(V^3)
//Memory = 2*(V^2 + V)
struct PushRelabel{
	// IMPORTANTE: esto solo funciona cuando solo existe un "sink"
	// en caso de haber muchas "sink" cuya cantidad es de x, se crea una "sink global" con flow_INF
	// y se crea aristas de la "sink global" a las "sink" individuales, con un flujo de "flof_INF/x"
	// esto es debido al push q se hace en las aristas, donde "flow_INF/x" es mayor q el maximo flujo
	Long n, s, t;
	const Long flow_INF = 1e18;
	vector< vector<Long> > capacity, flow;
	vector< Long > height, excess;
	// los nodos van de [0,n>
	PushRelabel(Long n, Long s, Long t) : n(n), s(s), t(t) {
		height.assign(n,0);
		excess.assign(n,0);
		flow.assign(n,vector<Long>(n,0));
		capacity.assign(n,vector<Long>(n,0));
	}
	
	void push(Long u, Long v){
		Long d = min(excess[u],capacity[u][v] - flow[u][v]);
		flow[u][v] += d; flow[v][u] -= d;
		excess[u] -= d; excess[v] += d;
		return;
	}
	
	void relabel(Long u){
		Long d = flow_INF;
		for(Long i = 0; i < n; i++){
			if(capacity[u][i] - flow[u][i] > 0){
				d = min(d,height[i]);
			}
		}
		if(d < flow_INF) height[u] = d + 1;
		return;
	}
	
	vector<Long> find_max_height_vertices(){
		vector<Long> max_height;
		for(Long i = 0; i < n; i++){
			if(i != s && i != t && excess[i] > 0){
				if(!max_height.empty() && height[i] > height[max_height[0]]){
					max_height.clear();
				}
				if(max_height.empty() || height[i] == height[max_height[0]]){
					max_height.push_back(i);
				}
			}
		}
		return max_height;
	}
	
	void add_edge(Long u, Long v, Long uv, Long vu = 0){
		capacity[u][v] += uv;
		capacity[v][u] += vu;
		return;
	}
	
	Long max_flow(){
		height[s] = n;
		excess[s] = flow_INF;
		for(Long i = 0; i < n; i++){
			if(i != s) push(s,i);
		}
		
		vector<Long> current;
		while(!(current = find_max_height_vertices()).empty()){
			for(Long i : current){
				bool pushed = false;
				for(Long j = 0; j < n && excess[i]; j++){
					if(capacity[i][j] - flow[i][j] > 0 && height[i] == height[j] + 1){
						push(i,j);
						pushed = true;
					}
				}
				if(!pushed){
					relabel(i);
					break;
				}
			}
		}
		
		Long max_flow = 0;
		for(Long i = 0; i < n; i++){
			max_flow += flow[s][i];
		}
		return max_flow;
	}
};
//------------------------------- Push - Relabel improved -------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


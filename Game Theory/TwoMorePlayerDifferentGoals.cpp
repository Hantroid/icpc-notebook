#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const MX = 1e5;
const PLAYERS = 2;

//-------------------------------- Two More Players With Differing Goals -------------------------------

bool memo[MX][PLAYERS];
bool used[MX][PLAYERS];

// memo[state][t] = represents the state "state" after the "player t" move
// to kown if player 1 wins or not, check for all next valid move is true (memo[0][apply(currentState,move)] == true)
bool dp(Long state, Long t){
	if(used[t][state]) return memo[t][state];
	used[t][state] = true;
	
	if(baseState(state)){ // define valid state for each player
		if(t == 0){
			memo[t][state] = true;
		}else{
			memo[t][state] = false;
		}
		return memo[t][state];
	}
	
	if(used[t][state]) return memo[t][state];
	used[t][state] = true;
	
	bool ans = true;
	for(auto move : moves){
		// Usually if there is any win state when the state is "apply(state,move)" for the next player 
		// means that this state is a lose position for player "t" because in the next turn the next player can reach a win position
		if(checkValidMove(state,move)) // check is a valid move
		if(dp(apply(state,move),nxtPlayer)){
			ans = false;
		}
	}
	
	memo[t][state] = ans;
	return ans;
}

//-------------------------------- Two More Players With Differing Goals -------------------------------

int main(){
	return 0;
}


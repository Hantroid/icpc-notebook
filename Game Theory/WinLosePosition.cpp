#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 111;

//--------------------------------- Win Lose Position ---------------------------------
// - check if impartial game (all players have the same rule, the same goal and alterning turns)
// - check if there is any simmetry strategy, could be better

vector<Long> moves = {2,3,5};
bool memo[MX];
bool used[MX];

bool dp(Long u){
	if(u < 2) return false; // base state could be true or false
	if(used[u]) return memo[u];
	used[u] = true;
	bool winPosition = false;
	for(Long move : moves){
		if(u - move >= 0){ // check if is a valid position
			winPosition |= !dp(u - move);
		}
	}
	memo[u] = winPosition;
	return winPosition;
}

//--------------------------------- Win Lose Position ---------------------------------

int main(){
	
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;

const ld EPS=1e-9;
const ld PI=acos(-1.0);

struct point{
	ld x,y;
	//constructor para una estructura vacia
	point(){x=y=0.0;}
	//constructor para una estructura con datos
	point(ld x, ld y): x(x), y(y){}
	//crea operadores +,-,*,/,+=,-=,*=,/=
	point& operator +=(const point &t){
		x+=t.x;
		y+=t.y;
		return *this;
	}
	point& operator -=(const point &t){
		x-=t.x;
		y-=t.y;
		return *this;
	}
	point& operator *=(ld t){
		x*=t;
		y*=t;
		return *this;
	}
	point& operator /=(ld t){
		x/=t;
		y/=t;
		return *this;
	}
	point operator +(const point &t) const{
		return point(*this)+=t;
	}
	point operator -(const point &t) const{
		return point(*this)-=t;
	}
	point operator *(ld t) const{
		return point(*this)*=t;
	}
	point operator /(ld t) const{
		return point(*this)/=t;
	}
	//crea operador de >,<,==
	bool operator <(point other) const{
		if(fabs(x-other.x)>EPS) return x<other.x;
		else return y<other.y;
	}
	bool operator ==(point other) const{
		return (fabs(x-other.x)<EPS && fabs(y-other.y)<EPS);
	}
};
//cambia el orden de la multiplicaicion
point operator*(ld a, point b) {
    return b * a;
}

//distancia entre puntos
ld dist(point p1, point p2){
	return hypot(p1.x-p2.x,p1.y-p2.y);
}

//rotacion de un punto con respecto al 0,0 en theta grados
// sentido antihorario
point rotate(point p, ld theta){
	ld rad=(theta*PI)/180.0;
	return point(p.x*cos(rad)-p.y*sin(rad),p.x*sin(rad)+p.y*cos(rad));
}



int main(){
	point a,b;
	a=point(2,1);
	b=point(7,8);
	ll aux=3;
	a=aux*a;
	printf("%LF %LF\n",a.x,a.y);
	a=a/aux;
	printf("%LF %LF\n",a.x,a.y);
	a-=b;
	printf("%LF %LF\n",a.x,a.y);
	a=rotate(a,90);
	printf("%LF %LF\n",a.x,a.y);
	return 0;
}


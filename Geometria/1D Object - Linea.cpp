#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;

const ld EPS=1e-9;
const ld PI=acos(-1.0);

const ll N=100001;

struct point{
	ld x,y;
	//constructor para una estructura vacia
	point(){x=y=0.0;}
	//constructor para una estructura con datos
	point(ld x, ld y): x(x), y(y){}
};

struct line{
	ld a,b,c;
	line(){}
	line(ld a, ld b, ld c): a(a), b(b), c(c){}
};

void pointsToLine(point p1, point p2, line &l){
	if(fabs(p1.x-p2.x)<EPS){ // linea vertical
		l.a=1.0;
		l.b=0.0;
		l.c=-p1.x;
	}else{
		l.a=-(p1.y-p2.y)/(p1.x-p2.x);
		l.b=1.0; // important: poner el valor de b en 1.0
		l.c=-(l.a*p1.x)-p1.y;
	}
	return;
}

// el l.b=1.0 ayuda para simplificar la ecuacion de la recta y asi poder comparar si es paralelo
// o si es una misma linea
bool areParallel(line l1, line l2){ // revisa el coeficiente a y b
	return (fabs(l1.a-l2.a)<EPS && fabs(l1.b-l2.b)<EPS);
}

bool areSame(line l1, line l2){ // si son paralelos y el coeficiente c
	return areParallel(l1,l2) && (fabs(l1.c-l2.c)<EPS);
}

// si dos lineas son paralelas, intersecta en ningun punto o en infinitos puntos
//si dos lineas se intersectan en un punto
bool areIntersectAPoint(line l1, line l2, point &p){
	if(areParallel(l1,l2)) return false;
	p.x=(l2.b*l1.c-l1.b*l2.c)/(l2.a*l1.b-l1.a*l2.b);
	if(fabs(l1.b)>EPS){
		p.y=-(l1.a*p.x+l1.c);
	}else{
		p.y=-(l2.a*p.x+l2.c);
	}
	return true;
}



int main(){
	point p1=point(1,1);
	point p2=point(0,0);
	line l1,l2;
	pointsToLine(p1,p2,l1);
	cout<<l1.a<<" "<<l1.b<<" "<<l1.c<<endl;
	pointsToLine(p2,p1,l1);
	cout<<l1.a<<" "<<l1.b<<" "<<l1.c<<endl;
	point p3=point(1,1);
	point p4=point(2,0);
	pointsToLine(p3,p4,l2);
	point p;
	if(areIntersectAPoint(l1,l2,p)){
		cout<<p.x<<" "<<p.y<<endl;
	}else{
		cout<<"no existe"<<endl;
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;

const ld EPS=1e-9;
const ld PI=acos(-1.0);

const ll N=100001;

struct point{
	ld x,y;
	//constructor para una estructura vacia
	point(){x=y=0.0;}
	//constructor para una estructura con datos
	point(ld x, ld y): x(x), y(y){}
};

ld dist(point p1, point p2){
	return hypot(p1.x-p2.x,p1.y-p2.y);
}

struct line{
	ld a,b,c;
	line(){}
	line(ld a, ld b, ld c): a(a), b(b), c(c){}
};

struct vec{
	ld x,y;
	vec(){}
	vec(ld x, ld y): x(x), y(y){}
};

// crea un vector en base a dos puntos
// el vector es a -> b
vec toVec(point a, point b){
	return vec(b.x-a.x,b.y-a.y);
}

//modifica la escala de un vector
//si es s<0.0 la direccion del vector tambn cambia
vec scale(vec v, ld s){
	return vec(v.x*s,v.y*s);
}

// traslada el punto p al punto p+v;
point translate(point p, vec v){
	return point(p.x+v.x,p.y+v.y);
}

// retorna el producto escalar a.b=|a||b|cos(x)
ld dot(vec a, vec b){
	return (a.x*b.x+a.y*b.y);
}

// retorna la norma^2  = |v|^2 
ld norm_sq(vec v){
	return (v.x*v.x+v.y*v.y);
}

//los puntos a y b deben ser distintos
// halla la distancia de un punto P a la recta que pasa por A-B
ld distToLine(point p, point a, point b, point &c){
	vec ap=toVec(a,p);
	vec ab=toVec(a,b);
	ld u=dot(ap,ab)/norm_sq(ab); // u es la escala del vector A->B para hallar el punto C
	c=translate(a,scale(ab,u)); // C es el punto que pertenece a la recta A-B que es mas cercano a P
	return dist(p,c);
}


ld distToLineSegment(point p, point a, point b, point &c){
	vec ap=toVec(a,p);
	vec ab=toVec(a,b);
	ld u=dot(ap,ab)/norm_sq(ab);
	if(u<0.0){ // si la escala es negativa la distancia minima del punto P al segmento es dist(P,A)
		c=a;
		return dist(p,a);
	}
	if(u>1.0){// si la escala es mayor a 1 la distancia minima del punto P al segmento es dist(P,B)
		c=b;
		return dist(p,b);
	}
	return distToLine(p,a,b,c);// en los demas casos distancia minima del punto P al segmento es distToLine
}

// retorna el angulo en radianes
// OA.OB=|OA|*|OB|*cos(x)
// el punto O!=punto A  && punto B!=punto O
ld angle(point a, point o, point b){ 
	vec oa=toVec(o,a);
	vec ob=toVec(o,b);
	return acos(dot(oa,ob)/sqrt(norm_sq(oa)*norm_sq(ob)));
}

// halla el area de 3 puntos en base a 2 vectores
ld cross(vec a, vec b){
	return a.x*b.y-a.y*b.x;
}

// retorna verdad si el punto r esta al lado izquiero de la recta pq
bool ccw(point p, point q, point r){
	return cross(toVec(p,q),toVec(p,r))>0;
}

// retorna verdad si el punto r esta en la mimsma recta que pq
bool collinear(point p, point q, point r){
	return fabs(cross(toVec(p,q),toVec(p,r)))<EPS;
}


int main(){
	point p1=point(0,0);
	point p2=point(3,4);
	cout<<"Punto 1: "<<p1.x<<" "<<p1.y<<endl;
	cout<<"Punto 2: "<<p2.x<<" "<<p2.y<<endl;
	vec v1,v2;
	v1=toVec(p1,p2);
	cout<<"Vector 1: "<<v1.x<<" "<<v1.y<<endl;
	v2=toVec(p2,p1);
	cout<<"Vector 2: "<<v2.x<<" "<<v2.y<<endl;
	v1=scale(v1,8);
	cout<<"Vector 1 con escala 8: "<<v1.x<<" "<<v1.y<<endl;
	v2=scale(v2,4);
	cout<<"Vector 2 con escala 4: "<<v2.x<<" "<<v2.y<<endl;
	p2=translate(p2,v1);
	cout<<"Punto 2 trasladado en Vector 1 con escala 8: "<<p2.x<<" "<<p2.y<<endl;
	cout<<"------------------------------"<<endl;
	p1=point(0,3);
	p2=point(10,3);
	point p3=point(0,0);
	point p4;
	cout<<"Punto 1: "<<p1.x<<" "<<p1.y<<endl;
	cout<<"Punto 2: "<<p2.x<<" "<<p2.y<<endl;
	cout<<"Punto 3: "<<p3.x<<" "<<p3.y<<endl;
	vec v=toVec(p1,p2);
	cout<<"Vector: "<<v.x<<" "<<v.y<<endl;
	cout<<norm_sq(v)<<endl;
	cout<<"Distancia minima: "<<distToLine(p3,p1,p2,p4)<<endl;
	cout<<"Punto de la recta donde la distancia a p3 es minima: "<<p4.x<<" "<<p4.y<<endl;
	cout<<"------------------------------"<<endl;
	p1=point(0,3);
	p2=point(10,3);
	p3=point(1,0);
	cout<<distToLineSegment(p3,p1,p2,p4)<<endl;
	cout<<p4.x<<" "<<p4.y<<endl;
	p3=point(-3,0);
	cout<<distToLineSegment(p3,p1,p2,p4)<<endl;
	cout<<p4.x<<" "<<p4.y<<endl;
	p3=point(30,0);
	cout<<distToLineSegment(p3,p1,p2,p4)<<endl;
	cout<<p4.x<<" "<<p4.y<<endl;
	cout<<"------------------------------"<<endl;
	p1=point(0,3);
	p2=point(20,3);
	p3=point(10,3);
	cout<<angle(p1,p3,p2)<<endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;

const ld EPS=1e-9;
const ld PI=acos(-1.0);

const ll N=100001;

struct point{
	ld x,y;
	//constructor para una estructura vacia
	point(){x=y=0.0;}
	//constructor para una estructura con datos
	point(ld x, ld y): x(x), y(y){}
};

//distancia entre puntos
ld dist(point p1, point p2){
	return hypot(p1.x-p2.x,p1.y-p2.y);
}

ld area(point a, point b, point c){
	return fabs( (a.x*b.y+b.x*c.y+c.x*a.y) - (a.y*b.x+b.y*c.x+c.y*a.x) )/2.0;
}

// radio del circulo inscrito en el triangulo
ld rInCircle(point a, point b, point c){
	return area(a,b,c)/(0.5*(dist(a,b)+dist(b,c)+dist(c,a)));
}

int main(){
	point p1=point(0,0);
	point p2=point(3,3);
	point p3=point(0,3);
	return 0;
}


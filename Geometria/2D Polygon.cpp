#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;

const ld EPS=1e-9;
const ld PI=acos(-1.0);

const ll N=100001;

struct point{
	ld x,y;
	//constructor para una estructura vacia
	point(){x=y=0.0;}
	//constructor para una estructura con datos
	point(ld x, ld y): x(x), y(y){}
};

ld dist(point p1, point p2){
	return hypot(p1.x-p2.x,p1.y-p2.y);
}

struct vec{
	ld x,y;
	vec(){}
	vec(ld x, ld y): x(x), y(y){}
};

// crea un vector en base a dos puntos
// el vector es a -> b
vec toVec(point a, point b){
	return vec(b.x-a.x,b.y-a.y);
}

ld cross(vec a, vec b){
	return a.x*b.y-a.y*b.x;
}

// retorna verdad si el punto r esta al lado izquiero de la recta pq
bool ccw(point p, point q, point r){
	return cross(toVec(p,q),toVec(p,r))>0;
}

// retorna el producto escalar a.b=|a||b|cos(x)
ld dot(vec a, vec b){
	return (a.x*b.x+a.y*b.y);
}

// retorna la norma^2  = |v|^2 
ld norm_sq(vec v){
	return (v.x*v.x+v.y*v.y);
}

ld angle(point a, point o, point b){ 
	vec oa=toVec(o,a);
	vec ob=toVec(o,b);
	return acos(dot(oa,ob)/sqrt(norm_sq(oa)*norm_sq(ob)));
}

ld perimeter(const vector<point> &P){
	ld result=0.0;
	for(ll i=0; i<P.size()-1; i++){
		result+=dist(P[i],P[i+1]);
	}
	return result;
}

ld area(const vector<point> &P){
	ld result=0.0;
	for(ll i=0; i<P.size()-1; i++){
		result+=(P[i].x*P[i+1].y-P[i].y*P[i+1].x);
	}
	return fabs(result)/2.0;
}

// solo sirve cuando en el arreglo de puntos no exista puntos coolineales
// en caso de existir se debe eliminar esos puntos coolineales
bool isConvex(const vector<point> &P){
	ll sz=P.size();
	if(sz<=3) return false; // el poligono es de 2 o menos lados es no convexo +1(es por el loop)
	// como no existe puntos coolineales y el vector tiene sentido antihorario u horario
	// el algoritmo funciona correctamente
	bool isLeft=ccw(P[0],P[1],P[2]); // toma el sentido del ordenamiento del punto P[2] (horario o antihorario)
	for(ll i=1; i<sz-1; i++){
		if(ccw(P[i],P[i+1],P[(i+2)==sz ? 1:i+2])!=isLeft){//el i+2==sz es porq el ultimo punto es el punto 0, por ende el siguiente debe ser 1
			return false;
		}
	}
	return true;
}

// verificar si existe algun punto dentro del poligono
// se debe verificar antes si el punto pt esta es algun vertice de poligono o s colineal con alguna arista (retornar verdadero)
// sino se verifica, no funciona
bool inPolygon(point pt, const vector<point> &P){
	ll sz=P.size();
	if(sz==0) return false;
	ld sum=0; // se asume que el ultimo vertice es igual al primer vertice
	for(ll i=0; i<sz-1; i++){
		if(ccw(pt,P[i],P[i+1])){
			sum+=angle(P[i],pt,P[i+1]); // letf turn/ccw
		}else{
			sum-=angle(P[i],pt,P[i+1]); // right turn/cw
		}
	}
	return fabs(fabs(sum)-2*PI)<EPS;
}

int main(){
	// el ordenamiento del poligono debe estar en CounterClockWise (sentido antihorario)
	vector<point> P;
	P.push_back(point(1,1)); // P0
	P.push_back(point(3,3)); // P1
	P.push_back(point(9,1)); // P2
	P.push_back(point(12,4)); // P3
	P.push_back(point(9,7)); // P4
	P.push_back(point(1,7)); // P5
	P.push_back(P[0]); // important: terminar el bucle
	cout<<"El perimetro es: "<<perimeter(P)<<endl;
	cout<<"El area es: "<<area(P)<<endl;
	if(isConvex(P)){
		cout<<"Es convexo"<<endl;
	}else{
		cout<<"Es concavo"<<endl;
	}
	
	// nota: la funcion inPolygon solo sirve para puntos que estan dentro o fuera
	// pero en puntos que estan en los bordos se debe modifcar
	point p1=point(3,4);
	if(inPolygon(p1,P)){
		cout<<"El punto p1 esta dentro"<<endl;
	}else{
		cout<<"El punto p1 no esta dentro"<<endl;
	}
	point p2=point(3,1);
	if(inPolygon(p2,P)){
		cout<<"El punto p2 esta dentro"<<endl;
	}else{
		cout<<"El punto p2 no esta dentro"<<endl;
	}
	return 0;
}


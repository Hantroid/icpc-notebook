#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double tvar;
typedef long double Double;

//const Long MX = ;
const Double EPS = 1e-7;

/// ------------------------------ Closest points Euclidian distance --------------------------------

struct Point{
	tvar x, y;
	Long id;
	Point(){}
	Point(tvar x, tvar y, Long id) : x(x), y(y), id(id){}
};

bool cmpx(Point a, Point b){
	if(fabs(a.x - b.x) < EPS) return a.y < b.y;
	return a.x < b.x;
}

bool cmpy(Point a, Point b){
	if(fabs(a.y - b.y) < EPS) return a.x < b.x;
	return a.y < b.y;
}

struct ClosestPoints{
	Long n;
	vector<Point> points;
	vector<Point> auxiliarPoints;
	pair<Long,Long> bestPair;
	Double mnDist;
	
	ClosestPoints(vector<Point> points) : points(points){
		n = points.size();
		auxiliarPoints.resize(n);
		mnDist = 8e18;
	}
	
	void updateBestPair(Point a, Point b){ // O(1)
		Double dist = sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
		if(fabs(dist - mnDist) > EPS && dist < mnDist){
			mnDist = dist;
			bestPair = {a.id, b.id};
		}
	}
	
	// [l,r>
	void solveDC(Long l, Long r){ // O(7nlog(n)) = O(nlog(n))
		if(r - l <= 3){
			for(Long i = l; i < r; i++){
				for(Long j = i + 1; j < r; j++){
					updateBestPair(points[i],points[j]);
				}
			}
			sort(points.begin()+l, points.begin()+r, cmpy);
			return;
		}
		Long med = (l + r) / 2;
		tvar midx = points[med].x;
		solveDC(l,med);
		solveDC(med,r);
		
		// O(r - l)
		merge(points.begin() + l, points.begin() + med, points.begin() + med, points.begin() + r, auxiliarPoints.begin(), cmpy);
		copy(auxiliarPoints.begin(), auxiliarPoints.begin() + r - l, points.begin() + l);
		
		Long sz = 0;
		for(Long i = l; i < r; i++){  // O(7n) = O(n)
			Double difx = fabs(points[i].x - midx);
			if(fabs(difx - mnDist) > EPS && difx < mnDist){
				for(Long j = sz - 1; j >= 0; j--){  // almost compare with 7 points
					Double dify = fabs(points[i].y - auxiliarPoints[j].y);
					if(fabs(dify - mnDist) > EPS && dify < mnDist){
						updateBestPair(points[i], auxiliarPoints[j]);
					}else{
						break;
					}
				}
				auxiliarPoints[sz++] = points[i];
			}
		}
	}
	
	void solve(){ 
		sort(points.begin(),points.end(),cmpx);
		return solveDC(0,n); 
	}
};

/// ------------------------------ Closest points Euclidian distance --------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	return 0;
}


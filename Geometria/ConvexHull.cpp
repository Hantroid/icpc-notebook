#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef long double ld;

const ld EPS=1e-9;
const ld PI=acos(-1.0);

const ll N=100001;

struct point{
	ld x,y;
	//constructor para una estructura vacia
	point(){x=y=0.0;}
	//constructor para una estructura con datos
	point(ld x, ld y): x(x), y(y){}
	bool operator ==(point other) const{
		return (fabs(x-other.x)<EPS && fabs(y-other.y)<EPS);
	}
};

ld dist(point p1, point p2){
	return hypot(p1.x-p2.x,p1.y-p2.y);
}

struct vec{
	ld x,y;
	vec(){}
	vec(ld x, ld y): x(x), y(y){}
};

// crea un vector en base a dos puntos
// el vector es a -> b
vec toVec(point a, point b){
	return vec(b.x-a.x,b.y-a.y);
}

// halla el area de 3 puntos en base a 2 vectores
ld cross(vec a, vec b){
	return a.x*b.y-a.y*b.x;
}

// retorna verdad si el punto r esta al lado izquiero de la recta pq
bool ccw(point p, point q, point r){
	return cross(toVec(p,q),toVec(p,r))>0;
}

// retorna verdad si el punto r esta en la mimsma recta que pq
bool collinear(point p, point q, point r){
	return fabs(cross(toVec(p,q),toVec(p,r)))<EPS;
}
point pivot=point(0.0,0.0);

// ordenamiento de los puntos respecto al angulo q forma con el pivot
bool angleCmp(point a, point b){
	if(collinear(pivot,a,b)){ // caso especial
		return dist(pivot,a)<dist(pivot,b); // selecciona el mas cercano
	}
	ld d1x=a.x-pivot.x;
	ld d1y=a.y-pivot.y;
	ld d2x=b.x-pivot.x;
	ld d2y=b.y-pivot.y;
	return (atan2(d1y,d1x)-atan2(d2y,d2x))<0; // compara 2 angulos
}

// convex hull
// retorna los puntos como un poligono en sentido antihorario
// repitiendo el punto inicial en el arreglo, para cerrar el bucle
vector<point> CH(vector<point> &P){
	ll i,j,n=P.size();
	if(n<=3){
		if(!(P[0]==P[n-1])){
			P.push_back(P[0]);
		}
		return P;
	}
	// encuentra P0, el punto mas bajo y derecho de todos los puntos
	ll P0=0;
	for(i=1; i<n; i++){
		if(P[i].y<P[P0].y || (P[i].y==P[P0].y && P[i].x>P[P0].x)){
			P0=i;
		}
	}
	swap(P[0],P[P0]);
	pivot=P[0];
	sort(++P.begin(),P.end(),angleCmp);
	
	vector<point> S;
	S.push_back(P[n-1]);
	S.push_back(P[0]);
	S.push_back(P[1]);
	i=2;
	while(i<n){
		j=S.size()-1;
		if(ccw(S[j-1],S[j],P[i])){
			S.push_back(P[i]);
			i++;
		}else{
			S.pop_back();
		}
	}
	return S;
}

int main(){
	// no se debe repetir el ultimo punto
	vector<point> P,ans;
	P.push_back(point(1,1)); // P0
	P.push_back(point(3,3)); // P1
	P.push_back(point(9,1)); // P2
	P.push_back(point(12,4)); // P3
	P.push_back(point(9,7)); // P4
	P.push_back(point(1,7)); // P5
	
	ans=CH(P);
	for(ll i=0; i<ans.size(); i++){
		cout<<ans[i].x<<" "<<ans[i].y<<endl;
	}
	return 0;
}


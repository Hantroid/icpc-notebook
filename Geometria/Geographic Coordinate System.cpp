#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double PI = acos(-1.0);
const Double EPS = 1e-7;

struct Point{
	Double x, y, z;
	Point(){}
	Point(Double x, Double y, Double z) : x(x), y(y), z(z) {}
	Point operator-(const Point &P){ return (Point){x-P.x,y-P.y,z-P.z}; }
	Point operator+(const Point &P){ return (Point){x+P.x,y+P.y,z+P.z}; }
	Point operator/(const Double f){ return (Point){x/f,y/f,z/f}; }
	Point operator*(const Double f){ return (Point){x*f,y*f,z*f}; }
	Double mod(){ return sqrt(x*x + y*y + z*z); }
	Double mod2(){ return x*x + y*y + z*z; }
};
struct PointP{ // phi = longitude [0.0, 2*PI], ro = latitude [-PI/2,PI/2]
	Double r, phi, ro;
	PointP(){}
	PointP(Double r, Double phi, Double ro) : r(r), phi(phi), ro(ro) {}
};
Point polarTocartesian(PointP p){
	Double xy = p.r * cos(p.ro);
	Double z = p.r * sin(p.ro);
	Double x = xy * cos(p.phi);
	Double y = xy * sin(p.phi);
	return Point(x,y,z);
}
PointP cartesianTopolar(Point p){
	Double x = p.x , y = p.y , z = p.z;
	Double phi = atan2(y, x);
	Double ro = atan2(z, sqrt(x*x + y*y));
	Double r = p.mod();
	return PointP(r,phi,ro);
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Double radius, longitude, latitude;
	cin >> radius >> longitude >> latitude;
	PointP GeographicPointA = PointP(radius, 0.0, 0.0); // PointP(radius, 0, 0) to 3D point is Point(R,0.0,0.0);
	PointP GeographicPointB = PointP(radius, PI/2, 0.0); // PointP(radius, PI/2, 0) to 3D point is Point(0.0,R,0.0);
	PointP GeographicPointC = PointP(radius, 0.0, PI/2); // PointP(radius, 0, PI/2) to 3D point is Point(0.0,0.0,R);
	PointP GeographicPointD = PointP(radius, PI, 0.0); // PointP(radius, PI, 0) to 3D point is Point(-R,0.0,0.0);
	PointP GeographicPointE = PointP(radius, longitude, latitude);
	
	Point point3D_A = polarTocartesian(GeographicPointA);
	Point point3D_B = polarTocartesian(GeographicPointB);
	Point point3D_C = polarTocartesian(GeographicPointC);
	Point point3D_D = polarTocartesian(GeographicPointD);
	
	cout << "("<< point3D_A.x << ", " << point3D_A.y << ", " << point3D_A.z << ")" << endl;
	cout << "("<< point3D_B.x << ", " << point3D_B.y << ", " << point3D_B.z << ")" << endl;
	cout << "("<< point3D_C.x << ", " << point3D_C.y << ", " << point3D_C.z << ")" << endl;
	cout << "("<< point3D_D.x << ", " << point3D_D.y << ", " << point3D_D.z << ")" << endl;
	return 0;
}

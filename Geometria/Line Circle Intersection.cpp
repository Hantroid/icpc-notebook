#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-7;

struct Point{
    Double x, y;
    Point(){}
    Point(Double x, Double y) : x(x), y(y) {}
};

struct Line{
	Double a, b, c; //aX + bY + c = 0
	Line(){}
	Line(Double a, Double b, Double c) : a(a), b(b), c(c) {}
	Line(Point P1, Point P2){
		a = P1.y - P2.y;
		b = P2.x - P1.x;
		c = -a*P1.x - b*P1.y;    
	}
};

vector<Point> lineCircleIntersection(Double radius, Line L){ // center of circle should be in (0,0)
	vector<Point> points;
	Double a = L.a, b = L.b, c = L.c, r = radius;
	Double x0 = -a*c / (a*a + b*b);
	Double y0 = -b*c / (a*a + b*b);
	if(c*c > r*r * (a*a + b*b) + EPS) return points;
	if(fabs(c*c - r*r * (a*a + b*b)) < EPS){
		points.push_back(Point(x0,y0));
		return points;
	}
	Double d = r*r - c*c / (a*a + b*b);
	Double mult = sqrt(d / (a*a + b*b));
	Double ax, ay, bx, by;
	ax = x0 + b * mult;
	bx = x0 - b * mult;
	ay = y0 - a * mult;
	by = y0 + a * mult;
	points.push_back(Point(ax,ay));
	points.push_back(Point(bx,by));
	return points;
}
vector<Point> lineCircleIntersection(Double radius, Point A, Point B){
	return lineCircleIntersection(radius, Line(A,B));
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Point A = Point(10,10);
	Point B = Point(7,-6);
	Double radius = 10;
	
	vector<Point> points = lineCircleIntersection(radius, A, B);
	for(auto pt : points) cout << "(" << pt.x << ", " << pt.y << ")" << endl;
	return 0;
}

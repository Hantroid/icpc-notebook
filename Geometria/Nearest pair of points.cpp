#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-9;
const Double INF = 1e4;

struct point{
	Double x, y;
	Long id;
	point(){}
	point(Double a, Double b, Long c){ x = a; y = b; id = c;}
};

bool cmp_x(point a, point b){
	if(fabs(a.x - b.x) < EPS) return a.y < b.y;
	return a.x < b.x;
}

bool cmp_y(point a, point b){
	if(fabs(a.y - b.y) < EPS) return a.x < b.x;
	return a.y < b.y;
}

Long n;
vector<point> pt;
vector<point> t;
pair<Long,Long> best_pair;
Double mndist;

void upd_ans(point a, point b){
	Double dist = hypot(a.x - b.x, a.y - b.y);
	if(fabs(dist - mndist) > EPS && dist < mndist){
		mndist = dist;
		best_pair = {a.id,b.id};
	}
	return;
}

void rec(Long l, Long r){
	if(r - l <= 3){
		for(Long i = l; i < r; i++){
			for(Long j = i+1; j < r; j++){
				upd_ans(pt[i],pt[j]);
			}
		}
		sort(pt.begin()+l,pt.begin()+r,cmp_y);
		return;
	}
	
	Long med = (l+r)/2;
	Double midx = pt[med].x;
	rec(l,med);
	rec(med,r);
	
	merge(pt.begin() + l, pt.begin() + med, pt.begin() + med, pt.begin() + r, t.begin(), cmp_y);
	copy(t.begin(), t.begin() + r - l, pt.begin() + l);
	
	Long tsz = 0;
	for(Long i = l; i < r; i++){
		if(fabs(fabs(pt[i].x - midx) - mndist) > EPS && fabs(pt[i].x - midx) < mndist){
			for(Long j = tsz - 1; j >= 0; j--){
				if(fabs(pt[i].y - t[j].y - mndist) > EPS && pt[i].y - t[j].y < mndist){
					upd_ans(pt[i], t[j]);
				}else{
					break;
				}
			}
			t[tsz++] = pt[i];
		}
	}
	return;
}

void solve(){
	pt.clear();
	t.clear();
	
	Double x, y;
	for(Long i = 0; i < n; i++){
		cin >> x >> y;
		pt.push_back(point(x,y,i));
	}
	
	t.resize(n);
	sort(pt.begin(),pt.end(),cmp_x);
	mndist = 1e10;
	rec(0,n);
	
	if(fabs(mndist - INF) > EPS && mndist > INF){
		cout << "INFINITY" << "\n";
	}else{
		cout << mndist << "\n";
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(4);
	cout << fixed;
    
    while(true){
    	cin >> n;
    	if(n == 0){
    		break;
		}
		solve();
	}
	return 0;
}


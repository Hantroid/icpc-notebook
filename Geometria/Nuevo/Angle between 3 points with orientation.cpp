#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-8;
const Double PI = acos(-1.0);

struct Point{
	Double x, y;
	Point(){}
	Point(Double x, Double y): x(x), y(y){}
	Double mod2()	{ return x*x + y*y; }
	Double mod()	{ return hypot(x,y); }
	Double arg()	{ return atan2(y,x); } // 0 to PI
	Point ort()		{ return Point(-y,x); }
	Point unit()	{ Double k = mod(); return Point(x/k,y/k); }
};
typedef Point Vector;

Point operator -(const Point &A, const Point &B){ return Point(A.x - B.x, A.y - B.y); }


Double dot(const Vector &A, const Vector &B)	{ return A.x * B.x + A.y * B.y; }
Double cross(const Vector &A, const Vector &B)	{ return A.x * B.y - A.y * B.x; }

//------------- Angle between 3 points with orientation ----------------------

Double angle(Point O, Point A, Point B){ // Angle between OA to OB
	Vector OA = A - O;
	Vector OB = B - O;
	Double ang = acos((dot(OA,OB) * 1.0)/ (hypot(OA.x,OA.y) * hypot(OB.x,OB.y)));
	ang = (cross(OA,OB) > 0.0 ? ang : 2*PI - ang);
	if(fabs(ang) < EPS || fabs(2*PI - ang) < EPS) ang = 0.0;
	return ang;
}

//------------- Angle between 3 points with orientation ----------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	
	return 0;
}


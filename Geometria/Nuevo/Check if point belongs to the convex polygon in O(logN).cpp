#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef long double Double;

const Double EPS = 1e-8;

struct Point{
	Double x, y;
	Point(){}
	Point(Double x, Double y): x(x), y(y){}
	Double mod2()	{ return x*x + y*y; }
	Double mod()	{ return hypot(x,y); }
	Double arg()	{ return atan2(y,x); } // 0 to PI
	Point ort()		{ return Point(-y,x); }
	Point unit()	{ Double k = mod(); return Point(x/k,y/k); }
};
typedef Point Vector;

Point operator +(const Point &A, const Point &B){ return Point(A.x + B.x, A.y + B.y); }
Point operator -(const Point &A, const Point &B){ return Point(A.x - B.x, A.y - B.y); }
Point operator /(const Point &A, double k){ return Point(A.x / k, A.y / k); }
Point operator *(const Point &A, double k){ return Point(A.x * k, A.y * k); }

bool operator ==(const Point &A, const Point &B){ return fabs(A.x - B.x) < EPS && fabs(A.y - B.y) < EPS; }
bool operator !=(const Point &A, const Point &B){ return !(A==B); }
bool operator <(const Point &A, const Point &B){
	if(fabs(A.x - B.x) < EPS) return A.y < B.y;
	return A.x < B.x;
}

Double dist(const Point &A, const Point &B)		{ return hypot(A.x - B.x, A.y - B.y); }
Double dot(const Vector &A, const Vector &B)	{ return A.x * B.x + A.y * B.y; }
Double cross(const Vector &A, const Vector &B)	{ return A.x * B.y - A.y * B.x; }
Double area(const Point &A, const Point &B, const Point &C) { return cross(B - A, C - A); } // Doble del area del triangulo con signo


//------------- Check if point belongs to the convex polygon in O(logN)----------------------

//Condition: the polygon is ordered counter-clockwise.
void prepare(vector<Point> &Polygon){
	Long n = Polygon.size();
	Long pos = 0;
	for(Long i = 1; i < n; i++){
		if(Polygon[i] < Polygon[0]) pos = i;
	}
	rotate(Polygon.begin(),Polygon.begin()+pos,Polygon.end());
	return;
}

bool insideLine(Point &O, Point &A, Point &B){ return fabs(cross(A-O,B-O)) < EPS; }

bool insideSegment(Point &A, Point &B, Point &P){
	if(!insideLine(A,B,P)) return false; // aumentar precision si es necesario
	return min(A.x,B.x) <= P.x && P.x <= max(A.x,B.x) && min(A.y,B.y) <= P.y && P.y <= max(A.y,B.y);
}

bool insideTriangle(Point &A, Point &B, Point &C, Point &P){
	Double area1 = fabs(cross(B - A, C - A));
	Double area2 = fabs(cross(A - P, B - P)) + fabs(cross(B - P, C - P)) + fabs(cross(C - P, A - P));
	return fabs(area1 - area2) < EPS;
}

Long sgn(Double val) { return val > EPS ? 1 : (fabs(val) < EPS ? 0 : -1); }

// 0 = in the border
// 1 = inside of polygon
// -1 = outside of polygon
// O = Polygon[0];   N = Polygon[n-1]
Long pointInConvexPolygon(vector<Point> &Polygon, Point point){
	vector<Point> &P = Polygon;
	Long n = P.size();
	Vector O1 = P[1] - P[0]; // P0 -> P1
	Vector OP = point - P[0]; // P0 -> point
	Vector ON = P[n-1] - P[0]; // P0 ->  Pn-1
	
	if(!insideLine(P[0],P[1],point) && sgn(cross(O1,OP)) != sgn(cross(O1,ON))) return -1;
	if(!insideLine(P[0],P[n-1],point) && sgn(cross(ON,OP)) != sgn(cross(ON,O1))) return -1;

	if(insideSegment(P[0],P[1],point)) return 0;
	
	Long l = 1, r = n - 1;
	while(r - l > 1){
		Long med = (l+r)>>1;
		Vector OM = P[med] - P[0]; // P0 -> Pmed
		if(cross(OM,OP) >= 0.0){
			l = med;
		}else{
			r = med;
		}
	}
	if(insideSegment(P[l],P[l+1],point)) return 0;
	if(insideTriangle(P[0],P[l],P[l+1],point)) return 1;
	return -1;
}

//------------- Check if point belongs to the convex polygon in O(logN)----------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Double x, y;
	Long n, d;
	cin >> n >> d;
	
	vector<Point> Polygon;
	Polygon.push_back(Point(0,d));
	Polygon.push_back(Point(d,0));
	Polygon.push_back(Point(n,n-d));
	Polygon.push_back(Point(n-d,n));
	
	prepare(Polygon);
	
	Long q;
	cin >> q;
	for(Long i = 0; i < q; i++){
		cin >> x >> y;
		if(pointInConvexPolygon(Polygon,Point(x,y)) != -1){
			cout << "YES\n";
		}else{
			cout << "NO\n";
		}
	}
	return 0;
}


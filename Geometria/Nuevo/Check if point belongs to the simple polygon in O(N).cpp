#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

struct Point{
	Long x, y;
	Point(){}
	Point(Long x, Long y): x(x), y(y){}
};
typedef Point Vector;

Point operator +(const Point &A, const Point &B){ return Point(A.x + B.x, A.y + B.y); }
Point operator -(const Point &A, const Point &B){ return Point(A.x - B.x, A.y - B.y); }
Point operator /(const Point &A, double k){ return Point(A.x / k, A.y / k); }
Point operator *(const Point &A, double k){ return Point(A.x * k, A.y * k); }

bool operator ==(const Point &A, const Point &B){ return A.x == B.x && A.y == B.y; }
bool operator !=(const Point &A, const Point &B){ return !(A==B); }
bool operator <(const Point &A, const Point &B){
	if(A.x == B.x) return A.y < B.y;
	return A.x < B.x;
}

Long dot(const Vector &A, const Vector &B)	{ return A.x * B.x + A.y * B.y; }
Long cross(const Vector &A, const Vector &B)	{ return A.x * B.y - A.y * B.x; }
Long area(const Point &A, const Point &B, const Point &C) { return cross(B - A, C - A); } // Doble del area del triangulo con signo

//---------------- check if a point is inside a simple polygon ------------------------
// simple polygon =  if its boundary doesn't cross itself.
// strictly convex polygon = convex polygon if in addition no three vertices lie on the same line
// convex polygon = all interior angles are less 180 degrees

//blog: https://codeforces.com/blog/entry/48868
// Condition: The polygon is order ccw order && N >= 3
// 0 : is in the border of polygon
// 1 : is inside of polygon
// -1 : is outside of polygon
Long pointInPolygon(vector< Point > &Polygon, Point point){ // Complexity O(N)
	Long n = Polygon.size();
	Long windingNumber = 0;
	for(Long i = 0; i < n; i++){
		if(point == Polygon[i]) return 0;
		Long j = (i+1)%n;
		if(Polygon[i].y == point.y && Polygon[j].y == point.y){
			if(min(Polygon[i].x,Polygon[j].x) <= point.x && point.x <= max(Polygon[i].x,Polygon[j].x)) return 0;
		}else{
			bool below = Polygon[i].y < point.y;
			if(below != (Polygon[j].y < point.y)){
				Long orientation = cross(point - Polygon[i],Polygon[j] - Polygon[i]);
				if(orientation == 0) return 0;
				if(below == (orientation > 0)) windingNumber += below ? 1 : -1;
			}
		}
	}
	return windingNumber == 0 ? -1 : 1;
}

//---------------- check if a point is inside a simple polygon ------------------------

//https://codeforces.com/problemset/problem/659/D
int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Long n;
	cin >> n;
	vector< Point > v(n);
	for(Long i = 0; i < n; i++){
		cin >> v[i].x >> v[i].y;
		v[i].x *= 2;
		v[i].y *= 2;
	}
	cin >> v[0].x >> v[0].y;
	v[0].x *= 2;
	v[0].y *= 2;
	
	Long ans = 0;
	for(Long i = 0; i < n; i++){
		Long dx = v[(i+1)%n].x - v[i].x;
		Long dy = v[(i+1)%n].y - v[i].y;
		if(dx != 0) dx /= abs(dx);
		if(dy != 0) dy /= abs(dy);
		
		Point p = Point(v[(i+1)%n].x + dx,v[(i+1)%n].y + dy);
		
		Long aux = pointInPolygon(v,p);
		if(aux == 1) ans += aux;
	}
	cout << ans << "\n";
	return 0;
}


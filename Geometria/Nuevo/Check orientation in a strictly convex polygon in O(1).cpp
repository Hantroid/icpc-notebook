#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

struct Point{
	Long x, y;
	Point(){}
	Point(Long x, Long y): x(x), y(y){}
};
typedef Point Vector;

Point operator +(const Point &A, const Point &B){ return Point(A.x + B.x, A.y + B.y); }
Point operator -(const Point &A, const Point &B){ return Point(A.x - B.x, A.y - B.y); }
Point operator /(const Point &A, double k){ return Point(A.x / k, A.y / k); }
Point operator *(const Point &A, double k){ return Point(A.x * k, A.y * k); }

bool operator ==(const Point &A, const Point &B){ return A.x == B.x && A.y == B.y; }
bool operator !=(const Point &A, const Point &B){ return !(A==B); }
bool operator <(const Point &A, const Point &B){
	if(A.x == B.x) return A.y < B.y;
	return A.x < B.x;
}

Long dot(const Vector &A, const Vector &B)	{ return A.x * B.x + A.y * B.y; }
Long cross(const Vector &A, const Vector &B)	{ return A.x * B.y - A.y * B.x; }
Long area(const Point &A, const Point &B, const Point &C) { return cross(B - A, C - A); } // Doble del area del triangulo con signo

//---------------- check the orientation of a simple polygon ------------------------
// simple polygon =  if its boundary doesn't cross itself.
// strictly convex polygon = convex polygon if in addition no three vertices lie on the same line
// convex polygon = all interior angles are less 180 degrees

// Condition: The polygon ir order ccw or cw order && N >= 3
bool orientation_ccw(vector< Point > &Polygon){ // Complexity O(N)
	return cross(Polygon[2] - Polygon[1], Polygon[0] - Polygon[1]) > 0;
}

//---------------- check the orientation of a simple polygon ------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;
struct Point{
	Long x, y;
	Point(){}
	Point(Long x, Long y) : x(x), y(y) {}
};
typedef Point Vector;
Point operator -(const Point &A, const Point &B) { return Point(A.x - B.x, A.y - B.y); }
bool operator <(const Point &A, const Point &B){
	if(A.x != B.x) return A.x < B.x;
	return A.y < B.y;
}

Long cross(const Vector &A, const Vector &B) { return A.x * B.y - A.y * B.x; }
Long area(const Point &A, const Point &B, const Point &C){ return cross(B - A, C - A); }

vector< Point > ConvexHull(vector< Point > P){
	sort(P.begin(),P.end());
	
	Long n = P.size(), k = 0;
	Point H[2*n];
	
	for(Long i = 0; i < n; i++){
		while(k >= 2 && area(H[k-2],H[k-1],P[i]) <= 0) k--;
		H[k++] = P[i];
	}
	
	for(Long i = n - 2, t = k; i >= 0; i--){
		while(k > t && area(H[k-2],H[k-1],P[i]) <= 0) k--;
		H[k++] = P[i];
	}
	return vector< Point > (H,H+k-1);
}

// get all AntiPodals O(n)   ---- Rotating Callipers
// exist only n pairs antipodals
vector< Vector > getAntiPodals(vector< Point > &P){
	vector< Vector > antiPodals;
	Long n = P.size();
	for(Long i = 0, j = 2; i < n; i++){
		while(area(P[i], P[(i+1) % n], P[(j+1) % n]) > area(P[i], P[(i+1) % n], P[j])) j = (j + 1) % n;
		antiPodals.push_back(P[j] - P[i]);
	}
	return antiPodals;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


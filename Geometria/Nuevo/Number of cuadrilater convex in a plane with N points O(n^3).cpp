#include <bits/stdc++.h>
using namespace std;
typedef int Long;

const Long MX = 2010;

struct Point{
	Long x, y;
	Point(){}
	Point(Long x, Long y): x(x), y(y){}
};
typedef Point Vector;

Point operator -(const Point &A, const Point &B){ return Point(A.x - B.x, A.y - B.y); }

bool operator <(const Point &A, const Point &B){
	if(A.x == B.x) return A.y < B.y;
	return A.x < B.x;
}

Long cross(const Vector &A, const Vector &B)	{ return A.x * B.y - A.y * B.x; }

vector< Point > v;
Long cal[MX][MX];

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Long n, x, y;
	cin >> n;
	for(Long i = 0; i < n; i++){
		cin >> x >> y;
		v.push_back( Point(x,y) );
	}
	sort(v.begin(),v.end());
	
	for(Long i = 0; i < n; i++){
		for(Long j = i + 1; j < n; j++){
			for(Long k = i + 1; k < j; k++){
				if(cross(v[j] - v[i],v[k] - v[i]) < 0) cal[i][j]++;
			}
		}
	}
	
	long long ans = n*(n-1)*(n-2)*(n-3)/24;
	for(Long i = 0; i < n; i++){
		for(Long j = i + 1; j < n; j++){
			for(Long k = j + 1; k < n; k++){
				if(cross(v[k] - v[i],v[j] - v[i]) < 0){
					ans -= cal[i][k] - cal[i][j] - cal[j][k] - 1;
				}else{
					ans -= cal[i][j] + cal[j][k] - cal[i][k];
				}
			}
		}
	}
	
	cout << ans << "\n";
	return 0;
}


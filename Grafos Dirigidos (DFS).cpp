#include <bits/stdc++.h>
using namespace std;

vector<long long> adj[10];
long long estado[10]; // 0:blanco, 1:gris, 2:negro
long long td[10],tf[10],tiempo;

void dfs(long long u){
	estado[u]=1;
	td[u]=tiempo++; //asigna primero y luego inrcementa o puedes ponerse td[u]=tiempo, tiempo++;
	long long sz=adj[u].size();
	for(long long i=0; i<sz; i++){
		long long v=adj[u][i];
		if(estado[v]==0){
			cout<<char(u+'A')<<", "<<char(v+'A')<<" es tree edge"<<endl;
		}
		if(estado[v]==1){
			cout<<char(u+'A')<<", "<<char(v+'A')<<" es back edge"<<endl;
		}
		if(estado[v]==2){
			if(td[u]<td[v]){
				cout<<char(u+'A')<<", "<<char(v+'A')<<" es forward edge"<<endl;
			}
			if(td[u]>td[v]){
				cout<<char(u+'A')<<", "<<char(v+'A')<<" es cross edge"<<endl;
			}
		}
		if(estado[v]==0){
			dfs(v);
		}
	}
	estado[u]=2;
	tf[u]=tiempo++;
	//topsort[szz++]=u;
	//agregar con deque
	//topsort.push_from(u);
}

int main(){
	long long n,m;
	char a,b;
	cin>>n>>m;
	for(long long i=0; i<m; i++){
		cin>>a>>b;
		adj[a-'A'].push_back(b-'A');
	}
	for(long long i=0; i<n; i++){
		if(estado[i]==0){
			dfs(i);
		}
	}
	return 0;
}


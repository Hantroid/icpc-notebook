#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

struct Tree{
	Long n;
	vector<vector<Long>> adj, layer;
	vector<Long> dis, code, parent, label;
	
	Tree(){}
	Tree(Long n) : n(n){ // nodes [0,n>
		code.clear();
		adj.resize(n);
		layer.resize(n);
		dis.resize(n,0);
		parent.resize(n,-1);
		label.resize(n,-1);
	}
	
	void addEdge(Long u, Long v){
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	
	void dfs(Long u, Long d = 0, Long p = -1){
		dis[u] = d;
		parent[u] = p;
		layer[d].push_back(u);
		for(Long v : adj[u]) if(v != p) dfs(v,d+1,u);
	}
	
	void encode(Long root){
		dfs(root);
		Long mxDepth = 0;
		for(Long i = 0; i < n; i++) mxDepth = max(mxDepth, dis[i]);
		for(Long d = mxDepth; d >= 0; d--){
			vector<pair<vector<Long>,Long>> sortedNodes;
			for(Long u : layer[d]){
				auto labelComp = [&](Long &a, Long &b) {
					return label[a] < label[b];
				};
				sort(adj[u].begin(), adj[u].end(), labelComp);
				vector<Long> children;
				for(Long v : adj[u]){
					if(v == parent[u]) continue;
					children.push_back(label[v]);
				}
				sortedNodes.push_back({children,u});
			}
			sort(sortedNodes.begin(),sortedNodes.end());
			vector<Long> lastVec;
			Long lastId = 0;
			for(auto p : sortedNodes){
				if(lastVec != p.first){
					lastVec = p.first;
					lastId++;
				}
				Long u = p.second;
				label[u] = lastId;
				code.push_back(-2);
				for(Long v : adj[u]){
					if(v == parent[u]) continue;
					code.push_back(label[v]);
				}
			}
			code.push_back(-1);
		}
	}
	
	void clearCode(){ // mantain adj list
		code.clear();
		for(Long i = 0; i < n; i++){
			layer[i].clear();
			dis[i] = 0;
			parent[i] = label[i] = -1;
		}
	}
};

void solve(){
	Long n, x, y;
	cin >> n;
	
	vector<Long> code[2];
	for(Long t = 0; t < 2; t++){
		Tree T = Tree(n);
		for(Long i = 1; i < n; i++){
			cin >> x >> y;
			T.addEdge(x-1,y-1);
		}
		T.encode(0);
		code[t] = T.code;
	}
	if(code[0] == code[1]){
		cout << "YES\n";
	}else{
		cout << "NO\n";
	}
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	
	Long t;
	cin >> t;
	while(t--) solve();
	
	return 0;
}

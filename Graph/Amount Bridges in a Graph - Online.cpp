#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

// No testeado aun
// O((n+m)logn)
// solo se agrea arsitas, no se eliminan
struct AmountBridgesOnline{
	vector<Long> parent, dsu_2ecc, dsu_cc, dsu_cc_size;
	vector<Long> last_visit;
	Long lca_iteration;
	Long bridges;
	Long n;
	
	AmountBridgesOnline(){}
	AmountBridgesOnline(Long n) : n(n){ // los nodos van de [0,n>
		parent.resize(n);
		dsu_2ecc.resize(n);
		dsu_cc.resize(n);
		dsu_cc_size.resize(n);
		last_visit.assign(n,0);
		for(Long i = 0; i < n; i++){
			dsu_2ecc[i] = i;
			dsu_cc[i] = i;
			dsu_cc_size[i] = 1;
			parent[i] = -1;
		}
		bridges = lca_iteration = 0;
	}
	
	//los grupos forman un arbol de 2-edge-cconected-components
	Long find_2ecc(Long v){
		if(v == -1) return -1;
		return dsu_2ecc[v] == v ? v : dsu_2ecc[v] = find_2ecc(dsu_2ecc[v]);
	}
	
	//los grupos forman un arbol de los arboles de 2-edge-cconected-components
	Long find_cc(Long v){
		v = find_2ecc(v);
		return dsu_cc[v] == v ? v : dsu_cc[v] = find_cc(dsu_cc[v]);
	}
	
	void make_root(Long v){ // actualiza en nuevo arbol con raiz en v
		v = find_2ecc(v);
		Long root = v;
		Long child = -1;
		while(v != -1){
			Long p = find_2ecc(parent[v]);
			parent[v] = child;
			dsu_cc[v] = root;
			child = v;
			v = p;
		}
		dsu_cc_size[root] = dsu_cc_size[child];
		return;
	}
	
	void merge_path(Long a, Long b){
		lca_iteration++;
		vector<Long> path_a, path_b;
		Long lca = -1;
		while(lca == -1){
			if(a != -1){
				a = find_2ecc(a);
				path_a.push_back(a);
				if(last_visit[a] == lca_iteration) lca = a;
				last_visit[a] = lca_iteration;
				a = parent[a];
			}
			if(b != -1){
				path_b.push_back(b);
				b = find_2ecc(b);
				if(last_visit[b] == lca_iteration) lca = b;
				last_visit[b] = lca_iteration;
				b = parent[b];
			}
		}
		//se forma un anillo entre las raices de los arboles
		for(Long v : path_a){
			dsu_2ecc[v] = lca;
			if(v == lca) break;
			bridges--;
		}
		for(Long v : path_b){
			dsu_2ecc[v] = lca;
			if(v == lca) break;
			bridges--;
		}
	}
	
	void add_edge(Long a, Long b){
		a = find_2ecc(a);
		b = find_2ecc(b);
		if(a == b) return;
		Long ca = find_cc(a);
		Long cb = find_cc(b);
		if(ca != cb){
			bridges++;
			if(dsu_cc_size[ca] > dsu_cc_size[cb]){
				swap(a,b);
				swap(ca,cb);
			}
			make_root(a);
			parent[a] = dsu_cc[a] = b;
			dsu_cc_size[cb] += dsu_cc_size[a]; // cp algorithm
			//dsu_cc_size[cb] += dsu_cc_size[ca]; // idea
		}else{
			merge_path(a,b);
		}
		return;
	}
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	return 0;
}


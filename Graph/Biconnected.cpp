#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//https://www.spoj.com/problems/POLQUERY/

// ------------------------------------------ Biconected --------------------------------
//biconnected component is a maximal subgrah that does not have articulation points
//los nodos aislados (grado 0) no se modelan en el "bicomponent tree"
struct Edge{
	Long u, v;
	Edge(){}
	Edge(Long u , Long v) : u(min(u , v)) , v(max(u,v)){}
	bool operator == (const Edge &E) const { return E.u == u && E.v == v; }
	bool operator != (const Edge &E) const { return !(E == *this); }
};

struct BCT{
	Long n;
	vector< vector<Long> > adj;
	BCT(){}
	BCT(Long n) : n(n){
		adj.resize(n);
	}
	void addEdge(Long u , Long v){
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
};

struct Graph_to_Biconected{
	Long n;
	vector< vector<Long> > adj;
	vector< bool > vis, isArticulation;
	vector< Long > tin, low;
	Long timer;
	stack< Edge > edges;
	vector< unordered_map<Long,bool> > isBridge; 
	
	vector< Long > lastComponent; //articulation lastComponent
	
	vector< Long > bcn; //biconnected component of node. Articulation are considered separated
	vector< vector<Edge> > bce; // almacena las aristas donde el indice es el componente
	vector< Long > articulation;
	vector< Edge > bridge;
	
	BCT bct;
	Long numComponent;
	
	// los nodos van de [0,n>;
	Graph_to_Biconected(Long n) : n(n){
		timer = numComponent = 0;
		adj.resize(n);
		vis.resize(n,false);
		tin.resize(n,-1);
		low.resize(n,-1);
		isBridge.resize(n);
		isArticulation.resize(n,false);
		lastComponent.resize(n,-1);
		bcn.resize(n,-1);
		bce.resize(n*2);
		bct = BCT(n*2);
	}
	
	void addEdge(Long u , Long v){
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	
	
	void addEdgeBCT(Long nod){
		if(!isArticulation[nod]){
			bcn[nod] = numComponent;
		}else{
			if(lastComponent[nod] != numComponent){
				lastComponent[nod] = numComponent;
				bct.addEdge(bcn[nod] , numComponent);
			}
		}
	}
	
	void addBiconnectedComponent(Edge e){
		if(edges.empty()) return;
		while(!edges.empty()){
			Edge cur = edges.top();
			addEdgeBCT(cur.u);
			addEdgeBCT(cur.v);
			bce[numComponent].push_back(Edge(cur.u,cur.v));
			edges.pop();
			if(cur == e) break;
		}
		numComponent++;
	}
	
	void addArticulation(Long nod){
		if(!isArticulation[nod]){
			isArticulation[nod] = true;
			bcn[nod] = numComponent++;
			articulation.push_back(nod);
		}
	}
	
	void dfs(Long u, Long p = -1){ //O(N + M)
		vis[u] = true;
		tin[u] = low[u] = timer++;
		Long children = 0;
		for(Long v : adj[u]) {
			if(v == p) continue;
			if(vis[v]) {
				low[u] = min(low[u] , tin[v]);
				if(tin[v] < tin[u]){
					edges.push(Edge(u , v));
				}
			} else {
				edges.push(Edge(u , v));
				dfs(v , u);
				low[u] = min(low[u] , low[v]);
				if(low[v] > tin[u]) {
					isBridge[min(u,v)][max(u,v)] = true;
					bridge.push_back(Edge(u , v));
				}
				if(low[v] >= tin[u] && p != -1 ) {
					addArticulation(u);
					addBiconnectedComponent(Edge(u,v));
				}
				children++;
				if(p == -1 && children > 1){
					addArticulation(u);
					addBiconnectedComponent(Edge(u,v));
				}
			}
		}
		if(p == -1 ){
			Edge e = Edge(-1,-1);
			addBiconnectedComponent(e);
		}
	}
	
	void buildBicomponents(){
		for(Long i = 0; i < n; i++){
			if(!vis[i]) dfs(i);
		}
	}
	
	void printBlockCutTree(){
		for(Long i = 0; i < n; i++){
			cout << "biconnectedComponent[" << i << "] = " << bcn[i] << endl;
		}
		for(Long i = 0; i < n * 2; i++){
			for(Long v : bct.adj[i]){
				cout << i << " -> " << v << endl;
			}
		}
	}
	
	void printBridges() {
		cout << "Bridges = ";
		for(auto bridge_edge : bridge){
			cout << "( " << bridge_edge.u << " - " << bridge_edge.v << " ) ; ";
		}
		cout << endl;
	} 	
	
	void printArticulations() {
		cout << "Articulations = ";
		for(Long point_articualtion : articulation){
			cout << point_articualtion << " ";
		}
		cout << endl;
	}
	
	void printBicomponentsEdges() {
		for(Long i = 0; i < numComponent; i++){
			cout << "Component " << i << ":" << endl;
			for(auto edges_component : bce[i]){
				cout << "( " << edges_component.u << " - " << edges_component.v << " ) ; ";
			}
			cout << endl;
		}
		cout << endl;
	}
};

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	/*Graph_to_Biconected G(13);
	G.addEdge(1,2);
	G.addEdge(1,3);
	G.addEdge(2,3);
	G.addEdge(2,4);
	G.addEdge(3,4);
	G.addEdge(1,5);
	G.addEdge(1,0);
	G.addEdge(5,6);
	G.addEdge(0,6);
	G.addEdge(5,7);
	G.addEdge(7,8);
	G.addEdge(5,8);
	G.addEdge(8,9);
	G.addEdge(10,11);*/
	
	Graph_to_Biconected G(6); // 6 nodos y 7 componentes (articualacion + bicomponentes)
	G.addEdge(0,1);
	G.addEdge(1,2);
	G.addEdge(2,3);
	G.addEdge(1,4);
	G.addEdge(2,4);
	G.addEdge(4,5);
	
	G.buildBicomponents();
	//G.printBlockCutTree();
	//G.printBridges();
	//G.printArticulations();
	//G.printBicomponentsEdges();
	return 0;
}

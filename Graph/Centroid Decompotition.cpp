#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 2e5 + 10;

// WARNING: when you visit in centroid tree, and evaluate each centroid,
// add visited array to dont expand more than centroid subtree

struct CentroidD{
	Long n, val_vis;
	vector< vector<Long> > adj;
	vector<Long> cparent, szt, vis; // cparent: centroid_parent, szt: size_tree(subtree)
	vector< vector<Long> > cdTree; // centroid_decompotition Tree
	// centroid node is the node with cparent == -2	
	CentroidD(){}
	CentroidD(Long n) : n(n){ // nodes = [0,n>
		val_vis = 0;
		vis.resize(n, -1);
		cparent.resize(n, -1);
		szt.resize(n, 0);
		adj.resize(n);
		cdTree.resize(n);
	}
	
	void addEdge(Long u, Long v){
		adj[u].push_back(v);
		adj[v].push_back(u);
		return;
	}
	
	Long dfs(Long u, Long p){
		szt[u] = 1;
		vis[u] = val_vis;
		for(Long v : adj[u]){
			if(v != p && cparent[v] == -1 && vis[v] != val_vis) szt[u] += dfs(v,u);
		}
		return szt[u];
	}
	 // pc: parent_centroid, sz: size_subtree
	void decompotition(Long u, Long p, Long sz, Long pc){
		for(Long v : adj[u]){
			if(v != p && cparent[v] == -1 && (szt[v]<<1ll) > sz){
				decompotition(v,u,sz,pc);
				return;
			}
		}
		cparent[u] = pc;
		
		if(pc != -2){
			cdTree[u].push_back(pc);
			cdTree[pc].push_back(u);
		}
		for(Long v : adj[u]){
			if(cparent[v] == -1){
				dfs(v,-1);
				val_vis++;
				decompotition(v,-1,szt[v],u);
			}
		}
	}
	
	void build(Long u){
		dfs(u,-1);
		val_vis++;
		decompotition(u, -1, szt[u], -2);
	}
};


int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	CentroidD G(17);
	G.addEdge(1,4);
	G.addEdge(2,4);
	G.addEdge(3,4);
	G.addEdge(4,5);
	G.addEdge(5,6);
	G.addEdge(6,7);
	G.addEdge(7,8);
	G.addEdge(7,9);
	G.addEdge(6,10);
	G.addEdge(10,11);
	G.addEdge(11,12);
	G.addEdge(11,13);
	G.addEdge(12,14);
	G.addEdge(13,15);
	G.addEdge(13,16);
	
	G.build(1);
	return 0;
}


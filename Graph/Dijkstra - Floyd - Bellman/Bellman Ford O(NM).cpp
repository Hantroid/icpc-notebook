#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=1001;
const Long INF=1e14;


//-------------------- Bellman Ford ------------------------------
// Cplejidad O(N*M)
vector< pair<pair<Long,Long> ,Long> > edge;
Long dist[N];
Long parent[N];

void bellman(Long n){
	for(Long i = 0; i < n; i++){
		dist[i] = INF;
		parent[i] = -1;
	}
	//inicio nodo 0;
	Long inicio = 0;
	dist[inicio] = 0;
	// para bellman solo es necesario n-1 iteraciones para encontrar
	// el camino mas corto de un nodo fuente hacia los demas nodos
	for(Long k = 0; k < n-1; k++){
		bool modificado = false;
		for(Long i = 0; i < edge.size(); i++){
			Long ini = edge[i].first.first;
			Long fin = edge[i].first.second;
			Long peso = edge[i].second;
			// el dist[ini]!=INF evita q se modifique el IN cuadno hay una arista con peso negativo
			if(peso != INF && dist[ini] != INF){
				if(dist[fin] > dist[ini] + peso){
					dist[fin] = dist[ini] + peso;
					parent[fin] = ini;
					modificado = true;
				}
			}
		}
		// sino hizo cambio a algun camino termina las iteraciones
		if(!modificado) break;
	}
	
	// Hallando ciclo negativo
	/*bool ciclo_negativo = false;
	for(Long i = 0; i < edge.size(); i++){
		Long ini = edge[i].first.first;
		Long fin = edge[i].first.second;
		Long peso = edge[i].second;
		if(peso != INF && dist[ini] != INF){
			if(dist[fin] > dist[ini] + peso){
				dist[fin] = dist[ini] + peso;
				parent[fin] = ini;
				ciclo negativo = true;
			}
		}
	}*/
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    Long n;
	bellman(n);
	return 0;
}



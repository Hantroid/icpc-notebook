#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;
const ll INF=1e14;

vector<ll> adj[N];
vector<ll> pes[N];
ll parent[N];
ll dist[N];

void dijkstra(ll x, ll n){
	for(ll i=0; i<=n; i++){
		dist[i]=INF;
		parent[i]=-1;
	}
	dist[x]=0;
	priority_queue< pair<ll,ll> > q;
	q.push(make_pair(0,x));
	while(!q.empty()){
		ll u=q.top().second;
		ll d_u=-q.top().first;
		q.pop();
		if(d_u!=dist[u]) continue;
		for(ll i=0; i<adj[u].size(); i++){
			ll v=adj[u][i];
			ll w=pes[u][i];
			if(dist[v]>dist[u]+w){
				dist[v]=dist[u]+w;
				parent[v]=u;
				q.push(make_pair(-dist[v],v));
			}
		}
	}
	return;
}

void salida(ll x){
	if(x==-1) return;
	salida(parent[x]);
	cout<<x<<" ";
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
    cin.tie(NULL);
	ll n,m;
	cin>>n>>m;
	ll x,y,w;
	for(ll i=0; i<m; i++){
		cin>>x>>y>>w;
		adj[x].push_back(y);
		pes[x].push_back(w);
		adj[y].push_back(x);
		pes[y].push_back(w);
	}
	dijkstra(1,n);
	if(dist[n]==INF){
		cout<<-1<<endl;
	}else{
		salida(n);
	}
	return 0;
}



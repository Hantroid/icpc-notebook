#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=151;
const Long INF=1e14;

// ------------------------------ Floyd + ciclos negativos ---------------------------

Long dist[N][N];

//aplicar floyd antes de buscar los ciclos negativos
void floyd(Long n){
	for(Long k = 0; k < n; k++){
		for(Long i = 0; i < n; i++){
			for(Long j = 0; j < n; j++){
				if(dist[i][k] != INF && dist[k][j] != INF && dist[i][j] > dist[i][k] + dist[k][j]){
					dist[i][j] = dist[i][k] + dist[k][j];
				}
			}
		}
	}
	return;
}

// hallandao el par de ruta mas corta (se hace infinitamente negativo)
// si hay un nodo q tiene una distancia infinitamente negativa
void ciclos_neg(Long n){
	for(Long i = 0; i < n; i++){
		for(Long j = 0; j < n; j++){
			for(Long t = 0; t < n; t++){
				if(dist[i][t] < INF && dist[t][j] < INF && dist[t][t] < 0){
					dist[i][j] = -INF;
				}
			}
		}
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    
    floyd(n);
    ciclos_neg(n);
	for(Long i = 0; i < q; i++){
		cin >> x >> y;
		if(dist[x][y] == INF){
			cout << "Impossible" << endl;
		}else if(dist[x][y] == -INF){
			cout << "-Infinity" << endl;
		}else{
			cout << dist[x][y] << endl;
		}
	}
	cout<<endl;
	return 0;
}


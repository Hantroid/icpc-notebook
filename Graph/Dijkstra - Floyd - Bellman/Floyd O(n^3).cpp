#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=301;
const Long INF=1e14;

Long dist[N][N];

void floyd(Long n){
	for(Long k = 0; k < n; k++){
		for(Long i = 0; i < n; i++){
			for(Long j = 0; j < n; j++){
				if(dist[i][k] != INF && dist[k][j] != INF){
					dist[i][j] = min(dist[i][j],dist[i][k]+dist[k][j]);
				}
			}
		}
	}
	return;
}
int main(){
	ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    Long n;
	floyd(n);
	return 0;
}



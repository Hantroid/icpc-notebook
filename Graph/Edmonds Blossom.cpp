#include <bits/stdc++.h>
using namespace std;
typedef int Long;

const Long MX = 2e3;

//------------------------------- Edmonds Blossom --------------------------------
// Complexity O(n^2 m)
// find maximum matching in graph and construct any match
// the nodes should be in range [0,n>
vector<Long> adj[MX];
Long match[MX], qh, qt, q[MX], father[MX], base[MX];
bool inq[MX], inb[MX], inp[MX];
bool visEdge[MX][MX];

void addEdge(Long u, Long v){
	if(!visEdge[u][v]){
		adj[u].push_back(v);
		adj[v].push_back(u);
		visEdge[u][v] = visEdge[v][u] = true;
	}
	return;
}

Long lca(Long root, Long u, Long v){
	memset(inp, 0, sizeof(inp));
	while(true){
		inp[u = base[u]] = true;
		if(u == root) break;
		u = father[match[u]];
	}
	while(true){
		if(inp[v = base[v]]){
			return v;
		}else{
			v = father[match[v]];
		}
	}
}

void markBolssom(Long z, Long u){
	while(base[u] != z){
		Long v = match[u];
		inb[base[u]] = inb[base[v]] = true;
		u = father[v];
		if(base[u] != z) father[u] = v;
	}
	return;
}

void blossomContraction(Long s, Long u, Long v, Long n){
	Long z = lca(s,u,v);
	memset(inb, 0, sizeof(inb));
	markBolssom(z,u);
	markBolssom(z,v);
	if(base[u] != z) father[u] = v;
	if(base[v] != z) father[v] = u;
	for(Long i = 0; i < n; i++){
		if(inb[base[i]]){
			base[i] = z;
			if(!inq[i]){
				inq[q[++qt] = i] = true;
			}
		}
	}
	return;
}

Long findAugmentingPath(Long s, Long n){
	memset(inq, 0, sizeof(inq));
	memset(father, -1, sizeof(father));
	for(Long i = 0; i < n; i++) base[i] = i;
	inq[q[qh = qt = 0] = s] = true;
	while(qh <= qt){
		Long u = q[qh++];
		for(Long v : adj[u]){
			if(base[u] != base[v] && match[u] != v){
				if(v == s || match[v] >= 0 && father[match[v]] >= 0){
					blossomContraction(s,u,v,n);
				}else if(father[v] < 0){
					father[v] = u;
					if(match[v] < 0){
						return v;
					}else if(!inq[match[v]]){
						inq[q[++qt] = match[v]] = true;
					}
				}
			}
		}
	}
	return -1;
}

Long augmentedPath(Long s, Long t){
	Long u = t, v, w;
	while(u >= 0){
		v = father[u];
		w = match[v];
		match[v] = u;
		match[u] = v;
		u = w;
	}
	return t >= 0;
}

Long Edmonds(Long n){
	Long matchs = 0;
	memset(match, -1, sizeof(match));
	for(Long u = 0; u < n; u++){
		if(match[u] < 0){
			matchs += augmentedPath(u,findAugmentingPath(u,n));
		}
	}
	return matchs;
}

void constructMatch(Long n){
	for(Long i = 0; i < n; i++){
		if(i < match[i]){
			cout << i << " " << match[i] << "\n";
		}
	}
	return;
}
//------------------------------- Edmonds Blossom --------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	Long n, m, u, v, k;
	cin >> n >> m >> k;
	
	for(Long i = 0; i < m; i++){
		cin >> u >> v;
		u--;
		v--;
		addEdge(u,v);
	}
	cout << Edmonds(n) << "\n";
	return 0;
}

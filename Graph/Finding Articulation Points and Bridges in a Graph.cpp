#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 1001;


//------------------------ Articulation points and bridges-------------------------
// punto de articulacion = es el nodo que al ser removido del grafo incrementa el numero de componentes
// puente = es la arista que al ser removido del grafo incrementa el numero de componentes

// el low de todos los nodos no siempre es el mismo, depende del orden del recorrido del dfs
// por mas q sea rooteen en nodos iguales
// IMPORTANTE
// si hay multiples aristas de u -> v, se tomara como 1 sola de u -> v y lo podria tomar como puente
// lo cual puede dar un veredicto incorrecto
// pero los puntos de articulacion se mantienen correctos

// IMPORTANTE: los nodos de grado 1 no se consideran puntos de articulacion
// 			   debido a que no incrementa el numero de componentes

vector<Long> adj_APB[MX];
bool vis[MX];
Long tin[MX];
Long low[MX];
Long timer;

void dfs_APB(Long u, Long p = -1){
	vis[u] = true;
	tin[u] = low[u] = timer++;
	Long children = 0;
	// la cantidad de hijos es distinto a la cantidad de nodos adyacentes, ya que si ese nodo pertenece a un ciclo
	// los demas vertices adyacentes al nodo u, seran alcanzados por alguno nodo del subarbol q se forma
	// es por ello q si solo tiene un hijo -> si es un punto de articulacion
	for(Long v : adj_APB[u]){
		if(v == p) continue;
		if(!vis[v]){
			dfs_APB(v,u);
			low[u] = min(low[u],low[v]);
			if(tin[u] < low[v]){
				cout << "La arista de " << u << " a " << v << " es un puente" << endl;
			}
			// si no importase el p != -1, siempre tomaria a la raiz como punto de articulacion
			if(low[v] >= tin[u] && p != -1){
				// IMPORTANTE: el output de u se puede repetir
				// si se quiere hallar la cantidad de nodos que son puntos de articulacion
				// almacenarlos en un set
				cout << u << " es un punto de articulacion" << endl;
			}
			children++;
		}else{
			low[u] = min(low[u],tin[v]);
		}
	}
	// valida si la raiz es punto de articulacion
	if(p == -1 && children > 1){
		cout << u << " es un punto de articulacion" << endl;
	}
	return;
}

void find_APB(Long n){ // los nodos van de 0,n>
	timer = 0;
	for(Long i = 0; i < n; i++){
		vis[i] = false;
		tin[i] = low[i] = -1;
	}
	for(Long i = 0; i < n; i++){
		if(!vis[i]) dfs_APB(i);
	}
	return;
}

//------------------------ Articulation points and bridges-------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Long n, m, x, y;
	cin >> n >> m;
	for(Long i = 0; i < m; i++){
		cin >> x >> y;
		adj_APB[x].push_back(y);
		adj_APB[y].push_back(x);
	}
	
	find_APB(n+1);
	
	for(Long i = 1; i <= n; i++){
		cout << "--> " << tin[i] << " " << low[i] << "\n";
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = 1001;

vector<Long> adj[MX];
bool vis[MX];
Long tin[MX];
Long low[MX];
Long timer;

// el low de todos los nodos depende del orden del recorrido del dfs
// por mas q sea la misma raiz
// si hay multiples aristas de u -> v, se tomara como 1 sola de u -> v y lo podria tomar como puente
// lo cual puede dar un veredicto incorrecto
void dfs_bridge(Long u, Long p = -1){
	vis[u] = true;
	tin[u] = low[u] = timer++;
	for(Long v : adj[u]){
		if(v == p) continue;
		if(!vis[v]){
			dfs_bridge(v,u);
			low[u] = min(low[u],low[v]);
			if(tin[u] < low[v]){
				cout << "La arista de " << u << " a " << v << " es un puente" << endl;
			}
		}else{
			low[u] = min(low[u],tin[v]);
		}
	}
	return;
}

void find_bridges(Long n){ // los nodos van de 0,n>
	timer = 0;
	for(Long i = 0; i < n; i++){
		vis[i] = false;
		tin[i] = low[i] = -1;
	}
	for(Long i = 0; i < n; i++){
		if(!vis[i]) dfs_bridge(i);
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	Long n, m, x, y;
	cin >> n >> m;
	for(Long i = 0; i < m; i++){
		cin >> x >> y;
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	
	find_bridges(n+1);
	
	for(Long i = 1; i <= n; i++){
		cout << "--> " << tin[i] << " " << low[i] << "\n";
	}
	return 0;
}


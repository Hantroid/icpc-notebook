#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

const Long N = 1e5+9;

vector<Long> adj[N];

struct HLD{
	Long parent[N];
	Long depth[N];
	Long heavy[N];
	Long head[N];
	Long pos[N];
	Long cur_pos;
	
	void clear(Long n){ // nodos [0,n>
		cur_pos = 0;
		for(Long i = 0; i <= n; i++){
			parent[i] = heavy[i] = -1;
			depth[i] = head[i] = pos[i] = 0;
		}
		return;
	}
	
	Long dfs(Long u){
		Long size = 1;
		Long mx_size = 0;
		for(Long v : adj[u]){
			if(v != parent[u]){
				parent[v] = u;
				depth[v] = depth[u] + 1;
				Long size_v = dfs(v);
				size += size_v;
				if(mx_size < size_v){
					mx_size = size_v;
					heavy[u] = v;
				}
			}
		}
		return size;
	}
	
	// Tener cuidado si se usa Fenwik Tree ya que las posiciones estan indexadas desde 0
	// gracias a la descomposicion de caminos, se puede usar segment tree para queries y updates
	void decompose(Long u, Long h){
		head[u] = h;
		pos[u] = cur_pos; // indica que el nodo "u" esta en la posicion "cur_pos"
		cur_pos++;
		if(heavy[u] != -1){
			decompose(heavy[u],h);
		}
		for(Long v : adj[u]){
			if(v != parent[u] && v != heavy[u]){
				decompose(v,v);
			}
		}
		return;
	}
	
	Long lca(Long u, Long v){
		while(head[u] != head[v]){
			if(depth[head[u]] > depth[head[v]]){
				u = parent[head[u]];
			}else{
				v = parent[head[v]];
			}
		}
		return (depth[u] < depth[v] ? u : v);
	}
	// para los update y queries se puede usar una estructura como Fenwick Tree, Segment tree, u otras
	// con la ayuda de la logica de Heavy Light Decompotition
};

HLD G;

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    G.clear(16);
    adj[0].push_back(1);
    adj[0].push_back(2);
    adj[0].push_back(3);
    adj[1].push_back(4);
    adj[1].push_back(5);
    adj[5].push_back(8);
    adj[5].push_back(9);
    adj[2].push_back(7);
    adj[7].push_back(11);
    adj[2].push_back(6);
    adj[6].push_back(10);
    adj[10].push_back(12);
    adj[10].push_back(13);
    adj[10].push_back(14);
    adj[13].push_back(15);
    
    G.dfs(0);
    G.decompose(0,0);
    
    cout << "Parent: " << endl;
    for(Long i = 0; i < 16;  i++){
    	cout << G.parent[i] << " ";
	}
	cout << endl;
	cout << "Depth: " << endl;
	for(Long i = 0; i < 16;  i++){
    	cout << G.depth[i] << " ";
	}
	cout << endl;
	cout << "Heavy: " << endl;
	for(Long i = 0; i < 16;  i++){
    	cout << G.heavy[i] << " ";
	}
	cout << endl;
	cout << "Head: " << endl;
	for(Long i = 0; i < 16;  i++){
    	cout << G.head[i] << " ";
	}
	cout << endl;
	
	for(Long i = 0; i < 16; i++){
		for(Long j = 0; j < 16; j++){
			cout << "LCA " << i << " - " << j << " = " << G.lca(i,j) << endl;
		}
	}
	
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=100011;

vector<Long> adj[N];
vector<Long> inv_adj[N];
vector<Long> order;
bool vis[N];
Long component[N];

void dfs1(Long u){
	vis[u] = true;
	Long sz = adj[u].size();
	for(Long v : adj[u]){
		if(!vis[v]) dfs1(v);
	}
	order.push_back(u);
	return;
}

void dfs2(Long u, Long p){
	vis[u] = true;
	for(Long v : adj[u]){
		if(!vis[v]) dfs2(v,p);
	}
	component[u] = p;
	return;
}

// los vertices estan indexado en 1
void Kosaraju(Long n){
	for(Long i = 1; i <= n; i++) vis[i]=false;
	for(Long i = 1; i <= n; i++){
		if(!vis[i]) dfs1(i);
	}
	for(Long i = 1; i <= n; i++) vis[i]=false;
	for(Long i = 1; i <= n; i++){
		Long v = order[n-i];
		if(!vis[v]) dfs2(v,v);
	}
	return;
}


int main(){

	return 0;
}



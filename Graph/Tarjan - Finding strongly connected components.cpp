#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

const Long N = 1e5+1;

vector<Long> adj[N];
Long disc[N];
Long low[N];
stack<Long> st;
Long times = 0;
Long component[N];

void dfs(Long u){
	times++;
	disc[u] = low[u] = times;
	st.push(u);
	for(auto v : adj[u]){
		if(disc[v] == -1 || component[v] == -1){
			if(disc[v] == -1){
				dfs(v);
			}
			low[u] = min(low[u],low[v]);
		}
	}
	if(low[u] == disc[u]){
		while(st.top() != u){
			component[st.top()] = u;
			st.pop();
		}
		component[st.top()] = u;
		st.pop();
	}
	return;
}

void Tarjan(Long n){
	for(Long i = 0; i <= n; i++){
		disc[i] = -1;
		component[i] = -1;
	}
	for(Long i = 1; i <= n; i++){
		if(disc[i] == -1){
			dfs(i);
		}
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    /*adj[1].push_back(2);
    adj[2].push_back(3);
    adj[3].push_back(5);
    adj[3].push_back(4);
    adj[3].push_back(1);
    adj[4].push_back(1);
    adj[5].push_back(6);
    adj[6].push_back(7);
    adj[6].push_back(8);
    adj[7].push_back(5);
    adj[7].push_back(3);
    adj[8].push_back(9);
    adj[9].push_back(6);
    adj[9].push_back(10);
    adj[10].push_back(6);*/
    
    adj[1].push_back(2);
    adj[2].push_back(3);
    adj[3].push_back(4);
    adj[4].push_back(2);
    adj[2].push_back(5);
    adj[5].push_back(6);
    adj[6].push_back(5);
    adj[5].push_back(7);
    adj[7].push_back(8);
    adj[8].push_back(9);
    adj[7].push_back(10);
    
    Tarjan(10);
    
    for(Long i = 1; i <= 10; i++){
    	cout << low[i] << " ";
	}
	cout << endl;
	for(Long i = 1; i <= 10; i++){
    	cout << component[i] << " ";
	}
	cout << endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;

vector<ll> prefixFunction(string s){
	ll n=s.size();
	vector<ll> b(n);
	for(ll i=1; i<n; i++){
		ll j=b[i-1];
		while(j>0 && s[i]!=s[j]) j=b[j-1];
		if(s[i]==s[j]) j++;
		b[i]=j;
	}
	return b;
}

int main(){
	
	return 0;
}


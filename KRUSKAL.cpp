#include <bits/stdc++.h>
using namespace std;

long long n,m;
pair<long long, pair<long long, long long> > arista[9000001];
long long padre[3001];
long long rnk[3001];

long long buscapadre(long long x){
	if(x!=padre[x]){
		padre[x]=buscapadre(padre[x]);
	}
	return padre[x];
}

int main(){
	cin>>n>>m;
	for(long long i=0; i<n; i++){
		padre[i]=i;
		rnk[i]=0;
	}
	long long peso,x,y;
	for(long long i=0; i<m; i++){
		cin>>x>>y>>peso;
		x--;
		y--;
		//bidireccional
		arista[2*i]=make_pair(peso,make_pair(x,y));
		arista[2*i+1]=make_pair(peso,make_pair(y,x));
	}
	sort(arista,arista+2*m);
	
	/*cout<<endl;
	for(long long i=0; i<2*m; i++){
		cout<<arista[i].first<<" "<<arista[i].second.first+1<<" "<<arista[i].second.second+1<<endl;
	}
	cout<<endl;*/
	
	long long distotal=0;
	for(long long i=0; i<2*m; i++){
		long long u=arista[i].second.first;
		long long v=arista[i].second.second;
		long long padre_u=buscapadre(u);
		long long padre_v=buscapadre(v);
		if(padre_u!=padre_v){
			distotal=distotal+arista[i].first;
			//padre[padre_u]=padre_v;
			if(rnk[padre_u]>rnk[padre_v]){
				padre[padre_v]=padre_u;
			}else{
				padre[padre_u]=padre_v;
			}
			if(rnk[padre_u]==rnk[padre_v]){
				rnk[padre_v]++;
			}
		}
	}
	/*for(long long i=0; i<n; i++){
		cout<<padre[i]+1<<" ";
	}
	cout<<endl;*/
	cout<<distotal<<endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;
void snum(ll &a){scanf("%lld",&a);}
void snum(ll &a, ll &b){scanf("%lld %lld",&a,&b);}
void snum(ll &a, ll &b, ll &c){scanf("%lld %lld %lld",&a,&b,&c);}
void snum(ll &a, ll &b, ll &c, ll &d){scanf("%lld %lld %lld %lld",&a,&b,&c,&d);}

int main(){
	// complejidad O(n*W) W=el peso maximo q se puede cargar
	ll val[]={60,100,120};
	ll wt[]={10,50,40};
	ll W=50,n=3;
	ll K[3+2][W+2];
	for(ll i=0; i<=n; i++){
		for(ll w=0; w<=W; w++){
			if(i==0 || w==0){
				K[i][w]=0;
			}else if(wt[i-1]<=w){
				K[i][w]=max(val[i-1]+K[i-1][w-wt[i-1]],K[i-1][w]);
			}else{
				K[i][w]=K[i-1][w];
			}
		}
	}
	cout<<K[n][W]<<endl;
	return 0;
}



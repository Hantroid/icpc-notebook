#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=10001;



/*  ----------------------------- LCA -----------------------------
vector<Long> adj[M];
Long F[K][M];
Long D[M];
 
void lca_dfs(Long u){
	for(Long v : adj[u]){
		if(v == F[0][u]) continue;
		F[0][v] = u;
		D[v] = D[u] + 1;
		lca_dfs(v);
	}
	return;
}
 
void lca_init(Long x, Long n){
	D[x] = 0;
	F[0][x] = -1;
	lca_dfs(x);
	for(Long k = 1; k < K; k++){
		for(Long x = 0; x < n; x++){
			if(F[k-1][x] < 0) F[k][x] = -1;
			else F[k][x] = F[k-1][F[k-1][x]];
		}
	}
	return;
}
 
Long lca(Long x, Long y){
	if(D[x] < D[y]) swap(x,y);
	for(Long k = K-1; k >= 0; k--){
		if(D[x] - (1ll<<k) >= D[y]) x = F[k][x];
	}
	if(x == y) return y;
	for(Long k = K-1; k >= 0; k--){
		if(F[k][x] != F[k][y]){
			x = F[k][x];
			y = F[k][y];
		}
	}
	return F[0][x];
}
  ----------------------------- LCA -----------------------------*/

Long anc[N][40];
vector<Long> adj[N];
Long T[N];
Long L[N];


//FALTA MEJORAR EL TIEMPO DEL LCA, UNO MAS OPTIMO ES DEL DE MARCOSK
//https://codeforces.com/gym/102021/submission/48651474

void build(Long n){
	for(Long i = 0; i <= n; i++){
		for(Long j = 0; (1ll<<j) <= n; j++){
			anc[i][j] = -1;
		}
	}
	for(Long i = 0; i <= n; i++){
		anc[i][0] = T[i];
	}
	for(Long j = 1; (1ll<<j) <= n; j++){
		for(Long i = 0; i <= n; i++){
			if(anc[i][j-1] != -1){
				anc[i][j] = anc[anc[i][j-1]][j-1];
			}
		}
	}
	return;
}

Long lca(Long u, Long v){
	if(L[u] < L[v]) swap(u,v);
	Long lg = 63 - (__builtin_clzll(L[u]));
	for(Long i = lg; i >= 0; i--){
		if(L[u] - (1ll<<i) >= L[v]){
			u = anc[u][i];
		}
	}
	if(u == v) return u;
	for(Long i = lg; i >= 0; i--){
		if(anc[u][i] != -1 && anc[u][i] != anc[v][i]){
			u = anc[u][i];
			v = anc[v][i];
		}
	}
	return T[u];
}


//falta cambiar a bfs para n >= 1000000
void dfs(Long u){
	for(Long v : adj[u]){
		if(L[v] == -1){
			T[v] = u;
			L[v] = L[u]+1;
			dfs(v);
		}
	}
	return;
}

int main(){
	Long n,m,x;
	cin>>n;
	for(Long i=0; i<n; i++){
		cin>>m;
		for(Long j=0; j<m; j++){
			cin>>x;
			adj[i].push_back(x);
			adj[x].push_back(i);
		}
	}
	for(Long i=0; i<n; i++){
		T[i]=-1;
		L[i]=-1;
	}
	T[0]=-1;
	L[0]=0;
	dfs(0);
	build(n);
	Long q;
	cin>>q;
	Long l,r;
	for(Long i=0; i<q; i++){
		cin>>l>>r;
		cout<<lca(l,r)<<endl;
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=100001;

Long A[]={0, 8, 4, 12, 2, 10, 6, 14, 1, 9, 5, 13, 3, 11, 7, 15};
Long tail[16];

Long CellIndex(Long l, Long r, Long v){
	while(l!=r){
		Long med=(l+r)/2;
		if(tail[med]<v){
			l=med+1;
		}else{
			r=med;
		}
	}
	if(tail[l]<v){
		l++;
	}
	return l;
}

Long LongestIncreasingSubsequenceSize(Long n){
	for(Long i=0; i<n; i++){
		tail[i]=-1e9;
	}
	Long LISS=1;
	tail[0]=A[0];
	for(Long i=1; i<n; i++){
		if(A[i]>tail[LISS-1]){
			tail[LISS]=A[i];
			LISS++;
		}else{
			tail[CellIndex(0,LISS-1,A[i])]=A[i];
		}
	}
	return LISS;
}

int main(){
	cout<<LongestIncreasingSubsequenceSize(16)<<endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

//-------------------------------------- 2 SAT ----------------------------------
struct TwoSAT{
	Long n;
	vector< vector<Long> > adj, inv_adj;
	vector<bool> used;
	vector<Long> order, comp;
	vector<bool> assignment;
	
	TwoSAT(Long n) : n(n){
		comp.clear();
		order.clear();
		assignment.clear();
		adj.resize(n);
		inv_adj.resize(n);
	}
	
	void add_edge(Long u, Long v){
		adj[u].push_back(v);
		inv_adj[v].push_back(u);
		return;
	}
	
	void dfs1(Long u){
		used[u] = true;
		for(Long v : adj[u]){
			if(!used[v]) dfs1(v);
		}
		order.push_back(u);
	}
	
	void dfs2(Long u, Long c){
		comp[u] = c;
		for(Long v : inv_adj[u]){
			if(comp[v] == -1) dfs2(v,c);
		}
	}
	
	bool solve_2SAT(){
		used.assign(n,false);
		for(Long i = 0; i < n; i++){
			if(!used[i]) dfs1(i);
		}
		
		comp.assign(n,-1);
		for(Long i = 0, j = 0; i < n; i++){
			Long v = order[n - i - 1];
			if(comp[v] == -1) dfs2(v,j++);
		}
		
		assignment.assign(n/2,false);
		for(Long i = 0; i < n; i += 2){
			if(comp[i] == comp[i+1]) return false;
			assignment[i/2] = comp[i] > comp[i+1];
		}
		return true;
	}
};
//-------------------------------------- 2 SAT ----------------------------------

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	TwoSAT G(8);
	G.add_edge(0,3);
	G.add_edge(2,1);
	G.add_edge(4,7);
	G.add_edge(6,5);
	
	cout << G.solve_2SAT() << endl;
	for(Long x : G.comp){
		cout << x << " ";
	}
	cout << endl;
	
	for(Long x : G.assignment){
		cout << x << " ";
	}
	cout << endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef double Double;

struct CD {  // or typedef complex<Double> CD; (but 4x slower)
	Double r, i;
	CD(Double r=0, Double i=0) : r(r) , i(i){}
	Double real() const{ return r; }
	void operator /= (const int c){ r /= c, i /= c;}
};
CD operator*(const CD& a, const CD& b){ return CD(a.r * b.r - a.i * b.i, a.r * b.i + a.i * b.r); }
CD operator+(const CD& a, const CD& b){ return CD(a.r + b.r, a.i + b.i); }
CD operator-(const CD& a, const CD& b){ return CD(a.r - b.r, a.i - b.i); }

const Long MX = (1ll << 18);
const Double PI = acos(-1.0); // FFT
CD cp1[MX+9], cp2[MX+9]; // MX must be power of 2 !!
Long R[MX+9];

void fft(CD* a, Long n, bool inverso){
	for(Long i = 0; i < n; i++){
		if(R[i] < i){
			swap(a[R[i]],a[i]);
		}
	}
	for(Long m = 2; m <= n; m <<= 1ll){
		Double z = 2 * PI / m * (inverso ? -1 : 1); // FFT
		CD wi = CD(cos(z),sin(z)); // FFT
		for(Long j = 0; j < n; j += m){
			CD w(1);
			for(Long k = j, k2 = j + m/2; k2 < j + m; k++, k2++){
				CD u = a[k];
				CD v = a[k2] * w;
				a[k] = u + v;
				a[k2] = u - v;
				w = w * wi;
			}
		}
	}
	if(inverso){
		for(Long i = 0; i < n; i++){// FFT
			a[i] /= n;
		}
	}
	return;
}

//si falla por precicion se puede multpiplicar los coef por un multiplicador "mul"
//y a los coeficientes del producto dividirlos por "mul*mul", para tener la verdedadrea multiplicacion
//vector<Long> multiply(vector<Long>& p1, vector<Long>& p2, Long mul){
vector<Long> multiply(vector<Long>& p1, vector<Long>& p2){
	Long n = p1.size() + p2.size() + 1;
	Long m = 1, cnt = 0;
	while(m <= n){
		m += m;
		cnt++;
	}
	for(Long i = 0; i < m; i++){
		R[i] = 0;
		for(Long j = 0; j < cnt; j++){
			R[i] = (R[i] << 1ll) | ((i>>j)&1);
		}
	}
	for(Long i = 0; i < m; i++){
		cp1[i] = cp2[i] = 0;
	}
	for(Long i = 0; i < p1.size(); i++){
		cp1[i] = p1[i];
	}
	for(Long i = 0; i < p2.size(); i++){
		cp2[i] = p2[i];
	}
	fft(cp1,m,false);
	fft(cp2,m,false);
	for(Long i = 0; i < m; i++){
		cp1[i] = cp1[i] * cp2[i];
	}
	fft(cp1,m,true);
	vector<Long> result;
	n -= 2;
	for(Long i = 0; i < n; i++){
		result.push_back(Long(cp1[i].real() + (cp1[i].real() > 0 ? 0.5 : -0.5)));
	}
	return result;
}

vector<Long> countCharMatching(string &A, string &B){
	vector<Long> ans(A.size()+B.size(),0);
	Long letras = 26; //son los tipos de letra q puede existir sin contra el comodin
	//Long mul = 100; es un multiplicador para no perder precición
	
	Long n = A.size();
	Long m = B.size();
	vector<Long> rp;
	vector<Long> a(n,0);
	vector<Long> b(m,0);
	for(Long l = 0; l < letras; l++){
		for(Long i = 0; i < n; i++){
			if(A[i] - 'a' == l){
				a[i] = 1;
			}else{
				a[i] = 0;
			}
		}
		// es necesario voltear la cadena para acomodar el prob a una multiplicacion
		for(Long i = 0; i < m; i++){
			if(B[i] - 'a' == l){
				b[m - i - 1] = 1;
			}else{
				b[m - i - 1] = 0;
			}
		}
		rp = multiply(a,b);
		for(Long i = 0; i < rp.size(); i++){
			ans[i] += rp[i];
		}
	}
	return ans;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	string A = "abbcaba";
	string B = "acb";
	vector<Long> ans = countCharMatching(A,B);
	for(Long i = B.size() - 1; i < ans.size(); i++){
		cout << "Hay " << ans[i] << " coincidencias con el texto " << A << " iniciando en la pos " << i - B.size() + 1 << "\n";
	}
	
	// A = abbcaba
	// B = acb      -> 2 coincidencias
	// B =  acb     -> 0 coincidencias
	// B =   acb    -> 1 coincidencias
	// B =    acb   -> 1 coincidencias
	// B =     acb  -> 1 coincidencias
	return 0;
}


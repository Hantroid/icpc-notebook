#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef double Double;

struct CD {  // or typedef complex<Double> CD; (but 4x slower)
	Double r, i;
	CD(Double r=0, Double i=0) : r(r) , i(i){}
	Double real() const{ return r; }
	void operator /= (const int c){ r /= c, i /= c;}
};
CD operator*(const CD& a, const CD& b){ return CD(a.r * b.r - a.i * b.i, a.r * b.i + a.i * b.r); }
CD operator+(const CD& a, const CD& b){ return CD(a.r + b.r, a.i + b.i); }
CD operator-(const CD& a, const CD& b){ return CD(a.r - b.r, a.i - b.i); }

const Long MX = (1ll << 18);
const Double PI = acos(-1.0); // FFT
CD cp1[MX+9], cp2[MX+9]; // MX must be power of 2 !!
Long R[MX+9];

void fft(CD* a, Long n, bool inverso){
	for(Long i = 0; i < n; i++){
		if(R[i] < i){
			swap(a[R[i]],a[i]);
		}
	}
	for(Long m = 2; m <= n; m <<= 1ll){
		Double z = 2 * PI / m * (inverso ? -1 : 1); // FFT
		CD wi = CD(cos(z),sin(z)); // FFT
		for(Long j = 0; j < n; j += m){
			CD w(1);
			for(Long k = j, k2 = j + m/2; k2 < j + m; k++, k2++){
				CD u = a[k];
				CD v = a[k2] * w;
				a[k] = u + v;
				a[k2] = u - v;
				w = w * wi;
			}
		}
	}
	if(inverso){
		for(Long i = 0; i < n; i++){// FFT
			a[i] /= n;
		}
	}
	return;
}

vector<Long> multiply(vector<Long>& p1, vector<Long>& p2, Long letras, Long mul){
	Long n = p1.size() + p2.size() + 1;
	Long m = 1, cnt = 0;
	while(m <= n){
		m += m;
		cnt++;
	}
	for(Long i = 0; i < m; i++){
		R[i] = 0;
		for(Long j = 0; j < cnt; j++){
			R[i] = (R[i] << 1ll) | ((i>>j)&1);
		}
	}
	for(Long i = 0; i < m; i++){
		cp1[i] = cp2[i] = 0;
	}
	Double z = 2.0 * PI / letras;
	for(Long i = 0; i < p1.size(); i++){
		if(p1[i] != -1){
			cp1[i] = CD(cos(z*p1[i])*mul,sin(z*p1[i])*mul);
		}
	}
	for(Long i = 0; i < p2.size(); i++){
		if(p2[i] != -1){
			cp2[i] = CD(cos(z*p2[i])*mul,-sin(z*p2[i])*mul);
		}
	}
	fft(cp1,m,false);
	fft(cp2,m,false);
	for(Long i = 0; i < m; i++){
		cp1[i] = cp1[i] * cp2[i];
	}
	fft(cp1,m,true);
	vector<Long> result;
	n -= 2;
	mul *= mul;
	for(Long i = 0; i < n; i++){
		result.push_back(Long(cp1[i].real() + (cp1[i].real() > 0 ? 0.5 : -0.5)) / mul);
	}
	return result;
}

vector<Long> match(string &a, string &b){
	Long letras = 60; //son los tipos de letra q puede existir sin contra el comodin
	Long mul = 100; //es un multiplicador para no perder precici�n en los coeficientes ya que los coef iniciales son complejos con modulo = 1
	Long n = a.size();
	Long m = b.size();
	vector<Long> ans;
	vector<Long> A(n,0);
	vector<Long> B(m,0);
	for(Long i = 0; i < n; i++){
		if(a[i] == '*'){
			A[i] = -1;
		}else{
			A[i] = (a[i] - 'a');
		}
	}
	// es necesario voltear la cadena para acomodar el prob a una multiplicacion
	for(Long i = 0; i < m; i++){
		if(b[i] == '*'){
			B[m - i - 1] = -1;
		}else{
			B[m - i - 1] = (b[i] - 'a');
		}
	}
	ans = multiply(A,B,letras,mul); 
	// si el coeficiente despues de la multiplicacion es m - x, donde m: cantidad de letras del patr�n "b" y x: cantidad de comodines
	// entonces en esa pocision inicia el match (ans[m-1+i] = m - x) donde i es la pocision de inicio del match para el texto "a"
	return ans;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	string A = "abccaacc";
	string B = "a*c";
	Long comodines = 1;
	vector<Long> rp = match(A,B);
	
	cout << A << " " << B << endl;
	for(Long i = B.size() - 1; i < rp.size(); i++){
		if(rp[i] == B.size() - comodines){
			cout << "inicia en la pos " << i - B.size() + 1 << " y termina en " << i << "\n";
		}
	}
	return 0;
}


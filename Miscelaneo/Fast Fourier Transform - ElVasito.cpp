#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef double Double;

struct CD {  // or typedef complex<Double> CD; (but 4x slower)
	Double r, i;
	CD(Double r=0, Double i=0) : r(r) , i(i){}
	Double real() const{ return r; }
	void operator /= (const int c){ r /= c, i /= c;}
};
CD operator*(const CD& a, const CD& b){ return CD(a.r * b.r - a.i * b.i, a.r * b.i + a.i * b.r); }
CD operator+(const CD& a, const CD& b){ return CD(a.r + b.r, a.i + b.i); }
CD operator-(const CD& a, const CD& b){ return CD(a.r - b.r, a.i - b.i); }

const Long MX = (1ll << 20);
const Double PI = acos(-1.0); // FFT
CD cp1[MX+9], cp2[MX+9]; // MX must be power of 2 !!
Long R[MX+9];

void fft(CD* a, Long n, bool inverso){
	for(Long i = 0; i < n; i++){
		if(R[i] < i){
			swap(a[R[i]],a[i]);
		}
	}
	for(Long m = 2; m <= n; m <<= 1ll){
		Double z = 2 * PI / m * (inverso ? -1 : 1); // FFT
		CD wi = CD(cos(z),sin(z)); // FFT
		for(Long j = 0; j < n; j += m){
			CD w(1);
			for(Long k = j, k2 = j + m/2; k2 < j + m; k++, k2++){
				CD u = a[k];
				CD v = a[k2] * w;
				a[k] = u + v;
				a[k2] = u - v;
				w = w * wi;
			}
		}
	}
	if(inverso){
		for(Long i = 0; i < n; i++){// FFT
			a[i] /= n;
		}
	}
	return;
}

//si falla por precicion se puede multpiplicar los coef por un multiplicador "mul"
//y a los coeficientes del producto dividirlos por "mul*mul", para tener la verdedadrea multiplicacion
//vector<Long> multiply(vector<Long>& p1, vector<Long>& p2, Long mul){
vector<Long> multiply(vector<Long>& p1, vector<Long>& p2){
	Long n = p1.size() + p2.size() + 1;
	Long m = 1, cnt = 0;
	while(m <= n){
		m += m;
		cnt++;
	}
	//Orden de coef precalculado
	for(Long i = 0; i < m; i++){
		R[i] = 0;
		for(Long j = 0; j < cnt; j++){
			R[i] = (R[i] << 1ll) | ((i>>j)&1);
		}
	}
	for(Long i = 0; i < m; i++){
		cp1[i] = cp2[i] = 0;
	}
	for(Long i = 0; i < p1.size(); i++){
		cp1[i] = p1[i];
	}
	for(Long i = 0; i < p2.size(); i++){
		cp2[i] = p2[i];
	}
	fft(cp1,m,false);
	fft(cp2,m,false);
	for(Long i = 0; i < m; i++){
		cp1[i] = cp1[i] * cp2[i];
	}
	fft(cp1,m,true);
	vector<Long> result;
	n -= 2;
	for(Long i = 0; i < n; i++){
		result.push_back(Long(cp1[i].real() + (cp1[i].real() > 0 ? 0.5 : -0.5)));
	}
	return result;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	// Aplicaciones:
	// -Multplicacion de polinomios
	// -Cantidad de todas las posibles sumas tomando un elemento de cada arreglo A y B
	//  Ej: A = [1,2,3] y B = [2,4] --> a = (1x^1 + 1x^2 + 1x^3) y b = (1x^2 + 1x^4)
	//  el coef represanta la candidad de elemtos q tiene el arreglo y el exponente es el numero del arreglo
	//  a*b = (1x^3 + 1x^4 + 2x^5 + 1x^6 + 1x^7), expresa: 1 forma de sumar 3, 1 forma de sumar 4, 2 formas de sumar 5 ...
	// -String Matching -> O(nlongn) plantilla (busca patrones en un texto con o sin comodines), si no tiene comodin, usar KMP
	// -Cantidad de Matchig de caracteres de un patron a lo largo de un texto -> O(letras * nlogn) plantilla
	
	vector<Long> A, B, AB;
	A.push_back(0);
	A.push_back(1);
	A.push_back(2);
	B.push_back(1);
	B.push_back(1);
	B.push_back(3);
	AB = multiply(A,B);
	for(Long i = 0; i < AB.size(); i++){
		cout << AB[i] << "X" << i << " ";
	}
	cout << endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;
typedef double Double;
typedef complex<Double> cd;//typedef complex<Double> CD; (but 4x slower than struct)

const double PI = acos(-1.0);

void fft(vector<cd> &a, bool invert){
	Long n = a.size();
	
	for(Long i = 1, j = 0; i < n; i++){
		Long bit = n >> 1ll;
		for(; j & bit; bit >>= 1ll){
			j ^= bit;
		}
		j ^= bit;
		if(i < j){
			swap(a[i],a[j]);
		}
	}
	
	for(Long len = 2; len <= n; len <<= 1ll){
		double ang = 2 * PI / len * (invert ? -1 : 1);
		cd wlen(cos(ang), sin(ang));
		for(Long i = 0; i < n; i += len){
			cd w(1);
			for(Long j = 0; j < len / 2; j++){
				cd u = a[i+j], v = a[i+j+len/2] * w;
				a[i+j] = u + v;
				a[i+j+len/2] = u - v;
				w *= wlen;
			}
		}
	}
	
	if(invert){
		for(cd & x : a){
			x /= n;
		}
	}
	return;
}

//si falla por precicion se puede multpiplicar los coef por un multiplicador "mul"
//y a los coeficientes del producto dividirlos por "mul*mul", para tener la verdedadrea multiplicacion
//vector<Long> multiply(vector<Long> const &a, vector<Long> const &b, Long mul)
vector<Long> multiply(vector<Long> const &a, vector<Long> const &b){
	vector<cd> fa(a.begin(),a.end()), fb(b.begin(),b.end()); //asgina los coef enteros a coef complejos directamente
	Long n = 1;
	while(n < a.size() + b.size()){
		n <<= 1;
	}
	fa.resize(n);
	fb.resize(n);
	
	fft(fa,false);
	fft(fb,false);
	for(Long i = 0; i < n; i++){
		fa[i] *= fb[i];
	}
	fft(fa,true);
	
	// PROB H: https://blog.myungwoo.kr/121
	// si la precision falla se puede agregar al real 0.5 si es positivo, -0.5 si es negaivo
	vector<Long> result(n);
	for(Long i = 0; i < n; i++){
		result[i] = round(fa[i].real());
		//result[i] = Long(fa[i].real() + (fa[i].real() > 0 ? 0.5 : -0.5));
	}
	return result;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	vector<Long> A, B, AB;
	A.push_back(0);
	A.push_back(1);
	A.push_back(2);
	B.push_back(1);
	B.push_back(1);
	B.push_back(3);
	AB = multiply(A,B);
	for(Long i = 0; i < AB.size(); i++){
		cout << AB[i] << "X" << i << " ";
	}
	cout << endl;
	return 0;
}


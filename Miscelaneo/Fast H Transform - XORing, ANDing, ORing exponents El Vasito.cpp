#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long MX = (1ll << 20);

Long c1[MX+9],c2[MX+9];  // MX must be power of 2 !!
void fht(Long* p, Long n, bool inverso){
	for(Long l = 1; 2*l <= n; l *= 2){
		for(Long i = 0; i < n; i += 2*l){
			for(Long j = 0; j < l; j++){
				Long u = p[i+j], v = p[i+l+j];
				if(!inverso){ //XOR
					p[i+j] = u + v;
					p[i+l+j] = u - v;
				}else{
					p[i+j] = (u+v)/2;
					p[i+l+j] = (u-v)/2;
				}
				
				/*if(!inverso){ //AND
					p[i+j] = v;
					p[i+l+j] = u + v;
				}else{
					p[i+j] = -u + v;
					p[i+l+j] = u;
				}*/
				
				/*if(!inverso){ // OR
					p[i+j] = u + v;
					p[i+l+j] = u;	
				}else{
					p[i+j] = v;
					p[i+l+j] = u - v;
				}*/
			}
		}
	}
	return;
}
// like polynomial multiplication, but XORing exponents
// instead of adding them (also ANDing, ORing)
vector<Long> multiply(vector<Long>& p1, vector<Long>& p2){
	Long n = 1ll << (32 - __builtin_clz(max(p1.size(),p2.size())-1));
	for(Long i = 0; i < n; i++){
		c1[i] = c2[i] = 0;
	}
	for(Long i = 0; i < p1.size(); i++){
		c1[i] = p1[i];
	}
	for(Long i = 0; i < p2.size(); i++){
		c2[i] = p2[i];
	}
	fht(c1,n,false);
	fht(c2,n,false);
	for(Long i = 0; i < n; i++){
		c1[i] *= c2[i];
	}
	fht(c1,n,true);
	return vector<Long>(c1,c1+n);
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;

	vector<Long> A, B, AB;
	A.push_back(0);
	A.push_back(1);
	A.push_back(2);
	B.push_back(1);
	B.push_back(1);
	B.push_back(3);
	AB = multiply(A,B);
	// Realiza la multiplicacion de los coeficientes y el resultado es agregado al coeficiente con potencia xor de A, B
	for(Long i = 0; i < AB.size(); i++){
		cout << AB[i] << "X" << i << " ";
	}
	cout << endl;
	return 0;
}


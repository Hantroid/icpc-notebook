#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

// MOD-1 needs to be a multiple of MX !!
// big mod and primitive root for NTT:
#define MOD 998244353 // para MOD grandes usar mulmod
#define RT 5
Long mulmod(Long a, Long b){
	return (a*b) % MOD;
	/*Long r=0;
	while(b){
		if(b&1)r=(r+a)%MOD;
		b>>=1;a=(a+a)%MOD;
	}
	return r;*/
}
Long fp(Long a, Long e){
	Long r = 1;
	while(e){
		if(e & 1) r = mulmod(r,a);
		e >>= 1; a = mulmod(a,a);
	}
	return r;
}

struct CD {
	Long x;
	CD(){}
	CD(Long x) : x(x){}
	Long get() const{ return x; }
};
CD operator*(const CD& a, const CD& b){ return CD(mulmod(a.x,b.x)); }
CD operator+(const CD& a, const CD& b){ return CD((a.x+b.x)%MOD); }
CD operator-(const CD& a, const CD& b){ return CD((a.x+MOD-b.x)%MOD); }

const Long MX = (1ll << 20);
CD cp1[MX+9], cp2[MX+9]; // MX must be power of 2 !!
Long R[MX+9];

CD root(Long n, bool inverso){ // NTT
	Long r = fp(RT,(MOD-1) / n); // fp: modular exponentiation
	return CD(inverso ? fp(r,MOD-2) : r);
}

void fft(CD* a, Long n, bool inverso){
	for(Long i = 0; i < n; i++){
		if(R[i] < i){
			swap(a[R[i]],a[i]);
		}
	}
	for(Long m = 2; m <= n; m <<= 1ll){
		CD wi = root(m,inverso); // NTT
		for(Long j = 0; j < n; j += m){
			CD w(1);
			for(Long k = j, k2 = j + m/2; k2 < j + m; k++, k2++){
				CD u = a[k];
				CD v = a[k2] * w;
				a[k] = u + v;
				a[k2] = u - v;
				w = w * wi;
			}
		}
	}
	if(inverso){
		CD z(fp(n,MOD-2)); //NTT pm: modular exponentiation
		for(Long i = 0; i < n; i++){
			a[i] = a[i] * z;
		}
	}
	return;
}
vector<Long> multiply(vector<Long>& p1, vector<Long>& p2){
	Long n = p1.size() + p2.size() + 1;
	Long m = 1, cnt = 0;
	while(m <= n){
		m += m;
		cnt++;
	}
	for(Long i = 0; i < m; i++){
		R[i] = 0;
		for(Long j = 0; j < cnt; j++){
			R[i] = (R[i] << 1ll) | ((i>>j)&1);
		}
	}
	for(Long i = 0; i < m; i++){
		cp1[i] = cp2[i] = 0;
	}
	for(Long i = 0; i < p1.size(); i++){
		cp1[i] = p1[i];
	}
	for(Long i = 0; i < p2.size(); i++){
		cp2[i] = p2[i];
	}
	fft(cp1,m,false);
	fft(cp2,m,false);
	for(Long i = 0; i < m; i++){
		cp1[i] = cp1[i] * cp2[i];
	}
	fft(cp1,m,true);
	vector<Long> result;
	n -= 2;
	for(Long i = 0; i < n; i++){
		result.push_back(cp1[i].get());
	}
	return result;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	vector<Long> A, B, AB;
	A.push_back(0);
	A.push_back(1);
	A.push_back(2);
	B.push_back(1);
	B.push_back(1);
	B.push_back(3);
	AB = multiply(A,B);
	for(Long i = 0; i < AB.size(); i++){
		cout << AB[i] << "X" << i << " ";
	}
	cout << endl;
	return 0;
}


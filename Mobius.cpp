#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

const Long N = 10000;

Long fact[N];
Long mu[N];

void criba(){
	for(Long i = 0; i < N; i++) fact[i] = -1;
	for(Long i = 2; i*i < N; i++){
		if(fact[i] == -1){
			for(Long j = i*i; j < N; j += i){
				if(fact[j] == -1) fact[j] = i;
			}
		}
	}
	return;
}

void mobius(){
	mu[1] = 1;
	for(Long i = 2; i < N; i++){
		if(fact[i] == -1){
			mu[i] = -1;
		}else{
			Long nx = i / fact[i];
			if(nx % fact[i] == 0){
				mu[i] = 0;
			}else{
				mu[i] = -mu[nx];
			}
		}
	}
	return;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    criba();
    mobius();
    
	return 0;
}


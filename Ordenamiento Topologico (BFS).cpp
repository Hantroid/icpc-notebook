#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;

deque<ll> Q;
vector<ll> sol;
vector<ll> adj[N];
ll in_degree[N];

int main(){
	for(ll i=0; i<N; i++){
		if(in_degree[i]==0){
			Q.push_back(i);
		}
	}
	while(!Q.empty()){
		ll u=Q.front();
		sol.push_back(u);
		Q.pop_front();
		for(ll i=0; i<adj[u].size(); i++){
			ll v=adj[u][i];
			in_degree[v]--;
			if(in_degree[v]==0){
				Q.push_back(v);
			}
		}
	}
	return 0;
}


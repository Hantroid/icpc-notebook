#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//la lista de adyaciencia se guarda como (peso, nodo)
vector< pair<Long, Long> > adj[1001];
// priority queue ordena de menor a mayor y se saca el ultimo (el de mayor peso) con pq.top() y se insetra con pq.push()
priority_queue< pair<Long, Long> > pq;
bool vis[1001]; // se indica si ya ha sido usado o no

void process(Long nod){
	vis[nod]=true;
	Long sz=adj[nod].size();
	for(Long i=0; i<sz; i++){
		if(!vis[adj[nod][i].second]){
			pq.push(adj[nod][i]);
		}
	}
}

void prim(Long ini){
	Long pesototal=0;
	process(ini); //se agrega los adtyacentes al pq del nodo ini
	while(!pq.empty()){
		pair<Long, Long> ac=pq.top(); //se toma como pair actual
		pq.pop();//se saca el elemento tomado
		Long nod=ac.second;
		Long peso=-ac.first;
		if(!vis[nod]){
			pesototal += peso;
			process(nod);
		}
	}
	cout<<pesototal<<endl;
	return;
}

int main(){
	Long nodos,aristas;
	Long nod1,nod2,peso;
	cin>>nodos>>aristas;
	for(Long i=0; i<aristas; i++){
		cin>>nod1>>nod2>>peso;
		nod1--;
		nod2--;
		//se guarda el peso en negativo debido a que priority queue lo ordena de menor a mayor y se rquiere sacar el de menor peso y no el de mayor
		adj[nod1].push_back(make_pair(-peso,nod2));
		adj[nod2].push_back(make_pair(-peso,nod1));
	}
	//se escoge algun nodo de inicia (puede ser cualquiera)
	Long ini;
	cin>>ini;
	ini--;
	prim(ini);
	return 0;
}


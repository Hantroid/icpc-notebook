#include <bits/stdc++.h>
#define MAX(a,b) ((a) > (b) ? (a) : (b))
using namespace std;
char A[]={'a','b','c','b','a','b'}; //tama�o 6
char B[]={'b','a','a','b','a','a'}; //tama�o 6

int longLCS(int n, int m){
	if(n==0 || m==0){
		return 0;
	}
	if(A[n-1]==B[m-1]){
		return 1+longLCS(n-1,m-1);
	}else{
		return MAX((longLCS(n,m-1)),(longLCS(n-1,m)));
	}
}

int main(){
	cout<<longLCS(6,6)<<endl;
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
long long s[100001];

void merge(long long l, long long m, long long r){
    long long i, j, k;
    long long n1 = m - l + 1;
    long long n2 =  r - m;
 
    long long L[n1], R[n2];
 
    for (i = 0; i < n1; i++){
    	L[i] = s[l + i];
	}
    for (j = 0; j < n2; j++){
    	R[j] = s[m + 1+ j];
	}
 
    /* Merge the temp arrays back long longo arr[l..r]*/
    i = 0; // Initial index of first subarray
    j = 0; // Initial index of second subarray
    k = l; // Initial index of merged subarray
    while (i < n1 && j < n2){
        if (L[i] <= R[j]){
            s[k] = L[i];
            i++;
        }
        else{
            s[k] = R[j];
            j++;
        }
        k++;
    }
    while (i < n1){
        s[k] = L[i];
        i++;
        k++;
    }
    while (j < n2){
        s[k] = R[j];
        j++;
        k++;
    }
}
 
void mergeSort(long long l, long long r){
    if (l < r){
    	long long m = (r+l)/2;
        mergeSort(l, m);
        mergeSort(m+1, r);
        merge(l, m, r);
	}
}
int main(){
	long long n;
	cin>>n;
	for(long long i=0; i<n; i++){
		cin>>s[i];
	}
	mergeSort(0,n-1);
	for(long long i=0; i<n; i++){
		cout<<s[i]<<" ";
	}
	cout<<endl;
	return 0;
}


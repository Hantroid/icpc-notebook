#include <bits/stdc++.h>
using namespace std;

const int N=100005;
int st[N*4];
int A[N],n,m;

void build(int l, int r, int nodo){
	if(l==r){
		st[nodo]=A[r];
		return;
	}
	int med=(l+r)/2;
	build(l,med,nodo*2);
	build(med+1,r,nodo*2+1);
	st[nodo]=st[nodo*2]+st[2*nodo+1];
	return;
}

void update(int a, int v, int l, int r, int nodo){
	if(a<l || r<a) return;
	if(l==r){
		st[nodo]=v;
		return;
	}
	int med=(l+r)/2;
	update(a,v,l,med,2*nodo);
	update(a,v,med+1,r,2*nodo+1);
	st[nodo]=st[2*nodo]+st[2*nodo+1];
	return;
}

int query(int a, int b, int l, int r, int nodo){
	if(b<l or r<a) return 0;
	if(a<=l && r<=b) return st[nodo];
	int med=(l+r)/2;
	int L=query(a,b,l,med,nodo*2);
	int R=query(a,b,med+1,r,nodo*2+1);
	return R+L;
}

int main(){
	cin>>n;
	for(int i=0; i<n; i++){
		cin>>A[i];
	}
	
	build(0,n-1,1);
	
	cin>>m;
	for(int i=0; i<m; i++){
		int x,y,z;
		cin>>x>>y>>z;
		if(x==1){
			update(y-1,z,0,n-1,1);
		}else{
			cout<<query(y-1,z-1,0,n-1,1)<<endl;
		}
	}
	return 0;
}


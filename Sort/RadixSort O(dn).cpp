#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

//const Long MX = ;

void radixSort(vector<string> &v){
	Long n = v.size();
	Long mxLen = 0;
	Long mxDig = 0;
	for(auto ele : v){
		mxLen = max(mxLen, (Long) ele.size());
		for(auto dig : ele){
			mxDig = max(mxDig, (Long) dig);
		}
	}
	mxDig++;
	
	vector<Long> count(mxDig,0);
	for(Long pos = mxLen - 1; pos >= 0; pos--){
		auto newVec = v;
		for(Long i = 0; i < mxDig; i++) count[i] = 0; // reset frequency
		for(Long i = 0; i < n; i++){ // calculate frequency
			Long dig = (pos < v[i].size() ? v[i][pos] : 0);
			count[dig]++; 
		}
		for(Long i = 1; i < mxDig; i++) count[i] += count[i-1]; // acumulate frequency
		for(Long i = n-1; i >= 0; i--){ // assing new positions
			Long dig = (pos < v[i].size() ? v[i][pos] : 0);
			newVec[--count[dig]] = v[i];
		}
		v = newVec;
	}
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.tie(NULL);
	cout.precision(10);
	cout << fixed;
	
	//vector<string> v = {"15", "120", "53", "36", "167", "81", "75", "32", "9", "60"};
	vector<string> v = {"0000", "10", "11", "1", "01", "001", "002", "000", "0", "00"};
	radixSort(v);
	
	for(auto xx : v) cout << xx << " ";
	cout << endl;
	
	sort(v.begin(),v.end());
	for(auto xx : v) cout << xx << " ";
	cout << endl;
	return 0;
}

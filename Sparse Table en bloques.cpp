#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long N=100001;

Long ST[N][30];
Long A[N];

void precalculo(Long n){
	for(Long i = 0; i < n; i++){
		ST[i][0] = A[i];
	}
	for(Long j = 1; (1<<j) <= n; j++){
		for(Long i = 0; i+(1<<j)-1 < n; i++){
			ST[i][j]=ST[i][j-1]+ST[i+(1<<(j-1))][j-1];
		}
	}
	return;
}

Long query(Long l, Long r){
	Long rep = 0;
	Long T = r-l+1;
	//int lg=31-(__builtin_clz(T));
	Long lg = 63-(__builtin_clzll(T));
	for(Long j = lg; j >= 0; j--){
		if((1<<j) <= T){
			rep += ST[l][j];
			l += (1<<j);
			T -= (1<<j);
		}
	}
	return rep;
}

// usando dos bloques solo para 
// gcd max min

/*Long query(Long l, Long r){
	Long T = r-l+1;
	Long lg = 63-(__builtin_clzll(T));
	return max(ST[l][lg],ST[r-(1<<lg)+1][lg]);
}*/

int main(){
	Long n;
	cin>>n;
	for(Long i=0; i<n; i++){
		cin>>A[i];
	}
	precalculo(n);
	Long m;
	cin>>m;
	Long l,r;
	for(Long i=0; i<m; i++){
		cin>>l>>r;
		cout<<query(l,r)<<endl;
	}
	return 0;
}


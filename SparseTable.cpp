#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;

ll lookup[N][N/100];

void buildSparseTable(ll arr[],ll n){
	for(ll i=0; i<n; i++){
		lookup[i][0]=arr[i];
	}
	for(ll j=1; (1<<j)<=n; j++){
		for(ll i=0; (i+(1<<j)-1)<n; i++){
			lookup[i][j]=min(lookup[i][j-1],lookup[i+(1<<(j-1))][j-1]);
		}
	}
	return;
}

ll query(ll l, ll r){
	ll j=ll(log2(r-l+1));
	return min(lookup[l][j],lookup[r-(1<<j)+1][j]);
}

int main(){
	ll a[]={7,2,3,0,5,10,3,12,18};
	ll n=sizeof(a)/sizeof(a[0]);
	buildSparseTable(a,n);
	cout<<query(0,4)<<endl;
	cout<<query(4,7)<<endl;
	cout<<query(7,8)<<endl;
	return 0;
}


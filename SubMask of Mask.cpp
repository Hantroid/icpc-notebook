#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;
void snum(ll &a){scanf("%lld",&a);}
void snum(ll &a, ll &b){scanf("%lld %lld",&a,&b);}
void snum(ll &a, ll &b, ll &c){scanf("%lld %lld %lld",&a,&b,&c);}
void snum(ll &a, ll &b, ll &c, ll &d){scanf("%lld %lld %lld %lld",&a,&b,&c,&d);}

int main(){
	ll n;
	cin>>n;
	for(ll mask=0; mask<(1<<n); mask++){
		cout<<mask<<": "<<endl;
		for(ll submask=mask; submask>0; submask=((submask-1)&mask)){
			cout<<submask<<" ";
		}
		cout<<endl;
	}
	return 0;
}



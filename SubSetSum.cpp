#include <bits/stdc++.h>
using namespace std;

vector<long long> v;

bool dp[100][100];
bool vis[100][100];

bool sss(long long n, long long s){
	if(n<0) return false;
	if(vis[n][s]){
		return dp[n][s];
	}
	vis[n][s]=true;
	if(s==0){
		dp[n][s]=true;
	}
	if(s>0 && n>0){
		dp[n][s]=(sss(n-1,s) || sss(n-1,s-v[n-1]));
	}
	return dp[n][s];
}

int main(){
	long long n;
	cin>>n;
	long long x;
	for(long long i=0; i<n; i++){
		cin>>x;
		v.push_back(x);
	}
	long long k;
	cin>>k;
	if(sss(n,k)){
		cout<<"true"<<endl;
	}else{
		cout<<"false"<<endl;
	}
	
	for(long long i=0; i<n; i++){
		for(long long j=0; j<=10; j++){
			cout<<dp[i][j]<<" ";
		}
		cout<<endl;
	}
	return 0;
}


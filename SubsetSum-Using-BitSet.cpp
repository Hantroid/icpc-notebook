#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;
void snum(ll &a){scanf("%lld",&a);}
void snum(ll &a, ll &b){scanf("%lld %lld",&a,&b);}
void snum(ll &a, ll &b, ll &c){scanf("%lld %lld %lld",&a,&b,&c);}
void snum(ll &a, ll &b, ll &c, ll &d){scanf("%lld %lld %lld %lld",&a,&b,&c,&d);}

bool vis[21];

int main(){
	// halla todas las sumas de los subconjutos O(n*sum)
	ll A[]={3,7,9,1};
	ll n=4;
	ll sum=20;
	vis[0]=true; // la suma 0 siempre existe cuando el subconjunto es vacio
	for(ll i=0; i<n; i++){
		for(ll j=20; j>=A[i]; j--){
			vis[j]=(vis[j]|vis[j-A[i]]);
		}
	}
	if(vis[5]) cout<<"La suma 5 existe\n";
	else cout<<"La suma 5 no existe\n";
	return 0;
}



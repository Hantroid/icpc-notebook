#include <bits/stdc++.h>
#define REP(i, n) for (long long i = 0; i < n; i++)
using namespace std;
typedef long long Long;

//const Long N=;

struct Tuple{
	Long gcd, x, y;
	Tuple(Long a, Long b, Long c){
		gcd = a, x = b, y = c;
	}
};

Tuple extGcd(Long a, Long b){
	if(b == 0) return Tuple(a,1,0);
	Tuple ret = extGcd(b,a%b);
	return Tuple(ret.gcd, ret.y, ret.x - (a/b) * ret.y);
}

Long modularInverse(Long a, Long n){
	Tuple t = extGcd(a,n);
	return ((t.x % n) + n) % n;
}

Long chineseTheorem(vector<Long> &rem, vector<Long> &mod){
	Long k = mod.size();
	Long n = 1;
	for(Long i = 0; i < k; i++) n *= mod[i];
	Long x = 0;
	for(Long i = 0; i < k; i++){
		Long m = n / mod[i];
		Long y = modularInverse(m,mod[i]);
		x += (rem[i] * ((m*y)%n)) % n;
		x %= n;
	}
	return x;
}

int main(){
	ios_base::sync_with_stdio(false);
	cin.tie(NULL);
	cout.precision(10);
	cout << fixed;
    
    vector<Long> rem;
    vector<Long> mod;
    rem.push_back(1);
    mod.push_back(3);
    rem.push_back(2);
    mod.push_back(5);
    rem.push_back(3);
    mod.push_back(7);
    cout << chineseTheorem(rem,mod) << endl;
	return 0;
}


#include <bits/stdc++.h>
#define L 100000 //cantidad maxima de nodos
#define ALPH 26 //tama�o del alfabeto
using namespace std;
typedef long long Long;

Long trie[L][ALPH]; //una especie de lista de adyacencia
/*Dado el alfabeto ['a'-'z'] represnetado de [0,25] y el nodo u representa el prefijo P:
si trie[u][i]=0 entonces no existe el prefijo P+('a'+i), caso contrario trie[u][i]
sera igual al nodo v.*/
bool term[L];
Long nodos=1;

void init(Long cnt){
	nodos=1;
	for(Long i=0; i<cnt; i++){
		term[i]=0;
		for(Long j=0; j<ALPH; j++){
			trie[i][j]=0;
		}
	}
}

void addWord(string &s){
	Long sz=s.size();
	Long u=0; // empieza la raiz
	for(Long i=0; i<sz; i++){
		Long c=s[i]-'a';
		if(trie[u][c]==0){
			trie[u][c]=nodos;
			nodos++; // si no existe creamos noso
		}
		u=trie[u][c]; //nodo donde termina la plabra de manera temporal
	}
	//nodo donde realmente termina la palabra
	term[u]=1;// nodo terminal
}

bool isPerfix(string &s){
	Long sz=s.size();
	Long u=0;
	for(Long i=0; i<sz; i++){
		Long c=s[i]-'a';
		if(trie[u][c]==0){
			return 0;
		}
		u=trie[u][c];
	}
	return 1;
}

bool isWord(string &s){
	Long sz=s.size();
	Long u=0;
	for(Long i=0; i<sz; i++){
		Long c=s[i]-'a';
		if(trie[u][c]==0){
			return 0;
		}
		u=trie[u][c];
	}
	return term[u];//valida si es terminal
}

int main(){
	string A[]={"pikachu","mama","mar","marco","margarita","pikachu"};
	for(Long i=0; i<6; i++){
		addWord(A[i]);
	}
	Long q;
	cin>>q;
	string k;
	getline(cin,k);
	for(Long i=0; i<q; i++){
		getline(cin,k);
		cout<<"Es Prefijo: "<<isPerfix(k)<<endl;
		cout<<"Es Palabra: "<<isWord(k)<<endl;
	}
	return 0;
}


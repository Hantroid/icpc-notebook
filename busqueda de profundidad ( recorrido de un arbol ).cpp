#include <bits/stdc++.h>
#define K 100000
using namespace std;
vector<int> adj[K];
bool vis[K];

void dfs(int u){
	if(vis[u]) return ;
	vis[u]=true;
	int sz=adj[u].size();
	for(int i=0; i<sz; i++){
		int v=adj[u][i];
		dfs(v);
	}
}

int main(){
	int V,E,x,y;
	cin>>V>>E;
	for(int i=0; i<E; i++){
		cin>>x>>y;
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	dfs(0);
	for(int i=0; i<V; i++){
		cout<<vis[i]<<endl;
	}
	return 0;
}


#include <bits/stdc++.h>
using namespace std;
typedef long long Long;

const Long K=10000011;

Long fact[K];

void extSieve(){
	for( Long i=2; i*i < K; ++i ){
		if( fact[ i ] == 0 ){
			for( Long j = i*i ; j < K ; j+=i ) if( fact[j ] == 0) fact[ j ] = i;
		}
	}
	for( Long i=2; i<K; ++i ){
		if( fact[ i ] == 0 ){
			fact[ i ] = i;
		}
	}
}

void factorize( Long n ){
	while( n > 1 ){
		Long f = fact[ n ];
		Long exp = 0;
		while( n % f == 0 ){
			n /= f;
			exp++;	
		}
		cout << f << "^" << exp << endl;
	}
}

int main(){
	extSieve();
	Long n;
	cin >> n;
	factorize( n );
	return 0;
}

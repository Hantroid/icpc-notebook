#include <bits/stdc++.h>
#define N 10000000

using namespace std;

bool isPrime[N];

void sieve(){
    for (int i=2; i<N; i++) isPrime[i]=true;
    for(int i=2;i*i<N;i++){
        if(isPrime[i]){
            for(int j=i*i; j<N; j+=i) isPrime[j]=false;
        }
    }
    //si se quiere todos los primos en un arreglo hacer un for de 2 a N
    // ya q el ultimo for solo va hasta la raiz
}


int main(){
    sieve();
}


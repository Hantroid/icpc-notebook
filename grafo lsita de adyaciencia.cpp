#include <bits/stdc++.h>
#define N 100000
using namespace std;
vector<long long> adj[N];
int main(){
	long long n,m,x,y;
	cin>>n>>m; // n nodos y m aristas
	//grafo no dirigido
	for(long long i=0; i<m; i++){
		cin>>x>>y;
		x--;
		y--; // arista x e y
		adj[x].push_back(y);
		adj[y].push_back(x);
	}
	//imprimiendo listas de adyaciencias
	for(long long i=0; i<n; i++){
		cout<<"lista de nodo "<<i+1<<endl;
		long long sz=adj[i].size();
		for(long long j=0; j<sz; j++){
			cout<<" "<<adj[i][j]+1;
		}
		cout<<endl;
	}
	return 0;
}

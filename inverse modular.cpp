#include <bits/stdc++.h>
#define ALL(x) x.begin(),x.end()
#define UNIQUE(c) (c).resize(unique(ALL(c))-(c).begin())
using namespace std;
typedef long long ll;

// A naive method to find modulor multiplicative inverse of
// 'a' under modulo 'm'
int modInverse(int a, int m) {
    int m0 = m;
    int y = 0, x = 1;
 
    if (m == 1) return 0;
 
    while (a > 1){
        // q is quotient
        int q = a / m;
        int t = m;
 
        // m is remainder now, process same as
        // Euclid's algo
        m = a % m, a = t;
        t = y;
 
        // Update y and x
        y = x - q * y;
        x = t;
    }
    // Make x positive
    if (x < 0){
    	x += m0;
	}
    return x;
}

int main(){
    int a = 120/2, m = 1000000006/2;
    ll aux=modInverse(a, m);
    cout <<aux<<endl;
    return 0;
}

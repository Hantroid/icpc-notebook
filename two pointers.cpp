#include <bits/stdc++.h>
using namespace std;
typedef long long ll;

const ll N=100001;

ll A[]={1,5,7,12,14,18,21,31};

int main(){
	ll x;
	cin>>x;
	ll i=0;
	ll j=7;
	while(i!=j){
		if(A[i]+A[j]<x){
			i++;
		}else if(A[i]+A[j]>x){
			j--;
		}else{
			cout<<A[i]<<" "<<A[j]<<endl;
			return 0;
		}
	}
	cout<<"Imposible"<<endl;
	return 0;
}

